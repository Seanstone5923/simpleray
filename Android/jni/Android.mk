LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

TARGET_PLATFORM := 24
LOCAL_MODULE    := SimpleRay

LOCAL_C_INCLUDES += $(LOCAL_PATH)/../include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../DisplayWindow/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Incarnate/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Utils/PBRTLoader/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Examples/SimplePBRT/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Experimental/HEALPix/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/../../Incarnate/include/3rdparty/STB

FILE_LIST += $(wildcard $(LOCAL_PATH)/../src/*.c)
FILE_LIST += $(wildcard $(LOCAL_PATH)/../src/*.cpp)
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../DisplayWindow/src/*.cpp)
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Incarnate/src/*.cpp)
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Incarnate/src/3rdparty/STB/*.c)
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Utils/PBRTLoader/src/*.cpp)
#FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Examples/BasicWindowing/src/*.cpp)
#FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Examples/SimplePBRT/src/*.cpp)
FILE_LIST += $(wildcard $(LOCAL_PATH)/../../Experimental/HEALPix/src/*.cpp)
LOCAL_SRC_FILES := $(FILE_LIST:$(LOCAL_PATH)/%=%)

LOCAL_LDLIBS    		:= -llog -landroid -lEGL -lGLESv2 -latomic
LOCAL_CFLAGS 			+= -fopenmp
LOCAL_LDFLAGS 			+= -fopenmp

include $(BUILD_SHARED_LIBRARY)
