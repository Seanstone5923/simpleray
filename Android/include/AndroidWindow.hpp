#ifndef ANDROIDWINDOW_H
#define ANDROIDWINDOW_H

#include <jni.h>
#include <errno.h>
#include <math.h>

#include "AndroidApp.h"
#include "WindowBase.hpp"

struct AndroidWindow : public WindowBase
{
	static struct android_app* app;
	static int32_t handle_input(struct android_app* app, AInputEvent* event);
	static void handle_cmd(struct android_app* app, int32_t cmd);

	static EGLDisplay display;
	static EGLSurface surface;
	static EGLContext context;
	int32_t width;
	int32_t height;

	int32_t touchX;
	int32_t touchY;

	AndroidWindow();

    void loop();
    int getWidth()  { return width; }
    int getHeight() { return height; }
    void getCursorPos(int* x, int* y) { x = &touchX; y = &touchY; }
    void renderEvent() {}
    bool createWindow();
	bool createWindow(const char* title,int width,int height) { createWindow(); }
    void terminate();

	static void clear();
    inline void renderFrame();

	static void run();

	void init();
	void createContext();
	void createSurface();
	void surfaceChanged();
	void destroySurface();
	void destroyContext();

	static EGLConfig 	config;
	static EGLint 		format;
};

#endif
