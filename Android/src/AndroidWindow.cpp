#ifdef __ANDROID__

#include <thread>
#include <stdlib.h>

#include "Log.h"
#include "AndroidWindow.hpp"

PFNGLGENVERTEXARRAYSOESPROC 	glGenVertexArrays 		= (PFNGLGENVERTEXARRAYSOESPROC) 	eglGetProcAddress("glGenVertexArraysOES");
PFNGLBINDVERTEXARRAYOESPROC 	glBindVertexArray 		= (PFNGLBINDVERTEXARRAYOESPROC) 	eglGetProcAddress("glBindVertexArrayOES");
PFNGLDELETEVERTEXARRAYSOESPROC  glDeleteVertexArrays 	= (PFNGLDELETEVERTEXARRAYSOESPROC) 	eglGetProcAddress("glDeleteVertexArraysOES");

// Setup OpenGL ES 2
// http://stackoverflow.com/questions/11478957/how-do-i-create-an-opengl-es-2-context-in-a-native-activity

EGLDisplay 	AndroidWindow::display;
EGLSurface 	AndroidWindow::surface;
EGLContext 	AndroidWindow::context;
EGLConfig 	AndroidWindow::config;
EGLint 		AndroidWindow::format;

void AndroidWindow::init()
{
	LOGI("[AndroidWindow init]");
	display = eglGetDisplay(EGL_DEFAULT_DISPLAY);

	eglInitialize(display, 0, 0);

	/* Here, the application chooses the configuration it desires. In this
	 * sample, we have a very simplified selection process, where we pick
	 * the first EGLConfig that matches our criteria */
	 EGLint 		numConfigs;
	 const EGLint attribs[9] =
	 {
		 EGL_RENDERABLE_TYPE, EGL_OPENGL_ES2_BIT, //important
		 EGL_BLUE_SIZE, 	8,
		 EGL_GREEN_SIZE, 	8,
		 EGL_RED_SIZE, 		8,
		 EGL_NONE
	 };
	eglChooseConfig(display, attribs, &config, 1, &numConfigs);

	/* EGL_NATIVE_VISUAL_ID is an attribute of the EGLConfig that is
	 * guaranteed to be accepted by ANativeWindow_setBuffersGeometry().
	 * As soon as we picked a EGLConfig, we can safely reconfigure the
	 * ANativeWindow buffers to match, using EGL_NATIVE_VISUAL_ID. */
	eglGetConfigAttrib(display, config, EGL_NATIVE_VISUAL_ID, &format);
}

void AndroidWindow::createContext()
{
	LOGI("[AndroidWindow createContext]");
	const EGLint attribList[3] =
	{
		EGL_CONTEXT_CLIENT_VERSION, 2,
		EGL_NONE
	};
	context = eglCreateContext(display, config, NULL, attribList);
}

bool AndroidWindow::createWindow()
{
	LOGI("[AndroidWindow createWindow]");
	while (!surface)	// Read all pending events.
	{
		int ident;
		int events;
		struct android_poll_source* source;

		while ((ident=ALooper_pollAll(0, NULL, &events,(void**)&source)) >= 0)
		{
			if (source) source->process(app, source);								// Process this event.
			if (app->destroyRequested) return destroyContext(), terminate(), false;	// Check if we are exiting.
		}
	}

	return true;
}

void AndroidWindow::createSurface()
{
	LOGI("[AndroidWindow createSurface]");
	ANativeWindow_setBuffersGeometry(app->window, 0, 0, format);

	surface = eglCreateWindowSurface(display, config, app->window, NULL);

	if (eglMakeCurrent(display, surface, surface, context) == EGL_FALSE)
		LOGW("Unable to eglMakeCurrent");
}

void AndroidWindow::surfaceChanged()
{
	LOGI("[AndroidWindow surfaceChanged]");
	eglQuerySurface(display, surface, EGL_WIDTH, &width);
	eglQuerySurface(display, surface, EGL_HEIGHT, &height);
	glViewport(0, 0, width, height);
}

void AndroidWindow::loop()
{
	LOGI("[AndroidWindow loop]");
	while (1)	// Read all pending events.
	{
		int ident;
		int events;
		struct android_poll_source* source;

		while ((ident=ALooper_pollAll(0, NULL, &events,(void**)&source)) >= 0)
		{
			if (source) source->process(app, source);							// Process this event.
			if (app->destroyRequested) return destroySurface(), destroyContext(), terminate();	// Check if we are exiting.
		}
		if (surface)
		{
			renderEvent();
			//glClearColor(0.f, 1.f, 1.f, 1.f);
			clear();
			renderFrame();
		}
	}
}

void AndroidWindow::clear()
{
	glClear(GL_COLOR_BUFFER_BIT);
}

// Just the current frame in the display.
inline void AndroidWindow::renderFrame()
{
	render();
	eglSwapBuffers(display, surface);
}

void AndroidWindow::destroySurface()
{
	if (surface) eglDestroySurface(display, surface);
	surface = EGL_NO_SURFACE;
	eglMakeCurrent(display, EGL_NO_SURFACE, EGL_NO_SURFACE, context);
}

void AndroidWindow::destroyContext()
{
	WindowBase::terminateGL();

	if (context) eglDestroyContext(display, context);
	context = EGL_NO_CONTEXT;
	eglMakeCurrent(display, EGL_NO_SURFACE, EGL_NO_SURFACE, EGL_NO_CONTEXT);
}

//  Tear down the EGL context currently associated with the display.
void AndroidWindow::terminate()
{
	if (display) eglTerminate(display);
	display = EGL_NO_DISPLAY;
}

// Process the next input event.
int32_t AndroidWindow::handle_input(struct android_app* state, AInputEvent* event)
{
	AndroidWindow* window = (AndroidWindow*)state->userData;
	if (AInputEvent_getType(event) == AINPUT_EVENT_TYPE_MOTION)
	{
		window->touchX = AMotionEvent_getX(event, 0);
		window->touchY = AMotionEvent_getY(event, 0);
		LOGI("x %d\ty %d\n",window->touchX,window->touchY);
		return 1;
	}
	return 0;
}

// Process the next main command.
void AndroidWindow::handle_cmd(struct android_app* state, int32_t cmd)
{
	AndroidWindow* window = (AndroidWindow*)state->userData;
	switch (cmd)
	{
		case APP_CMD_START: 			// onStart
			LOGI("[AndroidWindow onStart]");
			window->init();
			window->createContext();
			break;
		case APP_CMD_RESUME:			// onResume
			LOGI("[AndroidWindow onResume]");
			break;
		case APP_CMD_PAUSE:				// onPause
			window->destroySurface();
			break;
		case APP_CMD_STOP:				// onStop
			window->destroyContext();
			break;
		case APP_CMD_DESTROY:			// onDestroy
			window->terminate();
			break;
		case APP_CMD_GAINED_FOCUS:		// onWindowFocusChanged
			break;
		case APP_CMD_LOST_FOCUS:		// onWindowFocusChanged
			break;
		case APP_CMD_INIT_WINDOW:		// surfaceCreated
			LOGI("[AndroidWindow surfaceCreated]");
			window->createSurface();
			if( ! window->initGL() )	// This inits The OpenGL enviroment WindowBase needs
				LOGW("Error: Failed to init OpenGL stuff for WindowBase");
			break;
		case APP_CMD_WINDOW_RESIZED:	// surfaceChanged
			window->surfaceChanged();
			break;
		case APP_CMD_TERM_WINDOW:		// surfaceDestroyed
			window->destroySurface();
			break;
		case APP_CMD_CONFIG_CHANGED:	// onConfigurationChanged
			break;
		case APP_CMD_WINDOW_REDRAW_NEEDED:
			if (window->surface) window->renderFrame();
			break;
	}
}

AndroidWindow::AndroidWindow()
{
	app->userData = this;
}

struct android_app* AndroidWindow::app;
extern int main();

// Main entry point, handles events
void android_main(struct android_app* state) // onCreate
{
	app_dummy();
	AndroidWindow::app 					= state;
	AndroidWindow::app->onAppCmd 		= AndroidWindow::handle_cmd;
	AndroidWindow::app->onInputEvent 	= AndroidWindow::handle_input;

	main();
}

#endif
