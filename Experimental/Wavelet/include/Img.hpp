#include "Vec.hpp"

struct Img;
struct ImgVec;

struct ImgVec
{
    ImgVec(Img* img, int i, bool COL) : img(img), i(i), COL(COL) {}
    Img* img;
    const bool COL = false;
    int i;
    inline float4& operator[] (int j);
};

struct Img
{
    Img(float4* buffer, int width, int height) : buffer(buffer), width(width), height(height) {}
    float4* buffer;
    const int width, height;
    ImgVec row(int y) { return ImgVec(this, y, false); }
    ImgVec col(int x) { return ImgVec(this, x, true); }
    inline float4& operator() (int x, int y) { return buffer[y*width +x]; }
};

float4& ImgVec::operator[] (int j) { return COL ? (*img)(i, j) : (*img)(j, i); }
