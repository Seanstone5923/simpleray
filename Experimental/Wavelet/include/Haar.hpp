#include <math.h>
#include <memory.h>

void haar(float* img, int length)
{
    if (length & 1)  return; // length needs to be even

    float4 buffer[length];
    memcpy(buffer, img, sizeof(buffer));

    for (int i=0; i<length/2; i++)
    {
        ((float4*)img)[i]             = (buffer[i*2] + buffer[i*2+1]) / sqrt(2.f);
        ((float4*)img)[length/2+i]    = (buffer[i*2] - buffer[i*2+1]) / sqrt(2.f);
    }
    return haar(img, length/2);
}

void haarx(float* img, int width, int height, int n=0)
{
    if ((width>>n) <= 1) return; // length needs to be even

    float* buffer = new float[4*width*height];
    memcpy(buffer, img, 4*width*height*sizeof(*buffer));

    for (int y=0; y<(height>>n); y++)
        for (int x=0; x<(width>>n)/2; x++)
        {
            ((float4*)img)[x + y*width]                 = (((float4*)buffer)[x*2 + y*width] + ((float4*)buffer)[x*2+1 + y*width]) / sqrt(2.f);
            ((float4*)img)[(width>>n)/2+x + y*width]    = (((float4*)buffer)[x*2 + y*width] - ((float4*)buffer)[x*2+1 + y*width]) / sqrt(2.f);
        }
}

void haary(float* img, int width, int height, int n=0)
{
    if ((height>>n) <= 1) return; // length needs to be even

    float* buffer = new float[4*width*height];
    memcpy(buffer, img, 4*width*height*sizeof(*buffer));

    for (int x=0; x<(width>>n); x++)
        for (int y=0; y<(height>>n)/2; y++)
        {
            ((float4*)img)[x + y*width]                 = (((float4*)buffer)[x + (y*2)*width] + ((float4*)buffer)[x + (y*2+1)*width]) / sqrt(2.f);
            ((float4*)img)[x + ((height>>n)/2+y)*width] = (((float4*)buffer)[x + (y*2)*width] - ((float4*)buffer)[x + (y*2+1)*width]) / sqrt(2.f);
        }
}

void haar2d(float* img, int width, int height, int n=0)
{
      haarx(img, width, height, n);
      haary(img, width, height, n);
      if ((height>>n) <= 1 && (width>>n) <= 1) return;
      return haar2d(img, width, height, n+1);
}

/*
def ihaar(a):
  if len(a) == 1:
    return a.copy()
  assert len(a) % 2 == 0, "length needs to be even"
  mid = ihaar(a[0:len(a)/2]) * scale
  side = a[len(a)/2:] * scale
  out = np.zeros(len(a), dtype=float)
  out[0::2] = (mid + side) / 2.
  out[1::2] = (mid - side) / 2.
  return out

def ihaar_2d(coeffs):
  h,w = coeffs.shape
  cols = np.zeros(coeffs.shape, dtype=float)
  for x in range(w):
    cols[:,x] = ihaar(coeffs[:,x])
  rows = np.zeros(coeffs.shape, dtype=float)
  for y in range(h):
    rows[y] = ihaar(cols[y])
  return rows
*/
