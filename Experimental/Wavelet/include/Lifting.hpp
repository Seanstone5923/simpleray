#include "Vec.hpp"
#include "Img.hpp"

/*
   Copyright and Use

   You may use this source code without limitation and without
   fee as long as you include:

   This software was written and is copyrighted by Ian Kaplan, Bear
   Products International, www.bearcave.com, 2001.

   This software is provided "as is", without any warrenty or
   claim as to its usefulness.  Anyone who uses this source code
   uses it at their own risk.  Nor is any support provided by
   Ian Kaplan and Bear Products International.

   Please send any bug fixes or suggested source changes to:
     iank@bearcave.com
*/

struct Lifting
{
    /**
    Split the <i>vec</i> into even and odd elements,
    where the even elements are in the first half
    of the vector and the odd elements are in the
    second half.
    */
    void split(ImgVec vec, int N)
    {
        int start = 1; // first odd
        int end = N - 1;
        while (start < end)
        {
            for (int i = start; i < end; i += 2)
            {

                float4 tmp = vec[i];
                vec[i] = vec[i+1];
                vec[i+1] = tmp;
            }
            start++;
            end--;
        }
    }

    /**
    Merge the odd elements from the second half of the N element
    region in the array with the even elements in the first
    half of the N element region.  The result will be the
    combination of the odd and even elements in a region
    of length N.

    */
    void merge(ImgVec vec, int N)
    {
        int half = N >> 1;
        int start = half-1;
        int end = half;
        while (start > 0)
        {
            for (int i = start; i < end; i += 2)
            {
                float4 tmp = vec[i];
                vec[i] = vec[i+1];
                vec[i+1] = tmp;
            }
            start--;
            end++;
        }
    }

    /**
    Predict step, to be defined by the subclass

    @param vec input array
    @param N size of region to act on (from 0..N-1)
    @param direction forward or inverse transform

    */
    virtual void predict(ImgVec vec, int N, bool forward);

    /**
    Update step, to be defined by the subclass

    @param vec input array
    @param N size of region to act on (from 0..N-1)
    @param direction forward or inverse transform

    */
    virtual void update(ImgVec vec, int N, bool forward);

    /**
    Simple wavelet Lifting Scheme forward transform

    forwardTrans is passed an array of doubles.  The array size must
    be a power of two.  Lifting Scheme wavelet transforms are calculated
    in-place and the result is returned in the argument array.

    The result of forwardTrans is a set of wavelet coefficients
    ordered by increasing frequency and an approximate average
    of the input data set in vec[0].  The coefficient bands
    follow this element in powers of two (e.g., 1, 2, 4, 8...).
    */
    void forwardTrans(Img img, int width, int height)
    {
        for (int w = width, h = height; w > 1 && h > 1; w = w >> 1, h = h >> 1)
        {
            for (int i=0; i<h; i++) forwardTrans(img.row(i), w);
            for (int j=0; j<w; j++) forwardTrans(img.col(j), h);
        }
    }

    void forwardTrans(ImgVec vec, int n)
    {
        split(vec, n);
        predict(vec, n, true);
        update(vec, n, true);
    }

    /**
    Default two step Lifting Scheme inverse wavelet transform

    inverseTrans is passed the result of an ordered wavelet
    transform, consisting of an average and a set of wavelet
    coefficients.  The inverse transform is calculated
    in-place and the result is returned in the argument array.
    */
    void inverseTrans(Img img, int width, int height)
    {
        for (int w = 2, h = 2; w <= width && h <= height; w = w << 1, h = h << 1)
        {
            for (int j=0; j<w; j++) inverseTrans(img.col(j), h);
            for (int i=0; i<h; i++) inverseTrans(img.row(i), w);
        }
    }

    void inverseTrans(ImgVec vec, int n)
    {
        update(vec, n, false);
        predict(vec, n, false);
        merge(vec, n);
    }

}; // Lifting
