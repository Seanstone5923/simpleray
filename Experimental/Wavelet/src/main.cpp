#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <iostream>
using namespace std;
#include <chrono>
#include <algorithm>
using namespace std::chrono;

#include "Window.hpp"
#include "Quaternion.hpp"
#include "Incarnate.hpp"
#include "Accelerator.hpp"
#include "Material.hpp"
#include "Renderer.hpp"
#include "Camera.hpp"
#include "Scene.hpp"
#include "PBRTLoader.hpp"
using namespace Incarnate;

#include "Lifting_Haar.hpp"

#define WINDOW_CONTROLS 0

Camera* 	camera;
Scene* 		scene;
Renderer* 	renderer;
Accelerator* accelerator;
Quatf Rotor = Quatf(1.0);

class MainWindow : public DisplayWindow
{
public:
	MainWindow(std::string title);
	void mouseDownEvent(Window::Button button);
	void resizeEvent(int width, int height);
	void keyDownEvent(Window::Key key);
	void renderEvent();
	void terminate();
};

void MainWindow::mouseDownEvent(Window::Button button)
{
	/*int width = getWidth(), height = getHeight();
	int u0 = width/2, v0 = height/2;

	// Rotate
	if (button ==  Window::Button::Button_Right)
	{
		// Get mouse position
		int x,y;
		getCursorPos(&x, &y);
		double u = x, v = y;

		// Compute new orientation
		Quatf rotateVector = (u-u0) * Quatf(camera->up) + (v-v0) * Quatf(camera->right);
		Quatf rotor = exp(2e-4 * rotateVector);

		camera->direction =  rotor * camera->direction * ~rotor;
		camera->right = normalize(cross(camera->direction, camera->up));
		camera->update();
	}

	renderer->render(scene);
	generateRenderTexture(renderer->getBuffer(), width, height);*/
}

void MainWindow::resizeEvent(int width, int height)
{
	//camera->setSize(width, height);
	//renderer->createBuffer(width, height);
	//renderer->render(scene);
	//camera->update();
	//generateRenderTexture(renderer->getBuffer(), width, height);
}

void MainWindow::keyDownEvent(Window::Key key)
{
	if (key == Window::Key_Escape)
		exit(0);
	/*else if(key == Window::Key_A)
		camera->position += camera->direction*0.1f;
	else if(key == Window::Key_Z)
		camera->position -= camera->direction*0.1f;
	else if(key == Window::Key_Right)
		camera->position += camera->right*0.1f;
	else if(key == Window::Key_Left)
		camera->position -= camera->right*0.1f;
	else if(key == Window::Key_Up)
		camera->position += camera->up*0.1f;
	else if(key == Window::Key_Down)
		camera->position -= camera->up*0.1f;
	camera->update();
	*/

	//renderer->render(scene);
	//generateRenderTexture(renderer->getBuffer(), getWidth(), getHeight());
}

MainWindow::MainWindow(std::string title)
{
	createWindow(("SimpleRay - "+title).c_str(), renderer->getWidth(), renderer->getHeight());

	// Raytrace
	renderer->createBuffer(renderer->getWidth(), renderer->getHeight());
	renderer->initRender();
}

void MainWindow::renderEvent()
{
	static high_resolution_clock::time_point t1 = high_resolution_clock::now();

	if(renderer->getCurrentSampleCount() < renderer->getSampleNum() || renderer->getSampleNum() < 0)
	{
		renderer->render();
		high_resolution_clock::time_point t2 = high_resolution_clock::now();
		double elapsed = duration_cast<duration<double>>(t2 - t1).count();

		//Clear line
		fputs("\033[A\033[2K",stdout);

		if(renderer->getSampleNum() < 0)
			cout << "Rendered " << renderer->getCurrentSampleCount() << " samples ";
		else
			cout << "Rendered " << renderer->getCurrentSampleCount() << "/"
				<< renderer->getSampleNum() << ". "
				<< (float)renderer->getCurrentSampleCount()*100.0/renderer->getSampleNum() << "% ";
		cout << "elapsed: " << elapsed << "s, " <<
			(double)((uint64_t)renderer->getWidth()*(uint64_t)renderer->getHeight()
			*(renderer->getCurrentSampleCount()+1))/elapsed/1000.0/1000.0 << " MSamples/s\n";
		fflush(stdout);//Force update to stdout
		rewind(stdout);//Reset cursor position
	}

	static float* waveletBuffer = 0;
	if (!waveletBuffer) waveletBuffer = new float[4*renderer->getWidth()*renderer->getHeight()];
	memcpy(waveletBuffer, renderer->getBuffer(), 4*renderer->getWidth()*renderer->getHeight()*sizeof(*renderer->getBuffer()));

	//haar2d(renderer->getBuffer(), renderer->getWidth(), renderer->getHeight());

	Img img((float4*)waveletBuffer, renderer->getWidth(), renderer->getHeight());
	Haar haar;
	haar.forwardTrans(img, renderer->getWidth(), renderer->getHeight());
	//haar.inverseTrans(img, renderer->getWidth(), renderer->getHeight());

	/*
	static float* diffBuffer = 0;
	if (!diffBuffer) diffBuffer = new float[4*renderer->getWidth()*renderer->getHeight()];
	for (int i=0; i<4*renderer->getWidth()*renderer->getHeight(); i++) diffBuffer[i] = waveletBuffer[i] - renderer->getBuffer()[i];
	*/

	generateRenderTexture(waveletBuffer, renderer->getWidth(), renderer->getHeight());
}

void MainWindow::terminate()
{
	WindowBase::terminate();
}

int main(int argc, char* argv[])
{
	std::string filename = "scenes/cornellBox.pbrt";
	PBRTLoader pbrtLoader;
	int sampleNum = 0, bounceDepth = 0;
	int width = 0, height = 0;
	float fov = 0;
	bool showVariance = false;
	VarianceRenderer* varianceRenderer = NULL;

	for (int i=1; i<argc; i++)
	{
		if(argv[i][0] == '-')
		{
			if(argv[i][1] == 's')		sampleNum 	= std::stoi(argv[i][2] == '\0' ? argv[++i] : argv[i]+2);
			else if (argv[i][1] == 'b')	bounceDepth = std::stoi(argv[i][2] == '\0' ? argv[++i] : argv[i]+2);
			else if (argv[i][1] == 'w')	width 		= std::stoi(argv[i][2] == '\0' ? argv[++i] : argv[i]+2);
			else if (argv[i][1] == 'h')	height 		= std::stoi(argv[i][2] == '\0' ? argv[++i] : argv[i]+2);
			else if (!strncmp(argv[i]+1, "fov", 3))	fov = std::stof(argv[i][4] == '\0' ? argv[++i] : argv[i]+4);
			else if (argv[i][1] == 'v')	showVariance = true;
			else std::cout << "Usage: ./simplePBRT <PBRT_scene_file>"
					" -s <sample_num_int> -b <bounce_depth_int>"
					" -w <film_width_int> -h <film_height_int>"
					" -fov <fov> [-v (show approximated variance)]\n", exit(0);
		}
		else filename = argv[i];
	}

	if(!pbrtLoader.loadScene(filename))
		exit(0);
	PBRTSceneInfo& sceneInfo = pbrtLoader.loaderInfo.sceneInfo;

	camera 		= sceneInfo.camera;
	scene 		= sceneInfo.scene;
	renderer 	= sceneInfo.renderer;
	accelerator = sceneInfo.accelerator;


	cout << "Scene loaded. Containing " << scene->getTriangleCount()/1000.f << "K triangles." << endl;
	cout << endl;

	int frameWidth, frameHeight;

	if(sampleNum) 	renderer->setSampleNum(sampleNum);
	if(bounceDepth)	renderer->setBounceDepth(bounceDepth);
	if(width) {frameWidth = width; if (!height) frameHeight = (float) width / camera->getAspectRatio() ; }
	if(height){frameHeight = height; if (!width) frameWidth = (float) height * camera->getAspectRatio() ; }
	if(width || height)
	{
		renderer->createBuffer(frameWidth, frameHeight);
		camera->setAspectRatio((float)renderer->getWidth()/(float)renderer->getHeight());
		camera->update();
	}
	//if(fov)camera->setFOV(float2(radians(fov)));

	cout << renderer->getWidth() << "x" << renderer->getHeight() << " (" << camera->getAspectRatio() << ")\n\n";

	// init Window
	MainWindow* displayWindow = new MainWindow(filename);
	if(showVariance)
	{
		varianceRenderer = new VarianceRenderer;
		varianceRenderer->setBaseRenderer(renderer);
		renderer = varianceRenderer;
		displayWindow->setGamma(1.f);
	}
	displayWindow->loop();
	cout << endl;

	// Terminate
	displayWindow->terminate();
	delete displayWindow;
	delete varianceRenderer;//delete will automaticlly ignore pointer that is equal to NULL.

	return 0;
}
