# SimpleRay #

SimpleRay is a simple [raytracer](https://en.wikipedia.org/wiki/Ray_tracing_(graphics)) for generating images of scenes based on physical principles. This project originated during the [6th Hackathon Taiwan](https://hackathon.tw/) (April 11-12, 2015).

## Outline ##

SimpleRay loads a scene consisting of light sources and reflecting surfaces defined by triangles. The scene is sampled by tracing rays from the camera, which are handled in parallel using OpenMP.

## Building and running the project ##

SimpleRay can be easily configured and built using [CMake](https://cmake.org/).
It has [SDL](https://www.libsdl.org/), [GLM](http://glm.g-truc.net/), [Embree](https://github.com/embree/embree)(If compiled on x86 platform) as dependencies.  
Also, please use a OpenMP compatible compiler for optimal perfoemace.

If you are compiling for ARM64/Aarch64 platforms. There is a [experimental port of Embree](https://github.com/marty1885/embree-arm) for it. Note that it won't work
for 32 bit ARM platforms.

### Building on Linux ###
On Linux, please ensure all dependencies are installed. Then build the project,

```shell
cmake .
make
```

### Building on Windows ###

On Windows, the [MinGW](http://sourceforge.net/projects/mingw-w64/) compiler can be used to build the project. For example,

```shell
cmake -G "MinGW Makefiles" .
mingw32-make
```

MSYS can also be used instead of raw MinGW
```shell
cmake -G "MSYS Makefiles" .
make
```

### Building on OS X ###
It's same as building on Linux. Please install all dependencies and execute the following command:
```shell
cmake .
make
```

### Running the binaries ###

* "WindowExample" is a simple SDL window showing a generated texture.

* "simplePBRT" is a program that loads and renders PBRTv2 scene files. See `simplePBRT -i` for more infomation.

## See SimpleRay in action ##
#### SmallPT Styled Box
![SmallPT styled box](https://bitbucket.org/repo/4rngpp/images/790493619-20.%20SmallPT%20styled%20box%205000SPP.png)

#### Environment Map
![Enviroent Map](https://bitbucket.org/repo/4rngpp/images/2902566064-30.%20Enviroent%20Map!!%202454SPP.png)

#### Sponza
![Sponza](https://bitbucket.org/repo/4rngpp/images/1992158794-%E6%9C%AA%E5%91%BD%E5%90%8D3.png)
