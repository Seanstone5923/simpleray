#ifndef GLSLSOURCE_H
#define GLSLSOURCE_H

namespace Window
{
//FIXME: Gamma correction using Intel/AMD/nvidia/(and maby other) GPUs make weird color in daek reagons

#ifdef __ANDROID__

const char basic2DVertexShaderSource[] =
"attribute vec2 position; \n\
\n\
varying vec2 textureCoord;\n\
\n\
void main()\n\
{\n\
	gl_Position = vec4(position,0,1.0);\n\
	textureCoord = position*0.5 + vec2(0.5);\n\
}\n\n";

const char gammaCorrectionFragmantShaderSource[] =
"precision mediump float; \n\
\n\
varying vec2 textureCoord;\n\
uniform sampler2D textureSampler;\n\
uniform float gamma;\n\
\n\
void main()\n\
{\n\
	vec4 uncorrectedColor = texture2D(textureSampler,vec2(textureCoord.x,-textureCoord.y));\n\
	float inverseGamma = 1.0f/gamma;\n\
	vec4 correctedColor = pow(uncorrectedColor,vec4(inverseGamma,inverseGamma,inverseGamma,0));\n\
	gl_FragColor = correctedColor;\n\
}\n\n";

#else

const char basic2DVertexShaderSource[] =
"#version 330 core\n\
\n\
layout (location = 0) in vec2 position;\n\
\n\
out vec2 textureCoord;\n\
\n\
void main()\n\
{\n\
	gl_Position = vec4(position,0,1.0);\n\
	textureCoord = position*0.5 + vec2(0.5);\n\
}\n\n";

const char gammaCorrectionFragmantShaderSource[] =
"#version 330 core\n\
out vec4 outColor;\n\
\n\
in vec2 textureCoord;\n\
uniform sampler2D textureSampler;\n\
uniform float gamma;\n\
\n\
void main()\n\
{\n\
	vec4 uncorrectedColor = texture(textureSampler,vec2(textureCoord.x,-textureCoord.y));\n\
	float inverseGamma = 1.0f/gamma;\n\
	vec4 correctedColor = pow(uncorrectedColor,vec4(inverseGamma,inverseGamma,inverseGamma,0));\n\
	outColor = correctedColor;\n\
}\n\n";

#endif

}

#endif
