typedef struct _Ray
{
	float4 origin;
	float4 direction;
} Ray;

typedef struct _Camera
{
	float4 position;
	float4 direction;
	float4 right;
	float4 up;
	int width;
	int height;
} Camera;

typedef struct _PointLight
{
	float4 position;
	float strength;
} PointLight;

typedef struct _Triangle
{
	float4 vertex[3];
} Triangle;

typedef struct _RayHit
{
	float distanceBwt;
	float u,v;
	int index;
} RayHit;

void printFloat4(float4 vec)
{
	/*printf("%.2f %.2f %.2f %.2f\n",dot(vec,(float4)(1.0f,0.0f,0.0f,0.0f)),
	dot(vec,(float4)(0.0f,1.0f,0.0f,0.0f)),
	dot(vec,(float4)(0.0f,0.0f,1.0f,0.0f)),
	dot(vec,(float4)(0.0f,0.0f,0.0f,1.0f)));*/
}

Triangle createTriangle(float4 a, float4 b, float4 c)
{
	Triangle triangle;
	triangle.vertex[0] = a;
	triangle.vertex[1] = b;
	triangle.vertex[2] = c;
	return triangle;
}

inline float4 getTriangleNormal(Triangle triangle)
{
	return cross(triangle.vertex[2]-triangle.vertex[0],triangle.vertex[1] - triangle.vertex[0]);
}

float findIntersection(Ray ray, Triangle triangle, float4* intersection)
{
	float4 rayCone[3];
	float4 cameraConeNormal[3];
	rayCone[0] = triangle.vertex[0] - ray.origin;
	rayCone[1] = triangle.vertex[1] - ray.origin;
	rayCone[2] = triangle.vertex[2] - ray.origin;
	cameraConeNormal[0] = cross(rayCone[1], rayCone[2]);
	cameraConeNormal[1] = cross(rayCone[2], rayCone[0]);
	cameraConeNormal[2] = cross(rayCone[0], rayCone[1]);
	//cameraConeNormal[4] = (float4)(0.0f,0.0f,0.0f,0.0f);
	float orientation = dot(rayCone[0], cameraConeNormal[0]) < 0 ? 1 : -1;
	float4 baryocentric = orientation * (float4)(
		dot(ray.direction, cameraConeNormal[0]),
		dot(ray.direction, cameraConeNormal[1]),
		dot(ray.direction, cameraConeNormal[2]),
		0.0f
	);
	if (baryocentric.x <= .0f && baryocentric.y <= .0f && baryocentric.z <= .0f)
	{
		float4 intersec = (baryocentric.x*triangle.vertex[0] +
			baryocentric.y*triangle.vertex[1] +
			baryocentric.z*triangle.vertex[2])
			/(baryocentric.x + baryocentric.y + baryocentric.z);

		if(intersection != 0)
			*intersection = intersec;
		return length(intersec - ray.origin);
	}
	else
		return -1;
}

float findIntersectionWorld(Ray ray,__global float4* vertices, __global int* indecies, float4* intersection,
	 int indeciesCount, int* index)
{
	float4 intersec;
	float nearestDistance = -1;

	for(int i=0;i<indeciesCount;)
	{
		Triangle triangle = createTriangle(vertices[indecies[i]],vertices[indecies[i+1]],
			vertices[indecies[i+2]]);
		float len = findIntersection(ray,triangle,&intersec);
		if(len > 0 && (len < nearestDistance || nearestDistance < 0))
		{
			nearestDistance = len;
			if(intersection != 0)
				*intersection = intersec;
			if(index != 0)
				*index = i/3;
		}
		i += 3;
	}
	return nearestDistance;
}

__kernel void raycast(__global Ray* rays, __global RayHit* rayHit, __global int* rayNum, __global float4* vertices,
	 __global int* indecies, __global int* indeciesCount)
{
	int id = get_global_id(0);
	int size = get_global_size(0);

	for(int i=id;i<*rayNum;)
	{
		float4 intersection;
		int index = -1;
		float nearestDistance = findIntersectionWorld(rays[i],vertices,indecies,&intersection,*indeciesCount,&index);
		if(nearestDistance > 0)
		{
			rayHit[i].index = index;
			rayHit[i].distanceBwt = nearestDistance;
		}
		else
			rayHit[i].index = -1;
		i+=size;
	}
}

__kernel void raytrace(__global float4* buffer, __global Camera* cam, __global float4* vertices, __global int* indecies,
	__global int* indeciesCount)
{
	int id = get_global_id(0);
	int size = get_global_size(0);
	int2 bufferSize = (int2)(cam->width,cam->height);
	Camera camera = *cam;

	for(int i = id;i < bufferSize.x*bufferSize.y;)
	{
		int x = i % camera.width;
		int y = i / camera.width;

		float4 rayVec = (convert_float(x)/convert_float(camera.width)-0.5f)*camera.right
			+ (0.5f-convert_float(y)/convert_float(camera.height))*camera.up
			+ camera.direction;
		Ray ray;
		ray.origin = camera.position;
		ray.direction = rayVec;
		float4 intersection;
		float nearestDistance = findIntersectionWorld(ray,vertices,indecies,&intersection,*indeciesCount,0);
		if(nearestDistance > 0)
		{
			PointLight light;
			light.position = (float4)(0.0f,3.0f,0.0f,0.0f);
			light.strength = 20;
			Ray reverseLightRay;
			reverseLightRay.origin = intersection + (light.position-intersection)*0.0001f;
			reverseLightRay.direction = light.position-intersection;
			//If anything is between us and our light
			if(findIntersectionWorld(reverseLightRay,vertices,indecies,0,*indeciesCount,0) < 0)
			{
				float len = length(light.position-intersection);
				float albedo = 1/(len*len);
				albedo *= light.strength;
				albedo *= dot((float4)(0.0f,1.0f,0.0f,0.0f),normalize(light.position-intersection));
				buffer[i] = (float4)(1.0f,1.0f,1.0f,1.0f)*albedo;
				//buffer[i] += (float4)(0.1f,1.0f,0.1f,0.0f);
			}
			else
				buffer[i] = (float4)(0.0f,0.0f,0.0f,1.0f);
		}
		else
			buffer[i] = (float4)(0.0f,0.0f,0.0f,1.0f);
		buffer[i].w = 1.0f;
		//buffer[i] = (float4)(1.0f,0.0f,0.0f,1.0f);
		i += size;
	}
}
