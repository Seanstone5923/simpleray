#include "Window.hpp"
#include <string>

class MainWindow : public DisplayWindow
{
public:
	MainWindow(std::string title);
	void mouseDownEvent(Window::Button button);
	void resizeEvent(int width, int height);
	void keyDownEvent(Window::Key key);
	void renderEvent();
	void terminate();
    void main();
};
