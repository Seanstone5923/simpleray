#include "simplePBRT.hpp"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <iostream>
#include <fstream>
#include <algorithm>
using namespace std;
#include <chrono>
using std::cout;
using std::endl;
using namespace std::chrono;

#include "Quaternion.hpp"
#include "Incarnate.hpp"
#include "Accelerator.hpp"
#include "Renderer.hpp"
#include "Camera.hpp"
#include "Scene.hpp"
#include "PBRTLoader.hpp"
using namespace Incarnate;

Camera* 	camera;
Scene* 		scene;
Renderer* 	renderer;
Accelerator* accelerator;
string saveFileName;
Quatf Rotor = Quatf(1.0);
bool allowUserInput = true;

void MainWindow::mouseDownEvent(Window::Button button)
{
	if(!allowUserInput)
		return;

	// Rotate
	if (button ==  Window::Button::Button_Right)
	{
		int width = getWidth(), height = getHeight();
		int u0 = width/2, v0 = height/2;

		// Get mouse position
		int x,y;
		getCursorPos(&x, &y);
		double u = x, v = y;

		// Compute new orientation
		Quatf rotateVector = (u0-u) * Quatf(camera->up) + (v0-v) * Quatf(camera->right);
		Quatf rotor = exp(2e-4 * rotateVector);

		camera->direction =  rotor * camera->direction * ~rotor;
		float4 globalUp(0,1,0,0);
		float4 right = normalize(cross(camera->direction, globalUp));
		float4 up = normalize(cross(right, camera->direction));
		camera->up = up;
		camera->right = right;

		camera->update();
		renderer->clearBuffer();
	}
}

void MainWindow::resizeEvent(int width, int height)
{
	if(!allowUserInput)
		return;

	renderer->createBuffer(width, height);
	camera->setAspectRatio((float)width/height);
	camera->update();
	renderer->clearBuffer();
}

void MainWindow::keyDownEvent(Window::Key key)
{
	if(!allowUserInput)
		return;
	const static vector<int> camKey = {Window::Key_A, Window::Key_Z, Window::Key_Right
		, Window::Key_Left, Window::Key_Up, Window::Key_Down};


	if(find(camKey.begin(),camKey.end(),key) != camKey.end())
	{
		if (key == Window::Key_Escape)
			exit(0);
		else if(key == Window::Key_A)
			camera->position += normalize(camera->direction)*0.1f;
		else if(key == Window::Key_Z)
			camera->position -= normalize(camera->direction)*0.1f;
		else if(key == Window::Key_Right)
			camera->position += normalize(camera->right)*0.1f;
		else if(key == Window::Key_Left)
			camera->position -= normalize(camera->right)*0.1f;
		else if(key == Window::Key_Up)
			camera->position += normalize(camera->up)*0.1f;
		else if(key == Window::Key_Down)
			camera->position -= normalize(camera->up)*0.1f;
		camera->update();
		renderer->clearBuffer();
	}
	else
	{
		if(key == Window::Key_S)
		{
			int width = renderer->getWidth(), height = renderer->getHeight();
			HDRImage img;
			img.load(renderer->getBuffer());
			img.save(saveFileName);
		}
	}
}

MainWindow::MainWindow(std::string title)
{
	#ifdef __ANDROID__
		createWindow();
	#else
		createWindow(("SimpleRay - "+title).c_str(), renderer->getWidth(), renderer->getHeight());
	#endif
}

void MainWindow::renderEvent()
{
	static high_resolution_clock::time_point startTime = high_resolution_clock::now();
	static high_resolution_clock::time_point lastRenderTime = high_resolution_clock::now();
	if(renderer->getCurrentSampleCount() < renderer->getSampleNum() || renderer->getSampleNum() < 0)
	{
		renderer->render();
		high_resolution_clock::time_point endRenderTime = high_resolution_clock::now();

		//Clear line
		fputs("\033[A\033[2K",stdout);

		int currentSampleCount = renderer->getCurrentSampleCount();
		uint64_t samplePerFrame = ((uint64_t)renderer->getWidth()*(uint64_t)renderer->getHeight());
		double megaSamplesPerFrame = samplePerFrame/1000.0/1000.0;

		if(renderer->getSampleNum() < 0)
			cout << "Rendered " << currentSampleCount << " samples ";
		else
			cout << "Rendered " << currentSampleCount << "/"
				<< renderer->getSampleNum() << ". "
				<< (float)currentSampleCount*100.0f/renderer->getSampleNum() << "% ";

		if(allowUserInput)
		{
			double elapsed = duration_cast<duration<double>>(endRenderTime - lastRenderTime).count();
			cout << ", " << megaSamplesPerFrame/elapsed << " MSamples/s, " << 1.f/elapsed << " FPS\n";
		}
		else
		{
			double elapsed = duration_cast<duration<double>>(endRenderTime - startTime).count();
			cout << "elapsed: " << elapsed << "s, " <<
				megaSamplesPerFrame*(uint64_t)currentSampleCount/elapsed<< " MSamples/s\n";
		}
		lastRenderTime = endRenderTime;
		fflush(stdout);//Force update to stdout
		rewind(stdout);//Reset cursor position

		generateRenderTexture(
			renderer->getBuffer(),
			renderer->getWidth(), renderer->getHeight());
	}
}

void MainWindow::terminate()
{
	WindowBase::terminate();
}

#ifndef __ANDROID__
int main(int argc, char* argv[])
#else
int main()
#endif
{
	#ifdef __ANDROID__
		std::string filename = "/sdcard/scenes/cornellBox.pbrt";
	#else
		std::string filename = "scenes/cornellBox.pbrt";
	#endif

	PBRTLoader pbrtLoader;
	int sampleNum = 0, bounceDepth = 0;
	int width = 0, height = 0;
	bool showVariance = false;
	VarianceRenderer* varianceRenderer = nullptr;

	#ifndef __ANDROID__
	//A hacky commandline parser
	for (int i=1; i<argc; i++)
	{
		if(argv[i][0] == '-')
		{
			if(argv[i][1] == 's')		sampleNum 	= std::stoi(argv[i][2] == '\0' ? argv[++i] : argv[i]+2);
			else if (argv[i][1] == 'b')	bounceDepth = std::stoi(argv[i][2] == '\0' ? argv[++i] : argv[i]+2);
			else if (argv[i][1] == 'w')	width 		= std::stoi(argv[i][2] == '\0' ? argv[++i] : argv[i]+2);
			else if (argv[i][1] == 'h')	height 		= std::stoi(argv[i][2] == '\0' ? argv[++i] : argv[i]+2);
			else if (!strncmp(argv[i]+1, "no-edit", 7))	allowUserInput = false;
			else if (argv[i][1] == 'v')	showVariance = true;
			else std::cout << "Usage: ./simplePBRT <PBRT_scene_file>"
					" -s <sample_num_int> -b <bounce_depth_int>"
					" -w <film_width_int> -h <film_height_int>"
					" -fov <fov> [-v (show approximated variance) "
					"-no-edit (disable moving camera and saving image via keyboard)]\n\n"
					"Keys:\n"
					"\tUp, Down, Left, Right: move camera\n"
					"\tA, Z : Zoom in/out\n"
					"\tS : Save current frame\n", exit(0);
		}
		else filename = argv[i];
	}
	#endif

	if(!pbrtLoader.loadScene(filename))
		exit(0);

	camera 		= pbrtLoader.getCamera();
	scene 		= pbrtLoader.getScene();
	renderer 	= pbrtLoader.getRenderer();
	accelerator	= pbrtLoader.getAccelerator();
	saveFileName = pbrtLoader.loaderInfo.filmInfo.filename;

	std::string fileExt = saveFileName.substr(saveFileName.find_last_of(".") + 1);
	//HACK: Only allow formats supports by Incarnate. Else use png
	if(fileExt != "png" && fileExt != "bmp" && fileExt != "tga" && fileExt != "hdr")
		saveFileName += ".png";

	cout << "Scene loaded. Containing " << scene->getTriangleCount()/1000.f << "K triangles." << endl;
	cout << endl;

	int frameWidth, frameHeight;

	if(sampleNum) 	renderer->setSampleNum(sampleNum);
	if(bounceDepth)	renderer->setBounceDepth(bounceDepth);
	if(width) {frameWidth = width; if (!height) frameHeight = (float) width / camera->getAspectRatio() ; }
	if(height){frameHeight = height; if (!width) frameWidth = (float) height * camera->getAspectRatio() ; }
	if(width || height)
	{
		renderer->createBuffer(frameWidth, frameHeight);
		camera->setAspectRatio((float)renderer->getWidth()/(float)renderer->getHeight());
		camera->update();
	}

	// init Window
	MainWindow* displayWindow = new MainWindow(filename);

	#ifdef __ANDROID__
		glClearColor(0.f, 1.f, 1.f, 1.f);
		displayWindow->clear();
	#endif

	// Renderer
	accelerator->build();
	renderer->initRender();

	displayWindow->setGamma(2.2f);

	if(showVariance)
	{
		varianceRenderer = new VarianceRenderer;
		varianceRenderer->setBaseRenderer(renderer);
		renderer = varianceRenderer;
		displayWindow->setGamma(1.f);
	}
	displayWindow->loop();
	cout << endl;

	// Terminate
	displayWindow->terminate();
	delete displayWindow;
	delete varianceRenderer;//delete will automaticlly ignore pointer that is NULL.

	return 0;
}
