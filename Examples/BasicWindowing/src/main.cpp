#include <stdlib.h>
#include <stdio.h>

#include <Window.hpp>

class MainWindow : public DisplayWindow
{
public:
	void resizeEvent(int width, int height);
	void mouseDownEvent(Window::Button button);
	void keyDownEvent(Window::Key key);
};

void MainWindow::resizeEvent(int width, int height)
{
	printf("size %d,%d\n", width, height);
}

void MainWindow::mouseDownEvent(Window::Button button)
{
	if(button == Window::Button_Right)
		exit(0);
}

void MainWindow::keyDownEvent(Window::Key key)
{
	if(key == Window::Key::Key_Q)
	exit(0);
}

int main()
{
	unsigned char* buffer = new unsigned char[1200*800*4];
	for(int i=0;i<1200*800;i++)
	{
		buffer[i*4+0] = 255;
		buffer[i*4+1] = 127;
		buffer[i*4+2] = 40;
		buffer[i*4+3] = 255;
	}

	for(int i=0;i<70*800;i++)
	{
		buffer[i*4+0] = 140;
		buffer[i*4+1] = 127;
		buffer[i*4+2] = 80;
		buffer[i*4+3] = 255;
	}

	MainWindow window;
	window.createWindow("SimpleRay - Basic Windowing", 1200, 800);
	window.generateRenderTexture(buffer, 1200, 800);

	// window.setMousePressHandler(mouseHandle);
	// window.setKeyHandler(keyHandle);
	// window.setResizeHandler(resizeHandle);

	window.loop();
	window.terminate();

	delete [] buffer;
	return 0;
}
