#include <Mesh.hpp>

using namespace Incarnate;

Mesh::Mesh()
{
}

Mesh::~Mesh()
{
}

void Mesh::setVertices(const float4* verts, int num)
{
	vertices.createBuffer(sizeof(float4)*num);
	float4* buffer = (float4*)vertices.getBuffer();

	for(int i=0;i<num;i++)
		buffer[i] = verts[i];
	dataBufferArray[0] = &vertices;
}

void Mesh::setIndices(const int* inds, int num)
{
	indices.createBuffer(sizeof(int)*num);
	int* buffer = (int*)indices.getBuffer();

	for(int i=0;i<num;i++)
		buffer[i] = inds[i];
	dataBufferArray[1] = &indices;
}

void Mesh::setNormals(const float4* norms, int num)
{
	normals.createBuffer(sizeof(float4)*num);
	float4* buffer = (float4*)normals.getBuffer();

	for(int i=0;i<num;i++)
		buffer[i] = norms[i];
	dataBufferArray[2] = &normals;
}

void Mesh::setUVs(const float2* uvPtr, int num)
{
	uvs.createBuffer(sizeof(float2)*num);
	float2* buffer = (float2*)uvs.getBuffer();

	for(int i=0;i<num;i++)
		buffer[i] = uvPtr[i];
	dataBufferArray[3] = &uvs;
}

void Mesh::setMaterial(Material* materialPtr)
{
	material = materialPtr;
}

DataBufferArray* Mesh::getDataBufferArray()
{
	return &dataBufferArray;
}
