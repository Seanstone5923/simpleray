#include <stb_image.h>
#include <stb_image_write.h>

#include <Texture.hpp>
using namespace Incarnate;

#include <iostream>
#include <string.h>

int Image::getWidth() const
{
	return width;
}

int Image::getHeight() const
{
	return height;
}

bool Image::save(const std::string& path, float gamma) const
{

	if(!good())
	{
		std::cout << "No image is in the buffer! can't save image." << std::endl;
		return false;
	}
	std::string fileExt = path.substr(path.find_last_of(".") + 1);
	if(fileExt == "hdr")
		return saveHDR(path,gamma);
	else if(fileExt != "png" && fileExt != "bmp" && fileExt != "tga")
	{
		std::cout << "Error : Image format *" << fileExt << "* not supported." << std::endl << std::endl;
		return false;
	}

	const float inversedGamma = 1.f/gamma;
	unsigned char* imageData = new unsigned char[width*height*4];
	for(int i=0;i<height;i++)
	{
		for(int j=0;j<width;j++)
		{
			int index = i*width+j;
			float4 pixVal = glm::pow(getPixel(j,i),float4(inversedGamma))*255.f;
			pixVal = glm::min(pixVal, float4(255));
			pixVal.a = 255.f;
			for(int k=0;k<4;k++)
				imageData[(i*width+j)*4+k] = pixVal[k];
		}
	}

	int success = 0;
	if(fileExt == "png")
		success = stbi_write_png(path.c_str(), width, height, 4, imageData, width*4);
	else if(fileExt == "bmp")
		success = stbi_write_bmp(path.c_str(), width, height, 4, imageData);
	else if(fileExt == "tga")
		success = stbi_write_tga(path.c_str(), width, height, 4, imageData);
	delete [] imageData;

	if(success == 0)
	{
		std::cout << "Error : failed to save image as *" << path << "*." << std::endl << std::endl;
		return false;
	}
	return true;
}

bool Image::saveHDR(const std::string& path, float gamma) const
{
	constexpr float preGamma = 2.2f;
	const float inversedGamma = preGamma/gamma;
	float4* imageData = new float4[width*height*4];
	for(int i=0;i<height;i++)
	{
		for(int j=0;j<width;j++)
		{
			int index = i*width+j;
			float4 pixVal = glm::pow(getPixel(j,i),float4(inversedGamma));
			pixVal.a = 1;
			imageData[i*width+j] = pixVal;
		}
	}
	int success = 0;
	success = stbi_write_hdr(path.c_str(),  width, height, 4, (float*)imageData);
	delete [] imageData;

	if(success == 0)
	{
		std::cout << "Error : failed to save image as *" << path << "*." << std::endl << std::endl;
		return false;
	}
	return true;
}

bool Image::load(const std::string& path, float gamma)
{
	return false;
}


bool HDRImage::load(const std::string& path, float gamma)
{
	//Clear any previous data. Although in any good programming. This shouldn't happen
	if(buffer != nullptr)
		unload();

	int comp = 0;
	float* imageData = stbi_loadf(path.c_str(), &width, &height, &comp, 4);

	if(imageData == nullptr)
	{
		std::cout << "Filed to load image " << path << std::endl;
		return false;
	}

	create(width, height);
	for(int i=0;i<height;i++)
	{
		for(int j=0;j<width;j++)
		{
			float4 pixVal = float4(imageData[4*(i*width+j)+0],imageData[4*(i*width+j)+1]
				,imageData[4*(i*width+j)+2],imageData[4*(i*width+j)+3]);

			//Flip the image verticaly. Acculding to the OpenGL convention, the origin is at bottom-left.
			//But STBI assumes the origin is at the top-left.
			// /2.2 because stbi_loadf by default turns everything into linear rgb.
			buffer[(height-i-1)*width+j] = glm::pow(pixVal,float4(gamma/2.2,gamma/2.2,gamma/2.2,1));
		}
	}

	stbi_image_free(imageData);
	return true;
}

bool HDRImage::load(const float4* data, int w, int h)
{
	if(buffer != nullptr)
		unload();
	width = w;
	height = h;
	create(width, height);
	memcpy(buffer,data,width*height*sizeof(float4));
	return true;
}

bool HDRImage::load(const Film* data)
{
	create(data->width, data->height);
	for(int i=0;i<width*height;i++)
		buffer[i] = float4(data->buffer[i].color,1);
}

void HDRImage::unload()
{
	delete [] buffer;
	width = 0;
	height = 0;
	buffer = nullptr;
}

vec4* HDRImage::getRaw()
{
	return buffer;
}

bool HDRImage::good() const
{
	if(buffer != nullptr)
		return true;
	return false;
}

float4 HDRImage::getPixel(int x, int y) const
{
	x = clamp(x, 0, width-1);
	y = clamp(y, 0, height-1);
	return buffer[y*width+x];
}

HDRImage::HDRImage()
{
}

HDRImage::HDRImage(const std::string& path)
{
	load(path);
}

HDRImage::~HDRImage()
{
	unload();
}

void HDRImage::create(int w, int h)
{
	if(buffer)
		unload();
	width = w;
	height = h;
	buffer = new float4[width*height];
}

LDRImage::LDRImage()
{
}

LDRImage::LDRImage(const std::string& path)
{
	load(path);
}

bool LDRImage::load(const std::string& path, float gamma)
{
	HDRImage hdrImage;
	hdrImage.load(path, gamma);
	if(!hdrImage.good())
		return false;

	int w = hdrImage.getWidth();
	int h = hdrImage.getHeight();
	create(w, h);
	for(int i=0;i<h;i++)
	{
		for(int j=0;j<w;j++)
		{
			float4 color = hdrImage.getPixel(j,i)*255.f;
			color = glm::clamp(color,float4(0),float4(255));
			buffer[(i*w+j)*4+0] = color.r;
			buffer[(i*w+j)*4+1] = color.g;
			buffer[(i*w+j)*4+2] = color.b;
			buffer[(i*w+j)*4+3] = color.a;
		}
	}
	return true;
}

bool LDRImage::load(const unsigned char* data, int w, int h)
{
	if(buffer != nullptr)
		unload();
	width = w;
	height = h;
	create(width, height);
	memcpy(buffer,data,width*height*sizeof(unsigned char));
	return true;
}

float4 LDRImage::getPixel(int x, int y) const
{
	x = clamp(x, 0, width-1);
	y = clamp(y, 0, height-1);
	float4 val(
		buffer[(y*width+x)*4+0],
		buffer[(y*width+x)*4+1],
		buffer[(y*width+x)*4+2],
		buffer[(y*width+x)*4+3]
	);
	return val/255.f;
}

bool LDRImage::good() const
{
	if(buffer)
		return true;
	return false;
}

void LDRImage::unload()
{
	delete [] buffer;
	width = 0;
	height = 0;
	buffer = nullptr;
}

unsigned char* LDRImage::getRaw()
{
	return buffer;
}

void LDRImage::create(int w, int h)
{
	if(!buffer)
		unload();
	width = w;
	height = h;
	buffer = new unsigned char[width*height*4];
}

LDRImage::~LDRImage()
{
	unload();
}

float2 Texture::wrapUV(const float2& uv) const
{
	switch (uvWrapMode)
	{
		case UVWrapMode::Repeat:
		{
			return glm::fract(uv);
			break;
		}
		case UVWrapMode::MirroredRepeat:
		{
			float2 vec = glm::fract(uv);
			int2 block = uv-vec;
			vec = float2((block.x%2==0?vec.x:1.f-vec.x),(block.y%2==0?vec.y:1.f-vec.y));
			return vec;
			break;
		}
		case UVWrapMode::Clamp:
		default:
		{
			return clamp(uv,float2(0,0),float2(0.99999f));
			break;
		}
	}
	return float2(0);
}

float4 Texture::getPixel(float2 uv)
{
	uv = wrapUV(uv);
	int imageWidth = image->getWidth();
	int imageHeight = image->getHeight();

	switch (filteringMethod)
	{
		case TextureFilteringMethod::Bilinear:
		{
			float2 texCentCoord = uv*float2(imageWidth,imageHeight) - float2(0.5);
			int x = texCentCoord.x;
			int y = texCentCoord.y;
			int2 texVexCoord = int2(texCentCoord);
			float2 texCoord = texCentCoord;
			float2 ratio = texCoord - float2(texVexCoord);
			float2 oppositeRatio = float2(1.f) - ratio;
			float4 pixels[4] = {image->getPixel(x,y), image->getPixel(x+1,y)
				, image->getPixel(x,y+1), image->getPixel(x+1,y+1)};
			float4 interpolate[2] = {pixels[0]*oppositeRatio.x + pixels[1]*ratio.x
				, pixels[2]*oppositeRatio.x + pixels[3]*ratio.x};
			float4 result = interpolate[0] * oppositeRatio.y
				+ interpolate[1] * ratio.y;
			return result;
			break;
		}

		case TextureFilteringMethod::Nearest:
		default:
		{
			int xCoord = imageWidth*uv.x;
			int yCoord = imageHeight*uv.y;
			return image->getPixel(xCoord, yCoord);
		}
	}
	//Should not execute the line of code
	return float4(0,0,0,1);
}

void Texture::setImage(Image* img)
{
	image = img;
}

void Texture::setFilteringMethod(TextureFilteringMethod method)
{
	filteringMethod = method;
}

TextureFilteringMethod Texture::getFilteringMethod() const
{
	return filteringMethod;
}

void SphereMap::setTexture(Texture* tex)
{
	texture = tex;
}

Texture* SphereMap::getTexture()
{
	return texture;
}

float4 SphereMap::getPixel(float4 direction) const
{
	float2 uv;
	const float4& v = direction;
	uv.x = 0.5f - atan2f(v.x, v.z)*(float)M_1_PI*0.5f;
	uv.y = -acosf(v.y)*(float)M_1_PI;
	return texture->getPixel(uv);
}
