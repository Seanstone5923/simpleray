#include <string.h>

#include <Film.hpp>
using namespace Incarnate;

Film::Film() : Film(480,360)
{
}

Film::Film(int w, int h)
{
	resize(w, h);
}

Film::~Film()
{
	distory();
}

void Film::resize(int w, int h)
{
	buffer = new FilmElement[w*h];
	width = w;
	height = h;
}

void Film::distory()
{
	delete [] buffer;
	width = 0;
	height = 0;
}

bool Film::good() const
{
	return buffer != nullptr;
}

void Film::clear()
{
	if(good())
		memset(buffer,0,width*height*sizeof(FilmElement));
}
