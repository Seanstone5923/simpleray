#include <Incarnate.hpp>
#include <Ray.hpp>
#include <AABB.hpp>
#include <Triangle.hpp>
#include <Vec.hpp>
using namespace Incarnate;

#include <random>

#include <3rdparty/PCG/pcg_random.hpp>

namespace Incarnate
{
	//some helper class to generate random number
	//decleared as static here to not messsup stuff outside
	static pcg32 generator(pcg_extras::seed_seq_from<std::random_device>{});
	static std::uniform_real_distribution<float> distr(0,1);
}

//XXX: This causes cache invalidation and race condition under multi-thread
float Incarnate::rand1()
{
	return distr(generator);
}

float Incarnate::findIntersection(const Ray& ray, const Triangle& triangle, float4* uv)
{
	float4 v0v1 = triangle.vertex[1] - triangle.vertex[0];
	float4 v0v2 = triangle.vertex[2] - triangle.vertex[0];
	float4 pvec = float4(cross(ray.direction,float3(v0v2)),0);
	float det = dot(v0v1,pvec);
	float u, v, t;

	// ray and triangle are parallel if det is close to 0
	//This is a bad idea. Alot of times we we find the if() statement is true
	// but the triangle should be displayed.
	// if(det == 0.0)
	//  	return -1;

	float invDet = 1 / det;

	float4 tvec = float4(ray.origin,0) - triangle.vertex[0];
	u = dot(tvec, pvec) * invDet;
	if(u < 0 || u > 1)
		return -1;

	float4 qvec = cross(tvec, v0v1);
	v = dot(ray.direction, float3(qvec)) * invDet;
	if(v < 0 || u + v > 1)
		return -1;

	t = dot(v0v2, qvec) * invDet;

	if(t > 0)
	{
		if(uv != nullptr)
			*uv = float4(u, v, 0.0f, 0.0f);
	}

	return t;
}

float Incarnate::findIntersection(Ray ray, AABB aabb)
{
	int sign[3];
	float3 inverseDirection = ray.inverseDirection();
	ray.calcSign(sign);
	return findIntersection(ray, sign, inverseDirection, aabb);
}

float Incarnate::findIntersection(const Ray& ray,int* sign,const float3& inverseDirection, const AABB& aabb)
{
	float tmin, tmax, tymin, tymax, tzmin, tzmax;
	const float3* bounds = &(aabb.bounds[0]);

	tmin = (bounds[sign[0]].x - ray.origin.x) * inverseDirection.x;
	tmax = (bounds[!sign[0]].x - ray.origin.x) * inverseDirection.x;
	tymin = (bounds[sign[1]].y - ray.origin.y) * inverseDirection.y;
	tymax = (bounds[!sign[1]].y - ray.origin.y) * inverseDirection.y;

	if((tmin > tymax) || (tymin > tmax))
		return -1;
	if(tymin > tmin)
		tmin = tymin;
	if(tymax < tmax)
		tmax = tymax;

	tzmin = (bounds[sign[2]].z - ray.origin.z) * inverseDirection.z;
	tzmax = (bounds[!sign[2]].z - ray.origin.z) * inverseDirection.z;

	if((tmin > tzmax) || (tzmin > tmax))
		return -1;
	if(tzmin > tmin)
		tmin = tzmin;
	if(tzmax < tmax)
		tmax = tzmax;

	return tmax;
}

float2 Incarnate::uniformSampleTriangle()
{
	float2 uv(rand1(),rand1());
	if(uv.x+uv.y > 1.f)
		uv = float2(1.f)-uv;
	return uv;
}
