#include <Camera.hpp>
using namespace Incarnate;

Camera::Camera()
{
}

Camera::Camera(float4 camPosition, float4 camDirection,
	float4 camRight, float4 camUp)
{
	position = camPosition;
	direction = camDirection;
	right = camRight;
	up = camUp;
}

void Camera::lookAt(float4 camPos, float4 lookAt, float4 upVec)
{
	float4 diffBtw = lookAt - camPos;
	position = camPos;
	right = normalize(cross(diffBtw,upVec));
	direction = normalize(diffBtw);
	up = normalize(upVec);
}

void Camera::setAspectRatio(float ratio)
{
	pixelAspectRatio = ratio;
}

float Camera::getAspectRatio() const
{
	return pixelAspectRatio;
}

Ray Camera::createCameraRay(float2 uv) const
{
	return Ray();
}

void Camera::update()
{
}

void PerspectiveCamera::setFOV(float2 filedOfView)
{
	fov = filedOfView;
}

void PerspectiveCamera::setFOV(float fovX, float fovY)
{
	fov = float2(fovX,fovY);
}

Ray PerspectiveCamera::createCameraRay(float2 uv) const
{
	float3 rayVec = (uv.x*2.0f-1.0f)*float3(rightVec)
		+ (1.0f-uv.y*2.0f)*float3(upVec)
		+ float3(direction);
	rayVec = normalize(rayVec);

	Ray ray(float3(position),rayVec);
	return ray;
}

void PerspectiveCamera::update()
{
	rightVec = right*tanf(fov.x*0.5f)*pixelAspectRatio;
	upVec = up*tanf(fov.y*0.5f);
}
