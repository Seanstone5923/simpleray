#include <LightSource.hpp>
#include <Triangle.hpp>
#include <Incarnate.hpp>
#include <TriangleProperty.hpp>
#include <Texture.hpp>

using namespace Incarnate;

AreaLightSource::AreaLightSource()
{
}

AreaLightSource::AreaLightSource(int geomGlobalIndex, const Triangle& geom, float3 emitRadience)
{
	setTriangle(geom, geomGlobalIndex);
	setRadience(emitRadience);
}

Ray AreaLightSource::sampleLi(const Intersection& intersection, float* pdf) const
{
	//Don't sample the lightsource if the intersection we are on is the lightsource
	if(intersection.globalIndex == geomId)
	{
		*pdf = 0;
		return Ray();
	}

	Ray sampleRay;

	float2 lightSampleUV = uniformSampleTriangle();
	float4 targetPos = triangle.interpolate(lightSampleUV);

	float4 connectVec = targetPos - intersection.intersection;
	float dist2 = dot(connectVec,connectVec);
	sampleRay.direction = float3(normalize(connectVec));
	sampleRay.origin = float3(intersection.intersection) + sampleRay.direction*1e-3f;

	//TODO: calculate the acuatl normal if we do have per vertex normals
	*pdf = dist2/std::abs(dot(-sampleRay.direction,float3(triangle.normal()))*surfaceArea);
	return sampleRay;
}

float AreaLightSource::pdfLi(const Intersection& pointOnLight, const float3& pointInSpace) const
{
	float dist2 = pointOnLight.t*pointOnLight.t;
	float pdf = dist2/std::abs(dot(-float3(pointOnLight.direction),pointOnLight.normal)*surfaceArea);
	return pdf;
}

float3 AreaLightSource::le(const Intersection& pointOnLight) const
{
	return float3(radience);
}

bool AreaLightSource::connectionIsValid(const RayHit& hit, float3 point) const
{
	if(hit.index == geomId)
		return true;
	return false;
}

void AreaLightSource::setRadience(float3 emitRadience)
{
	radience = emitRadience;
}

void AreaLightSource::setTriangle(const Triangle& geom, int geomGlobalIndex)
{
	triangle = geom;
	surfaceArea = geom.surfaceArea();
	geomId = geomGlobalIndex;
}

AreaLightSource* AreaLightSource::clone() const
{
	AreaLightSource* light = new AreaLightSource(geomId, triangle, radience);
	return light;
}

void AreaLightSource::distoryClone()
{
	delete this;
}

bool AreaLightSource::isDelta() const
{
	return false;
}

Ray PointLightSource::sampleLi(const Intersection& intersection, float* pdf) const
{
	float3 connectVec = position - float3(intersection.intersection);
	float3 direction = normalize(connectVec);
	float3 origin = float3(intersection.intersection) + direction*1e-3f;
	*pdf = dot(connectVec,connectVec);
	return Ray(origin, direction);
}

float PointLightSource::pdfLi(const Intersection& pointOnLight, const float3& pointInSpace) const
{
	return 0.f;
}

float3 PointLightSource::le(const Intersection& pointOnLight) const
{
	return intensity;
}

bool PointLightSource::connectionIsValid(const RayHit& hit, float3 point) const
{
	float4 connectVec = float4(position - point,0);
	float dist2 = dot(connectVec,connectVec);
	if((hit.t*hit.t) > dist2 || hit.t < 0)
		return true;
	return false;
}

bool PointLightSource::isDelta() const
{
	return true;
}

void PointLightSource::setIntensity(float3 val)
{
	intensity = val;
}

void PointLightSource::setPosition(float3 pos)
{
	position = pos;
}

Ray DirectionalLight::sampleLi(const Intersection& intersection, float* pdf) const
{
	float3 direction = -incomeDirection;
	float3 origin = float3(intersection.intersection) + direction*1e-3f;
	*pdf = 1.0f;
	return Ray(origin, direction);
}

float DirectionalLight::pdfLi(const Intersection& pointOnLight, const float3& pointInSpace) const
{
	return 0.f;
}

float3 DirectionalLight::le(const Intersection& pointOnLight) const
{
	return radience;
}

bool DirectionalLight::connectionIsValid(const RayHit& hit, float3 point) const
{
	return hit.index == -1;
}

bool DirectionalLight::isDelta() const
{
	return true;
}

void DirectionalLight::setRadience(float3 val)
{
	radience = val;
}

void DirectionalLight::setIncomeDirection(float3 direction)
{
	incomeDirection = direction;
}

float3 EnvironmentLight::power(const Intersection& intersection) const
{
	const float4& v = intersection.direction;
	return float3(environmentMap->getPixel(v)*scale);
}

void EnvironmentLight::setEnvironmentMap(EnvironmentMap* envMap)
{
	environmentMap = envMap;
}

EnvironmentMap* EnvironmentLight::getEnvironmentMap()
{
	return environmentMap;
}

void EnvironmentLight::setScale(float4 val)
{
	scale = val;
}

float3 GoniometricLight::le(const Intersection& pointOnLight) const
{
	const float4& v = pointOnLight.direction;
	return float3(environmentMap->getPixel(v));
}

void GoniometricLight::setEnvironmentMap(EnvironmentMap* envMap)
{
	environmentMap = envMap;
}

EnvironmentMap* GoniometricLight::getEnvironmentMap()
{
	return environmentMap;
}
