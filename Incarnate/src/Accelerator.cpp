#include <Accelerator.hpp>
#include <Triangle.hpp>
#include <Scene.hpp>
using namespace Incarnate;

#include <vector>
#include <algorithm>
using std::cout;
using std::endl;
using std::vector;

#include <stdint.h>
#include <limits.h>

void Accelerator::setScene(Scene* ptr)
{
	scene = ptr;
}

Scene* Accelerator::getScene()
{
	return const_cast<Scene*>(static_cast<const Accelerator*>(this)->getScene());
}

const Scene* Accelerator::getScene() const
{
	return scene;
}

// Stub accelerator: do nothing
RayHit Accelerator::raycast(const Ray& ray)
{
	RayHit hit;
	hit.t = -1;
	hit.index = -1;
	hit.u = -1;
	hit.v = -1;
	return hit;
}

void Accelerator::raycastStream(const Ray* rays, RayHit* hits, int num)
{
	for(int i=0;i<num;i++)
		hits[i] = raycast(rays[i]);
}

// Brute force accelerator: search the whole scene
RayHit BruteforceAccelerator::raycast(const Ray& ray)
{
	float nearestDistance = -1;
	float4 currentUV;
	RayHit hit;

	int size = scene->getTriangleCount();
	for(int i=0;i<size;i++)
	{
		Triangle triangle = scene->getTriangle(i);
		float distance = findIntersection(ray, triangle, &currentUV);
		if(distance > 0 && (distance < nearestDistance || nearestDistance < 0))
		{
			nearestDistance = distance;

			hit.index = i;
			hit.u = currentUV.x;
			hit.v = currentUV.y;
		}
	}
	hit.t = nearestDistance;
	return hit;
}

BVHAccelerator::~BVHAccelerator()
{
	delete [] triangles;
	triangles = nullptr;
}

// Build BVH
void BVHAccelerator::build()
{
	int triangleCount = scene->getTriangleCount();
	if(triangleCount == 0)
	{
		std::cout << "Error: No triangle in scene, BVHAccelerator will crash" << std::endl;
		return;
	}

	//Create a list that stores all the reiangles so we don't have to recreate them while raycasting
	delete [] triangles;
	triangles = new Triangle[triangleCount];

	triangleIndexList.clear();
	triangleIndexList.resize(triangleCount);
	for(int i=0;i<triangleCount;i++)
	{
		triangleIndexList[i] = i;
		triangles[i] = scene->getTriangle(i);
	}

	//the # of nodes in a balienced tree. This is here for stoping std::vector copy it's data too much
	if(splitMethod == SplitMethod::EqualCount)
		bvh.reserve(triangleCount*2);
	else
		bvh.reserve(triangleCount*3);//Should be enough

	bvh.clear();
	recusiveBuild(0,triangleCount-1);

	// for(BVHNode node : bvh)
	// 	cout << node.nextNode[0] << "\t" << node.nextNode[1] <<  "\t" << node.isLeaf << endl;
}

void BVHAccelerator::recusiveBuild(int start, int end)
{
	bvh.push_back(BVHNode());
	int index = bvh.size()-1;
	int objectCount = end - start;
	AABB aabb = createAABB(start,end);
	if(objectCount == 0)
	{
		bvh[index].nextNode[0] = triangleIndexList[start] | (1<<(sizeof(BVHNode::nextNode[0])*8-1));
		bvh[index].nextNode[1] = -1;//nothing
		bvh[index].aabb = aabb;
	}
	else
	{
		int axis = aabb.largestDimension();
		int midIndex = (start+end)/2;
		int splitIndex = midIndex;
		switch (splitMethod)
		{
			case SplitMethod::SAH:
			{
				float minSAHScore = FLT_MAX;
				int minSAHAxis = 0;
				const int MAX_TEST_NUM = 2048;
				const int TEST_NUM = std::min(objectCount, MAX_TEST_NUM);
				//rightScore[0] is unused.
				float* rightScore = new float[TEST_NUM];

				for(int sortAxis=0;sortAxis<3;sortAxis++)
				{
					std::sort(triangleIndexList.begin()+start
						,triangleIndexList.begin()+end+1
						,TriangleCentroidComparator(sortAxis,triangles));

					int lastRightIndex = end;
					AABB rightAABB;

					//No need to compute the SAH cost for edge cases.
					for(int i=TEST_NUM-1;i>=1;i--)
					{
						int index = start + ((int64_t)objectCount*i/TEST_NUM)+1;
						AABB delta = createAABB(index,lastRightIndex);
						rightAABB = enclose(rightAABB, delta);
						lastRightIndex = index;

						rightScore[i] = rightAABB.surfaceArea()*(float)(end-index+1);
					}

					AABB leftAABB;
					int lastLeftIndex = start;

					for(int i=1;i<TEST_NUM;i++)
					{
						int index = start + ((int64_t)objectCount*i/TEST_NUM);
						AABB delta = createAABB(lastLeftIndex,index);
						leftAABB = enclose(leftAABB, delta);
						lastLeftIndex = index;

						float sahScore = leftAABB.surfaceArea()*(float)(index-start+1)
							+ rightScore[i];

						if(sahScore < minSAHScore)
						{
							minSAHScore = sahScore;
							splitIndex = index;
							minSAHAxis = sortAxis;
						}
					}
				}
				delete [] rightScore;

				if(minSAHAxis != 2)
					std::sort(triangleIndexList.begin()+start
						,triangleIndexList.begin()+end+1
						,TriangleCentroidComparator(minSAHAxis,triangles));
				break;
			}
			case SplitMethod::EqualCount:
			default:
			{
				std::nth_element(triangleIndexList.begin()+start
					,triangleIndexList.begin()+midIndex
					,triangleIndexList.begin()+end+1
					,TriangleCentroidComparator(axis,triangles));
				splitIndex = midIndex;
			}


		}

		int firstChildIndex = bvh.size();
		recusiveBuild(start, splitIndex);
		int secondChildIndex = bvh.size();
		recusiveBuild(splitIndex+1, end);
		bvh[index].aabb = aabb;
		bvh[index].nextNode[0] = firstChildIndex;
		bvh[index].nextNode[1] = secondChildIndex;
	}
}

TriangleCentroidComparator::TriangleCentroidComparator(int compareDimension,Triangle* triangleList)
{
	dimension = compareDimension;
	triangles = triangleList;
}

bool TriangleCentroidComparator::operator()(int index1, int index2) const
{
	return triangles[index1].center()[dimension] < triangles[index2].center()[dimension];
}

// Use built BVH to find ray hit
RayHit BVHAccelerator::raycast(const Ray& ray)
{
	const int STACK_DEPTH = 32;
	RayHit hit;
	float nearestDistance = FLT_MAX;
	float4 currentUV;
	int sign[3];
	float3 inverseDirection = ray.inverseDirection();

	int nodeStack[STACK_DEPTH];
	int todoNode = 0;
	nodeStack[0] = 0;//Start traversing at the root node

	hit.t = -1.0f;
	hit.index = -1;
	ray.calcSign(sign);
	while(todoNode >= 0)
	{
		//NOTE: This might decrease perfoemace. If on any time it's giving too much overhead. remove it.
		if(todoNode >= STACK_DEPTH)
		{
			cout << "Error in BVHAccelerator : BVH traversing exceded maxium depth of " << STACK_DEPTH << ". Abord.\n";
			break;
		}
		const BVHNode& node = bvh[nodeStack[todoNode]];
		todoNode--;
		if(!(node.nextNode[0] & (1<<(sizeof(BVHNode::nextNode[0])*8-1))))
		{
			float aabbDistance[2];
			aabbDistance[0] = findIntersection(ray,sign,inverseDirection,bvh[node.nextNode[0]].aabb);
			aabbDistance[1] = findIntersection(ray,sign,inverseDirection,bvh[node.nextNode[1]].aabb);

			if(aabbDistance[0] >= 0)// && aabbDistance[0] < nearestDistance)
				nodeStack[++todoNode] = node.nextNode[0];

			if(aabbDistance[1] >= 0)// && aabbDistance[1] < nearestDistance)
				nodeStack[++todoNode] = node.nextNode[1];
		}
		else
		{
			int triangleIndex = node.nextNode[0] & (~(1<<(sizeof(BVHNode::nextNode[0])*8-1)));
			const Triangle& triangle = triangles[triangleIndex];
			float distance = findIntersection(ray, triangle, &currentUV);
			if(distance > 0 && distance < nearestDistance)
			{
				nearestDistance = distance;
				hit.t = nearestDistance;
				hit.index = triangleIndex;
				hit.u = currentUV.x;
				hit.v = currentUV.y;
			}
		}
	}

	return hit;
}

// Given index range of some triangles, return the boundaries for creating their bounding box
AABB BVHAccelerator::createAABB(int start, int end) const
{
	AABB ret;
	for(int i=start;i<=end;i++)
	{
		int index = triangleIndexList[i];
		Triangle triangle = triangles[index];

		ret = enclose(ret, float3(triangle.vertex[0]));
		ret = enclose(ret, float3(triangle.vertex[1]));
		ret = enclose(ret, float3(triangle.vertex[2]));
	}
	return ret;
}

float BVHAccelerator::calcSAHScore(int start, int end) const
{
	AABB aabb = createAABB(start,end);
	return (float)(end-start+1)*aabb.surfaceArea();
}

void BVHAccelerator::setSplitMethod(SplitMethod method)
{
	splitMethod = method;
}

SplitMethod BVHAccelerator::getSplitMethod() const
{
	return splitMethod;
}

#ifdef INC_EMBREE_ACCELERATOR

EmbreeAccelerator::EmbreeAccelerator()
{
	device = rtcNewDevice(nullptr);
	//Since we don't support dymantic scene for now. Just use a static scene
	//RTC_SCENE_DYNAMIC for dynamic scenes
	embreeScene = rtcDeviceNewScene(device, RTC_SCENE_STATIC|RTC_SCENE_HIGH_QUALITY|RTC_SCENE_INCOHERENT, RTC_INTERSECT1|RTC_INTERSECT_STREAM);
}
EmbreeAccelerator::~EmbreeAccelerator()
{
	rtcDeleteScene(embreeScene);
	rtcDeleteDevice(device);
}

void EmbreeAccelerator::build()
{
	// struct EmbreeVertex   { float x, y, z, a; };
	// struct EmbreeTriangle { int v0, v1, v2; };
	int triangleCount = scene->getTriangleCount();
	unsigned int mesh = rtcNewTriangleMesh(embreeScene, RTC_GEOMETRY_STATIC, triangleCount, triangleCount*3);
	//glm::vec4 should have the same layout as Embree's Vec3fa
	float4* vertices = (float4*)rtcMapBuffer(embreeScene,mesh,RTC_VERTEX_BUFFER);
	int3* triangleIndices = (int3*)rtcMapBuffer(embreeScene,mesh,RTC_INDEX_BUFFER);

	//Fill in the buffer in a lazy way
	#pragma omp parallel for
	for(int i=0;i<triangleCount;i++)
	{
		Triangle triangle = scene->getTriangle(i);
		for(int j=0;j<3;j++)
		{
			vertices[i*3+j] = triangle.vertex[j];
			triangleIndices[i][j] = i*3+j;
		}
	}

	rtcUnmapBuffer(embreeScene, mesh, RTC_VERTEX_BUFFER);
	rtcUnmapBuffer(embreeScene, mesh, RTC_INDEX_BUFFER);

	rtcCommit(embreeScene);
}

RayHit EmbreeAccelerator::raycast(const Ray& ray)
{
	RayHit hit;
	hit.t = -1.0f;
	hit.index = -1;

	RTCRay embreeRay;
	embreeRay.org[0] = ray.origin.x;
	embreeRay.org[1] = ray.origin.y;
	embreeRay.org[2] = ray.origin.z;

	embreeRay.dir[0] = ray.direction.x;
	embreeRay.dir[1] = ray.direction.y;
	embreeRay.dir[2] = ray.direction.z;

	embreeRay.tnear = 0.0f;
	embreeRay.tfar = FLT_MAX;

	embreeRay.geomID = RTC_INVALID_GEOMETRY_ID;
	embreeRay.primID = RTC_INVALID_GEOMETRY_ID;
	embreeRay.mask = -1;
	embreeRay.time = 0;

	static RTCIntersectContext context = {RTC_INTERSECT_INCOHERENT, nullptr};

	rtcIntersect1M(embreeScene,&context,&embreeRay,1,0);

	if(embreeRay.geomID != RTC_INVALID_GEOMETRY_ID)
	{
		hit.t = embreeRay.tfar;
		hit.index = embreeRay.primID;
		hit.u = embreeRay.u;
		hit.v = embreeRay.v;
	}
	return hit;
}

void EmbreeAccelerator::raycastStream(const Ray* rays, RayHit* hits, int num)
{
	RTCRay* embreeRays = new RTCRay[num];
	for(int i=0;i<num;i++)
	{
		RTCRay &embreeRay = embreeRays[i];
		embreeRay.org[0] = rays[i].origin.x;
		embreeRay.org[1] = rays[i].origin.y;
		embreeRay.org[2] = rays[i].origin.z;

		embreeRay.dir[0] = rays[i].direction.x;
		embreeRay.dir[1] = rays[i].direction.y;
		embreeRay.dir[2] = rays[i].direction.z;

		embreeRay.tnear = 0.0f;
		embreeRay.tfar = FLT_MAX;

		embreeRay.geomID = RTC_INVALID_GEOMETRY_ID;
		embreeRay.primID = RTC_INVALID_GEOMETRY_ID;
		embreeRay.mask = -1;
		embreeRay.time = 0;
	}

	const RTCIntersectContext context = {RTC_INTERSECT_INCOHERENT, nullptr};
	rtcIntersect1M(embreeScene,&context,embreeRays,num,sizeof(RTCRay));

	for(int i=0;i<num;i++)
	{
		if(embreeRays[i].geomID != RTC_INVALID_GEOMETRY_ID)
		{
			hits[i].t = embreeRays[i].tfar;
			hits[i].index = embreeRays[i].primID;
			hits[i].u = embreeRays[i].u;
			hits[i].v = embreeRays[i].v;
		}
		else
		{
			hits[i].t = -1;
			hits[i].index = -1;
		}
	}
	delete [] embreeRays;
}

#endif
