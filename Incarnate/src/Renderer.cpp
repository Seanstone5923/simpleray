#include <Incarnate.hpp>
#include <Accelerator.hpp>
#include <Triangle.hpp>
#include <Material.hpp>
#include <Renderer.hpp>
#include <Camera.hpp>
#include <Scene.hpp>
#include <LightSource.hpp>
#include <TriangleProperty.hpp>
#include <FixedSizeQueue.hpp>
using namespace Incarnate;

#include <iostream>
#include <chrono>
#include <algorithm>
using namespace std::chrono;
using std::cout;
using std::endl;

#include <string.h>
#include <stdint.h>

Renderer::Renderer()
{
	init();
}

Renderer::~Renderer()
{
	terminate();
}

int Renderer::init()
{
	//create a basic environment.
	film.resize(480,360);
	createBuffer(480,360);
	return 1;
}

void Renderer::clearBuffer()
{
	film.clear();
	currentSampleNum = 0;
}

void Renderer::terminate()
{
}

int Renderer::getSampleNum() const
{
    return sampleNum;
}

void Renderer::setSampleNum(int num)
{
	sampleNum = num;
}

int Renderer::getBounceDepth() const
{
	return bounceDepth;
}

void Renderer::setBounceDepth(int num)
{
	bounceDepth = num;
}

int Renderer::getWidth() const
{
	return bufferWidth;
}

int Renderer::getHeight() const
{
	return bufferHeight;
}

void Renderer::createBuffer(int width, int height)
{
	film.resize(width,height);

	bufferWidth = width;
	bufferHeight = height;
	clearBuffer();
}

const Film* Renderer::getBuffer() const
{
	return &film;
}

void Renderer::initRender()
{
	//HACK: gap with the previous archicture
	//Need a better way to to pass scene to renderer it self
	scene = accelerator->getScene();
	clearBuffer();
}

void Renderer::setAccelerator(Accelerator* ptr)
{
	accelerator = ptr;
}

int Renderer::getCurrentSampleCount() const
{
	return currentSampleNum;
}

const Accelerator* Renderer::getAccelerator() const
{
	return accelerator;
}

Accelerator* Renderer::getAccelerator()
{
	return const_cast<Accelerator*>(static_cast<const Renderer*>(this)->getAccelerator());
}

void Renderer::render()
{
	const Camera& camera = *scene->camera;

	int pixelCount = bufferWidth*bufferHeight;

	#if 0
	int chunkSize = bufferWidth;
	volatile int chunkIndex = 0;
	#pragma omp parallel
	{
		int iterateIndex;
		while(true)
		{
			#pragma omp critical
			{
				iterateIndex = chunkIndex;
				chunkIndex++;
			}
			if((iterateIndex+1)*chunkSize >= pixelCount)
				break;

			for(int j=0;j<chunkSize;j++)
			{
				int i = chunkSize*iterateIndex+j;
				int2 xy = calculatePixelCoord(i);
				int x = xy.x;
				int y = xy.y;

				int index = y*bufferWidth+x;
				float u = x/(float)bufferWidth;
				float v = y/(float)bufferHeight;

				Ray ray = camera.createCameraRay(float2(u,v));
				float4 renderedColor = trace(ray);

				film.addSample(index, renderedColor);
			}
		}

		#pragma omp once
		{
			for(int i=pixelCount-(pixelCount%chunkSize);i<pixelCount;i++)
			{
				int2 xy = calculatePixelCoord(i);
				int x = xy.x;
				int y = xy.y;

				int index = y*bufferWidth+x;
				float u = x/(float)bufferWidth;
				float v = y/(float)bufferHeight;

				Ray ray = camera.createCameraRay(float2(u,v));
				float4 renderedColor = trace(ray);

				film.addSample(index, renderedColor);
			}
		}
	}

	#else

	#pragma omp parallel for schedule(dynamic, bufferWidth)
	for(int i=0;i<pixelCount;i++)
	{
		//Create camera ray
		int2 xy = calculatePixelCoord(i);
		int x = xy.x;
		int y = xy.y;

		int index = y*bufferWidth+x;
		float u = x/(float)bufferWidth;
		float v = y/(float)bufferHeight;

		//Subpixels, antialias for free
		//They arn't as fast as I tought. They harm the perfoemace a lot when you have a simple scene
		/*u += (rand1()-0.5f)/bufferWidth;
		v += (rand1()-0.5f)/bufferHeight;*/

		Ray ray = camera.createCameraRay(float2(u,v));
		float4 renderedColor = trace(ray);

		film.addSample(index, float3(renderedColor));
	}
	#endif

	currentSampleNum++;
}

int2 Renderer::calculatePixelCoord(int index)
{
	//Tiled rendering isn't faster
	#if 0
		const int GRID_SIZE = 32;
		bool NOT_DIV = (bufferWidth%GRID_SIZE != 0);
		bool NOT_DIV_HEIGHT = (bufferHeight%GRID_SIZE != 0);
		const int GRID_COUNT = bufferWidth/GRID_SIZE + (NOT_DIV ? 0 : 1);
		const int V_GRID_COUNT = bufferHeight/GRID_SIZE + (NOT_DIV_HEIGHT ? 0 : 1);
		int rowPixCount = bufferWidth*GRID_SIZE;
		int gridY = index/rowPixCount;
		const int GRID_HEIGHT =(gridY!=V_GRID_COUNT || !NOT_DIV_HEIGHT ? GRID_SIZE : bufferHeight%GRID_SIZE);
		const int GRID_PIX_COUNT = GRID_SIZE*GRID_HEIGHT;
		int gridX = (index-(gridY*rowPixCount))/GRID_PIX_COUNT;
		const int GRID_WIDTH = (gridX!=GRID_COUNT || !NOT_DIV ? GRID_SIZE : bufferWidth%GRID_SIZE);
		int gridStartIndex = gridY*rowPixCount + gridX*GRID_PIX_COUNT;
		int subPixX = (index-gridStartIndex)%GRID_WIDTH;
		int subPixY = (index-gridStartIndex)/GRID_WIDTH;

		int x = gridX*GRID_SIZE + subPixX;
		int y = gridY*GRID_SIZE + subPixY;

	#else
		int x = index%bufferWidth;
		int y = index/bufferWidth;
	#endif
	return int2(x,y);
}

float4 PathRenderer::trace(Ray ray)
{
	const int BOUNCE_DEPTH = getBounceDepth();
	//Russian roulette: starting at depth 16
	const int RUSSIAN_ROULETTE_START_DEPTH = 16;
	const float FACTOR_CUT_OFF = 0.01;

	float4 factor(1.0f,1.0f,1.0f,1.0f);
	float4 renderedColor;
	Ray currentRay = ray;
	float rrFactor = 1.0f; // Russian roulette factor
	RayHit hit;

	#define EXP_LIGHT_SAMPLING
	// Find the ray hit by the accelerator
	#ifdef EXP_LIGHT_SAMPLING
	hit = accelerator->raycast(currentRay);
	#endif

	for(int i=0;i<BOUNCE_DEPTH||BOUNCE_DEPTH < 0;i++)//To ignore bounce limit. set bounceDepth to -1
	{
		if(i >= RUSSIAN_ROULETTE_START_DEPTH)//Russian roulette path termination
		{
			const float survivalProbability = 0.3f;
			if(rand1() > survivalProbability)
				break;
			rrFactor *= 1.0f/survivalProbability;
		}

		//Apply Russian roulette if factor is too low. This is unbiased
		if(dot(factor,float4(1,1,1,0)) <= FACTOR_CUT_OFF)
		{
			const float survivalProbability = 0.5f;
			if(rand1() > survivalProbability)
				break;
			rrFactor *= 1.0f/survivalProbability;
		}

		//If we have hit anything
		#ifndef EXP_LIGHT_SAMPLING
		hit = accelerator->raycast(currentRay);
		#endif
		if(hit.t > 0)
		{
			Intersection intersectionInfo = scene->getIntersection(currentRay,hit);
			//Triangle triangle = intersectionInfo.triangle;
			Material* material = intersectionInfo.material->resolve(intersectionInfo);
			//HACK: A hack to get mixed material
			intersectionInfo.material = material;

			//Add the light contrubution to the pixel value
			#ifdef EXP_LIGHT_SAMPLING
			if(i == 0)
			#endif
				renderedColor += factor*material->getEmit(intersectionInfo)*rrFactor;

			//Create sample ray based on the material
			float3 normal = intersectionInfo.normal;

			//renderedColor += factor*estimateDirect(currentRay, normal, intersectionInfo)*rrFactor;
			#ifdef EXP_LIGHT_SAMPLING
			Ray reflectRay;
			float4 transmitCoeff;
			float4 radience = estimateDirectMIS(currentRay, normal, intersectionInfo,&reflectRay, &hit, &transmitCoeff);
			renderedColor += factor*radience*rrFactor;
			#else

			//Create the sample ray and coefficient
			Ray reflectRay;
			float4 transmitCoeff;
			material->sample(currentRay, normal, intersectionInfo, reflectRay, transmitCoeff);
			#endif

			currentRay = reflectRay;//direction must be pre-normalized
			factor *= transmitCoeff;

		}
		else
		{
			if(environmentLight != nullptr)
			{
				Intersection intersection;
				intersection = scene->getIntersection(currentRay,hit);
				renderedColor += factor*float4(environmentLight->power(intersection),0)*rrFactor;
			}
			break;
		}
	}

	return renderedColor;
}

void VarianceRenderer::createBuffer(int width, int height)
{

	delete [] cumulationBuffer;
	delete [] squareCumulationBuffer;
	delete [] baseBuffer;

	cumulationBuffer = new float4[width*height];
	squareCumulationBuffer = new float4[width*height];
	baseBuffer = new float4[width*height];

	Renderer::createBuffer(width,height);

	baseRenderer->createBuffer(width,height);
	clearBuffer();
}

void VarianceRenderer::clearBuffer()
{
	int bufferSize = getWidth()*getHeight();
	memset(cumulationBuffer,0,bufferSize*sizeof(float4));
	memset(squareCumulationBuffer,0,bufferSize*sizeof(float4));
	memset(baseBuffer,0,bufferSize*sizeof(float4));
	Renderer::clearBuffer();

	baseRenderer->clearBuffer();
}

void VarianceRenderer::initRender()
{
	baseRenderer->initRender();
	Renderer::initRender();
}

void VarianceRenderer::setBaseRenderer(Renderer* renderer)
{
	baseRenderer = renderer;
	if(renderer == nullptr)
		return;
	createBuffer(baseRenderer->getWidth(),baseRenderer->getHeight());
	setSampleNum(baseRenderer->getSampleNum());
	setBounceDepth(baseRenderer->getBounceDepth());
	setAccelerator(baseRenderer->getAccelerator());
}

void VarianceRenderer::render()
{
	baseRenderer->render();
	int size = bufferWidth*bufferHeight;
	int sampleCount = baseRenderer->getCurrentSampleCount();

	//Copy the buffer on the initial rendering
	if(sampleCount == 1)
		memcpy(baseBuffer,baseRenderer->getBuffer(),size*sizeof(float4));

	//Use OpenMP to speed the process up a bit
	#pragma omp parallel for
	for(int i=0;i<size;i++)
	{
		float4 integral;
		float4 variance;
		float4 stddev;

		float4 currentColor = (float)sampleCount*((float4*)baseRenderer->getBuffer())[i]
			- (float)(sampleCount-1)*baseBuffer[i];

		cumulationBuffer[i] += currentColor;
		squareCumulationBuffer[i] += currentColor * currentColor;

		integral = cumulationBuffer[i]/(float)(sampleCount+1);

		if (integral == float4(0))
			integral = float4(0,0,0,1);

		variance = squareCumulationBuffer[i]/(float)(sampleCount+1)
			- integral*integral;
		//Workarround float point errors
		variance = glm::max(variance,float4(0,0,0,1));

		//Workarround nans and fast-math optimizations
		if(dot(float3(cumulationBuffer[i]),float3(1)) != 0)
			// /10 or the resulting vale might bit too high
			stddev = sqrt(variance) / integral / 10.f;
		stddev.w = 1;

		film.setSample(i,float3(stddev),sampleCount);
		baseBuffer[i] = ((float4*)baseRenderer->getBuffer())[i];
	}

	currentSampleNum++;
}

void VarianceRenderer::setSampleNum(int num)
{
	baseRenderer->setSampleNum(num);
	Renderer::setSampleNum(num);
}

void VarianceRenderer::setBounceDepth(int num)
{
	baseRenderer->setBounceDepth(num);
	Renderer::setBounceDepth(num);
}

void VarianceRenderer::setAccelerator(Accelerator* ptr)
{
	baseRenderer->setAccelerator(ptr);
	Renderer::setAccelerator(ptr);
}

void VarianceRenderer::terminate()
{
	delete [] cumulationBuffer;
	delete [] squareCumulationBuffer;
	delete [] baseBuffer;

	cumulationBuffer = nullptr;
	squareCumulationBuffer = nullptr;
	baseBuffer = nullptr;

	Renderer::terminate();
}

DirectLightingRenderer::~DirectLightingRenderer()
{
	terminate();
}

void DirectLightingRenderer::terminate()
{
	lights.clear();
	Renderer::terminate();
}

void DirectLightingRenderer::clearBuffer()
{
	Renderer::clearBuffer();
}

float4 DirectLightingRenderer::trace(Ray ray)
{
	float4 renderedColor;
	RayHit hit = accelerator->raycast(ray);
	if(hit.t >= 0)
	{
		Intersection surfaceIntersection = scene->getIntersection(ray,hit);

		//Triangle triangle = surfaceIntersection.triangle;
		Material* material = surfaceIntersection.material;

		//Add the light contrubution to the pixel value
		renderedColor = surfaceIntersection.material->getEmit(surfaceIntersection);

		//Get the normal for the current surface
		float3 normal = surfaceIntersection.normal;
		if(dot(normal,ray.direction) > 0)
			normal = -normal;

		renderedColor += estimateDirect(ray, normal, surfaceIntersection);
	}

	return renderedColor;
}

float4 DirectLightingRenderer::estimateDirect(const Ray& ri, float3 normal, const Intersection& intersection)
{
	//Select a lightsource randomly
	const LightSource* light = lights[randomLightSelector(*rng)];

	if(intersection.material->specular(intersection) == false)
	{

		//Uniformly sample the lightsource
		float pdf;
		Ray sampleRay;
		sampleRay = light->sampleLi(intersection, &pdf);
		if(dot(normal,ri.direction) > 0)
			normal = -normal;
		//Ignore if the lightsource is on the other side of the surface we hit
		if(dot(normal,sampleRay.direction) < 0)
			return float4(0);
		//Don't sample the lightsource if the intersection we are on is the lightsource
		//which happens when pdf=0
		if(pdf == 0)
			return float4(0);

		//Raycasy secondary/shadow ray
		RayHit hit = accelerator->raycast(sampleRay);
		if(light->connectionIsValid(hit,float4(sampleRay.origin,0)))
		{
			//To compensate that if we have n lights, we have 1/n chance to select a particular light
			float lightCount = lights.size();
			float4 radience = float4(light->le(intersection),0);
			//evaluate the BSDF on the direction ro(sampleRay.direction)
			float4 factor = intersection.material->evaluate(ri, sampleRay, normal, intersection);
			//return the light contrubution
			return factor*lightCount*radience/pdf;
		}
	}
	else//Sample the BSDF if it's a specular material
	{
		Ray reflectRay;
		float4 transmitCoeff;
		intersection.material->sample(ri, normal, intersection, reflectRay, transmitCoeff);
		RayHit hit = accelerator->raycast(reflectRay);
		if(hit.t > 0)
		{
			Intersection intersectionInfo = scene->getIntersection(reflectRay,hit);
			return transmitCoeff*intersectionInfo.material->getEmit(intersectionInfo);
		}
	}
	return float4(0);
}

//XXX: Should this be here?
inline float powerHeuristic(float numf, float fPdf, float numg, float gPdf)
{
	float f = numf * fPdf;
	float g = numg * gPdf;

	return (f*f)/(f*f+g*g);
}

float4 DirectLightingRenderer::estimateDirectMIS(const Ray& ri, float3 normal
	, const Intersection& intersection, Ray* sampleRay,RayHit* sampleHit, float4* coefficient)
{
	float4 directLighting;
	const LightSource* light = lights[randomLightSelector(*rng)];
	//To compensate that if we have n lights, we have 1/n chance to select a particular light
	float lightCount = lights.size();
	if(dot(normal,ri.direction) > 0)
		normal = -normal;
	if(intersection.material->specular(intersection) == false)
	{
		//Uniformly sample the lightsource
		float lightPdf;
		float scatteringPdf;
		float weight;
		Ray ray = light->sampleLi(intersection, &lightPdf);
		float4 radience = float4(light->le(intersection),0);
		//Ignore if the lightsource is on the other side of the surface we hit
		if(dot(normal,ray.direction) >= 0 || lightPdf == 0)
		{
			//Raycasy secondary/shadow ray
			RayHit hit = accelerator->raycast(ray);
			if(light->connectionIsValid(hit, float4(ray.origin,0)))
			{
				scatteringPdf = intersection.material->pdf(ri,ray,normal,intersection).x;

				//evaluate the BSDF on the direction ro(ray.direction)
				float4 factor = intersection.material->evaluate(ri, ray, normal, intersection);
				float weight = powerHeuristic(1,lightPdf, 1, scatteringPdf);

				directLighting += weight*factor*radience/lightPdf;
			}
		}

		Ray reflectRay;
		float4 coeff;
		intersection.material->sample(ri, normal, intersection, reflectRay, coeff);
		*sampleHit = accelerator->raycast(reflectRay);
		RayHit hit = *sampleHit;

		*sampleRay = reflectRay;
		*coefficient = coeff;
		if(sampleHit->t > 0)
		{

			if(light->connectionIsValid(hit, reflectRay.origin))
			{
				Intersection intersectionInfo = scene->getIntersection(reflectRay,*sampleHit);
				//HACK:
				scatteringPdf = intersection.material->pdf(ri,reflectRay,intersection.normal,intersection).x;
				lightPdf = light->pdfLi(intersectionInfo, intersection.intersection);
				if(scatteringPdf > 0 && lightPdf > 0)
				{
					float4 f = intersection.material->evaluate(ri,reflectRay,intersection.normal,intersection);
					weight = powerHeuristic(1,scatteringPdf, 1, lightPdf);
					directLighting += f*radience*weight/scatteringPdf;
				}
			}
		}
	}
	else
	{
		Ray reflectRay;
		float4 coeff;
		intersection.material->sample(ri, normal, intersection, reflectRay, coeff);
		*sampleHit = accelerator->raycast(reflectRay);
		*sampleRay = reflectRay;
		*coefficient = coeff;
		if(sampleHit->t > 0)
		{

			Intersection intersectionInfo = scene->getIntersection(reflectRay,*sampleHit);
			directLighting += coeff*intersectionInfo.material->getEmit(intersectionInfo);
		}
		return directLighting;
	}

	return directLighting*lightCount;
}

void DirectLightingRenderer::initRender()
{
	lights.clear();
	Renderer::initRender();

	for(LightSource* light : scene->lights)
		lights.push_back(light);

	int triangleCount = scene->getTriangleCount();
	for(int i=0;i<triangleCount;i++)
	{
		Material* material = scene->getTriangleProprty(i).material;
		if(material->isEmit())
		{
			AreaLightSource* light = material->getEmitProperty()->clone();
			light->setTriangle(scene->getTriangle(i), i);
			lights.push_back(light);
		}
	}

	environmentLight = scene->getnEvirotmentLight();

	if(lights.size() == 0)
		std::cout << "Warrning: No light found in scene" << std::endl;
	if(rng != nullptr)
		delete rng;
	rng = new std::mt19937;

	std::uniform_int_distribution<int>::param_type range(0,lights.size()-1);
	randomLightSelector.param(range);
}

void ExplcitRenderer::render()
{
	const Camera& camera = *scene->camera;

	int pixelCount = bufferWidth*bufferHeight;

	const int chunkSize = 440;
	int pixIndex = 0;
	#pragma omp parallel
	{
		bool solved = false;
		FixedSizeQueue<Ray> rays(chunkSize);
		FixedSizeQueue<int> indices(chunkSize);
		RayHit* hits = new RayHit[chunkSize];
		while(true)
		{
			int start;
			#pragma omp critical
			{
				if(pixIndex >= pixelCount)
					solved = true;
				start = pixIndex;
				pixIndex += chunkSize;
			}
			if(solved)
				break;
			int targetNum = start+chunkSize<pixelCount?chunkSize:pixelCount-start;

			for(int i=0;i<targetNum;i++)
			{
				int ind = i+start;
				int2 xy = calculatePixelCoord(ind);
				int x = xy.x;
				int y = xy.y;

				int index = y*bufferWidth+x;
				float u = x/(float)bufferWidth;
				float v = y/(float)bufferHeight;
				indices.enqueue(index);
				rays.enqueue(camera.createCameraRay(float2(u,v)));
			}

			accelerator->raycastStream(&rays[0],hits,targetNum);

			for(int i=0;i<targetNum;i++)
			{
				float4 renderedColor = integratePoint(rays[i],hits[i]);

				int index = indices[i];
				film.addSample(index, float3(renderedColor));
			}
			rays.clear();
			indices.clear();
		}
		delete [] hits;

	}
	currentSampleNum++;
}

float4 NormalRenderer::integratePoint(Ray ray, RayHit hit)
{
	if(hit.t >= 0)
	{
		Intersection surfaceIntersection = scene->getIntersection(ray,hit);
		return float4(glm::abs(surfaceIntersection.normal),0);
	}
	return float4(0);
}
