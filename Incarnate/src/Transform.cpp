#include <Transform.hpp>

using namespace Incarnate;

float3 Transform::getTranslation()
{
	return translation;
}

float4x4 Transform::getTransformMatrix()
{
	if(manuelTransformMatrixApplied)
		return transformMatrix;

	applyComponent();
	return transformMatrix;
}

void Transform::translate(float3 vec)
{
	manuelTransformMatrixApplied = false;
	translation = vec;
}

void Transform::rotate(float3 vec, float rad)
{
	manuelTransformMatrixApplied = false;
	rotateVector = vec;
	radiance = rad;
}

void Transform::scale(float3 vec)
{
	manuelTransformMatrixApplied = false;
	scaling = vec;
}

float4x4 Transform::getRotationMatrix()
{
	return glm::rotate(radiance,rotateVector);
}

float4x4 Transform::getScalingMatrix()
{
	return glm::scale(scaling);
}

float4x4 Transform::getTranlationMatrix()
{
	return glm::translate(translation);
}

void Transform::setTransformMatrix(float4x4 matrix)
{
	manuelTransformMatrixApplied = true;
	transformMatrix = matrix;
}

void Transform::applyComponent()
{
	float4x4 translationMatrix = getTranlationMatrix();
	float4x4 rotationMatrix = getRotationMatrix();
	float4x4 scalingMatrix = getScalingMatrix();

	transformMatrix = translationMatrix*rotationMatrix*scalingMatrix;
}
