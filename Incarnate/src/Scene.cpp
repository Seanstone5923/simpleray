#include <Scene.hpp>
#include <Mesh.hpp>
#include <DataBuffer.hpp>
#include <Ray.hpp>
#include <Material.hpp>

using namespace Incarnate;
using std::cout;
using std::endl;



void Scene::setCamera(Camera* cam)
{
	camera = cam;
}

void Scene::addMesh(float4* verts, int verticesNum, int* inds, int indicesCount, Material* material, DataBufferArray* dataBufferArray, float4x4 transformMatrix)
{
	int currentVerticesCount = vertices.size();
	int currentIndicesCount = indices.size();
	int triangleCount = indicesCount/3;

	//verticies
	for(int i=0;i<verticesNum;i++)
		vertices.push_back(transformMatrix*verts[i]);

	//indicies
	indices.insert(indices.end(),inds, &inds[indicesCount]);
	for(int i=0;i<indicesCount;i++)
		indices[currentIndicesCount+i] += currentVerticesCount;

	int meshId = meshDatas.size();

	MeshProperty prop;
	prop.startIndex = currentIndicesCount/3;
	prop.material = material;
	prop.dataBufferArray = dataBufferArray;
	prop.transformMatrix = transformMatrix;
	meshDatas.push_back(prop);

	//per triangle data
	for(int i=0;i<triangleCount;i++)
	{
		triToMeshMap.push_back(meshId);
	}
}

Triangle Scene::getTriangle(int index) const
{
	return getTriangleData(vertices,index);
}

Triangle Scene::getTriangleData(const std::vector<float4>& data, int index) const
{
	int ind[3] = {0};
	getIndices(ind,index);
	return Triangle(data[ind[0]],
		data[ind[1]],
		data[ind[2]]);
}

void Scene::build()
{
	int verticesNum = 0;
	int indicesNum = 0;
	vertices.clear();
	indices.clear();
	triToMeshMap.clear();
	meshDatas.clear();

	int meshNum = meshes.size();
	for(int i=0;i<meshNum;i++)
	{
		verticesNum += meshes[i]->getVerticesCount();
		indicesNum += meshes[i]->getIndicesCount();
	}

	vertices.reserve(verticesNum);
	indices.reserve(indicesNum);
	triToMeshMap.reserve(meshNum);
	meshDatas.reserve(meshNum);

	for(int i=0;i<meshNum;i++)
		addMesh(meshes[i]->getVertices(), meshes[i]->getVerticesCount()
		,meshes[i]->getIndices(), meshes[i]->getIndicesCount()
		,meshes[i]->getMaterial()
		,meshes[i]->getDataBufferArray()
		,transformMatrices[i]);
}

void Scene::addMesh(Mesh* mesh, float4x4 transformMatrix)
{
	meshes.push_back(mesh);
	transformMatrices.push_back(transformMatrix);
	meshIndex.insert({mesh,meshes.size()-1});
}

bool Scene::removeMesh(const Mesh* mesh)
{
	auto it = meshIndex.find(mesh);
	if(it != meshIndex.end())
	{
		meshes.erase(meshes.begin()+it->second);
		transformMatrices.erase(transformMatrices.begin()+it->second);
		meshIndex.erase(it);
		return true;
	}

	cout << "Error: Mesh requested to remvoe: " << mesh << " does not exist in the scene's mesh list" << endl;
	return false;
}

//This is not the best perfoemace wise option to get all the data we need. Just a hack for now.
//TODO: Make this function fast(pass pointer/ref in of sth else)
Intersection Scene::getIntersection(const Ray& ray, const RayHit& rayHit) const
{
	Intersection intersection;
	int globalIndex = rayHit.index;
	intersection.direction = float4(ray.direction,0);
	intersection.t = rayHit.t;

	//If we do hit something
	if(globalIndex >= 0)
	{
		intersection.globalIndex = globalIndex;
		intersection.intersection = float4(ray.origin+ray.direction*rayHit.t,0);
		intersection.uv = float2(rayHit.u,rayHit.v);

		const MeshProperty& mprop = meshDatas[triToMeshMap[globalIndex]];

		intersection.internalIndex = globalIndex - mprop.startIndex;
		intersection.material = mprop.material;
		intersection.dataBufferArray = mprop.dataBufferArray;
		intersection.normal = float3(normalize(mprop.transformMatrix*
			float4(intersection.material->getNormal(getTriangle(globalIndex).normal(),intersection),0)));
	}

	return intersection;
}

void Scene::setEnvirotmentLight(EnvironmentLight* envLight)
{
	environmentLight = envLight;
}

EnvironmentLight* Scene::getnEvirotmentLight()
{
	return environmentLight;
}
