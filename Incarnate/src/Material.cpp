#include <Material.hpp>
#include <DataBuffer.hpp>
#include <Triangle.hpp>
#include <Texture.hpp>
#include <Incarnate.hpp>
#include <TriangleProperty.hpp>
#include <LightSource.hpp>
using namespace Incarnate;

float4 Material::hemisphere(float u1, float u2)
{
	const float r = sqrtf(1.0f-u1*u1);
	const float phi = 2.f * M_PI * u2;
	return float4(cosf(phi)*r, sinf(phi)*r, u1,0);
}

float3 Material::getNormal(const float3& normal, float2 uv, const DataBufferArray* dataBufferArray, int index)
{
	float3 norm = normal;
	if((*dataBufferArray)[2] != nullptr)
	{
		float4* normalBuffer = (float4*)(*dataBufferArray)[2]->getBuffer();
		int* indexBuffer = (int*)(*dataBufferArray)[1]->getBuffer();
		Triangle normalTriangle;
		normalTriangle.vertex[0] = normalBuffer[indexBuffer[index*3+0]];
		normalTriangle.vertex[1] = normalBuffer[indexBuffer[index*3+1]];
		normalTriangle.vertex[2] = normalBuffer[indexBuffer[index*3+2]];
		float4 n = normalTriangle.interpolate(uv.x,uv.y);
		norm = float3(n);
	}
	return normalize(norm);
}

float3 Material::getNormal(const float3& normal , const Intersection& intersection)
{
	DataBufferArray* dataBufferArray = intersection.dataBufferArray;
	int index = intersection.internalIndex;
	float2 uv = intersection.uv;
	return getNormal(normal, uv, dataBufferArray, index);
}

Ray Material::createRandomReflect(const float3& normal, const float4& intersection)
{
	float4 rotX, rotY;
	ons(float4(normal,0), rotX, rotY);
	float4 sampledDir = hemisphere(rand1(),rand1());
	float4 rotatedDir;
	rotatedDir.x = dot(float4(rotX.x, rotY.x, normal.x,0),sampledDir);
	rotatedDir.y = dot(float4(rotX.y, rotY.y, normal.y,0),sampledDir);
	rotatedDir.z = dot(float4(rotX.z, rotY.z, normal.z,0),sampledDir);

	float4 direction = rotatedDir;	// already normalized
	float4 origin = intersection
		+direction*1e-3f;
	Ray reflect;
	reflect.origin = float3(origin);
	reflect.direction = float3(direction);

	return reflect;
}

Ray Material::createCosineReflect(const float4& normal, const float4& intersection)
{
	float4 rotX, rotY;
	ons(normal, rotX, rotY);
	float4 sampledDir = cosineSampleHemisphere(rand1(),rand1());
	float4 rotatedDir;
	rotatedDir.x = dot(float4(rotX.x, rotY.x, normal.x,0),sampledDir);
	rotatedDir.y = dot(float4(rotX.y, rotY.y, normal.y,0),sampledDir);
	rotatedDir.z = dot(float4(rotX.z, rotY.z, normal.z,0),sampledDir);

	float4 direction = rotatedDir;	// already normalized
	float4 origin = intersection
		+direction*1e-3f;
	Ray reflectRay;
	reflectRay.origin = float3(origin);
	reflectRay.direction = float3(direction);

	return reflectRay;
}

float4 Material::pdf(const Ray& ri, const Ray& ro, float3 normal, const Intersection& property) const
{
	float val = std::abs(dot(ro.direction, normal))*(float)(M_1_PI);
	return float4(val,val,val,0);
}

float4 Material::evaluate(const Ray& ri, const Ray& ro, float3 normal, const Intersection& property) const
{
	return getReflect(property)*pdf(ri,ro,normal,property);
}

void Material::sample(const Ray& ray, float3 normal, const Intersection& intersection
	, Ray& sampleRay, float4& coefficient)
{
	sampleRay = createRandomReflect(normal,intersection.intersection);
	coefficient = float4(calcRandomReflectCoeff(sampleRay,normal));
}

float4 Material::getEmit(const Intersection& intersection) const
{
	if(emitProp)
		return float4(emitProp->le(intersection),0);
	return float4();
}

float4 Material::getReflect(const Intersection& intersection) const
{
	DataBufferArray* dataBufferArray = intersection.dataBufferArray;
	if((*dataBufferArray)[3] != nullptr && textures[0] != nullptr)
	{
		float2 textureUV;
		int index = intersection.internalIndex;
		float2* uvBuffer = (float2*)(*dataBufferArray)[3]->getBuffer();
		int* indexBuffer = (int*)(*dataBufferArray)[1]->getBuffer();

		float4 uvVertex[3] = {float4(uvBuffer[indexBuffer[index*3+0]],0,0)
			,float4(uvBuffer[indexBuffer[index*3+1]],0,0)
			,float4(uvBuffer[indexBuffer[index*3+2]],0,0)};
		Triangle uvTriangle(uvVertex[0],uvVertex[1],uvVertex[2]);
		textureUV = float2(uvTriangle.interpolate(intersection.uv));
		return textures[0]->getPixel(textureUV)*reflectColor;
	}
	return reflectColor;
}

void Material::setReflect(float4 reflect)
{
	reflectColor = reflect;
}

bool Material::specular(const Intersection& intersetion) const
{
	return isSpecular;
}

Material::~Material()
{
}

void Material::setTexture(Texture* tex, int index)
{
	textures[index] = tex;
}

Texture* Material::getTexture(int index)
{
	return textures[index];
}

Material* Material::getBSDF(const Intersection& intersection)
{
	return (Material*)(static_cast<const Material*>(this)->getBSDF(intersection));
}

const Material* Material::getBSDF(const Intersection& intersection) const
{
	return this;
}

Material* Material::resolve(const Intersection& intersection)
{
	Material* material = this;
	Material* mat = material->getBSDF(intersection);
	while(material != mat)
	{
		material = mat;
		mat = mat->getBSDF(intersection);
	}
	return material;
}

void Material::setEmitProperty(AreaLightSource* prop)
{
	emitProp = prop;
}
const AreaLightSource* Material::getEmitProperty() const
{
	return emitProp;
}

MatteMaterial::MatteMaterial(float4 color) : Material(color)
{
}

void MatteMaterial::sample(const Ray& ray, float3 normal, const Intersection& intersection
	, Ray& sampleRay, float4& coefficient)
{
	if(dot(float3(normal),ray.direction) > 0)
		normal = -normal;
	float4 color = getReflect(intersection);
	brdf.sample(ray,normal,intersection,sampleRay,coefficient);
	coefficient *= color;
}

bool MatteMaterial::specular(const Intersection& intersetion) const
{
	return false;
}

float4 MatteMaterial::pdf(const Ray& ri, const Ray& ro, float3 normal, const Intersection& property) const
{
	return brdf.pdf(ri,ro,normal,property);
}

const Material* MatteMaterial::getBSDF(const Intersection& intersection) const
{
	float4 color = getReflect(intersection);
	if(rand1() <= color.a)
		return this;
	return &transparentMaterial;
}
MirrorMaterial::MirrorMaterial(float4 color) : Material(color)
{
	setSpecular(true);
}

void MirrorMaterial::sample(const Ray& ray, float3 normal, const Intersection& intersection
	, Ray& sampleRay, float4& coefficient)
{
	if(dot(normal,ray.direction) > 0)
		normal = -normal;
	float3 reflectDirection = reflect(ray.direction,normal);
	sampleRay = Ray(float3(intersection.intersection) + reflectDirection*(1e-3f),reflectDirection);
	coefficient = getReflect(intersection);
}

float4 MirrorMaterial::pdf(const Ray& ri, const Ray& ro, float3 normal, const Intersection& property) const
{
	return float4(1,1,1,0);
}

PhongMaterial::PhongMaterial(float4 color, float powerVal) : Material(color)
{
	power = powerVal;
}

void PhongMaterial::setPower(float val)
{
	power = val;
}

float PhongMaterial::getPower() const
{
	return power;
}

void PhongMaterial::sample(const Ray& ray, float3 normal, const Intersection& intersection
	, Ray& sampleRay, float4& coefficient)
{
	/*
	sampleRay = createRandomReflect(normal,intersection);
	float4 reflectDirection = reflect(ray.direction,normal);
	coefficient = reflectColor * pow(calcRandomReflectCoeff(sampleRay,reflectDirection),power+1)
		*(float)((power+1));*/
	if(dot(normal,ray.direction) > 0)
		normal = -normal;

	float3 reflectDirection = reflect(ray.direction,normal);
	float2 rnd(rand1(),rand1());
	float phi = 2.0f*M_PI*rnd.x;
	float theta = acos(powf(rnd.y,1.0f/(power+2)));
	float3 rotX,rotY;
	ons(reflectDirection,rotX,rotY);
	float3 result = cosf(phi)*sinf(theta)*rotX
		+ sinf(phi)*sinf(theta)*rotY
		+ cosf(theta)*reflectDirection;
	sampleRay.origin = float3(intersection.intersection) + result*1e-5f;

	//So the sample ray dones't hit the surface again
	if(dot(result,normal) < 0)
		result = -result;
	sampleRay.direction = result;

	coefficient = getReflect(intersection)*(float)((power+1)/(power+2));
}

float4 PhongMaterial::pdf(const Ray& ri, const Ray& ro, float3 normal, const Intersection& property) const
{
	if(dot(float3(normal),ri.direction) > 0)
		normal = -normal;
	float4 reflectDirection = float4(reflect(ri.direction,normal),0);
	float n = power+1.f;
	float phongCoefficient = powf(dot(ro.direction,float3(reflectDirection)),n);
	if(dot(ro.direction,float3(reflectDirection)) < 0)
		return float4(0);
	float normalizationFactor = ((n+2.f)*(n+4.f))/(8.f*(powf(2,-n*0.5f)+n));
	float factor = phongCoefficient*normalizationFactor;
	return float4(factor,factor,factor,0);
}

void MixedMaterial::setBSDF(Material* material, int index)
{
	//Only accept 0 or 1 as index
	if(index < 2)
		materials[index] = material;
}

void MixedMaterial::setFirstBSDF(Material* material)
{
	setBSDF(material,0);
}
void MixedMaterial::setSecondBSDF(Material* material)
{
	setBSDF(material,1);
}

const Material* MixedMaterial::getBSDF(const Intersection& intersection) const
{
	int index = (rand1()>=ratio?0:1);
	Material* material = materials[index];
	if(material == nullptr)
		material = materials[!index];
	return (const Material*)material;
}

void MixedMaterial::setRatio(float mixRatio)
{
	ratio = mixRatio;
}

float MixedMaterial::getRatio()
{
	return ratio;
}

TransparentMaterial::TransparentMaterial()
{
	setSpecular(true);
}

void TransparentMaterial::sample(const Ray& ray, float3 normal, const Intersection& intersection
	, Ray& sampleRay, float4& coefficient)
{
	btdf.sample(ray,normal,intersection,sampleRay,coefficient);
}

float4 TransparentMaterial::pdf(const Ray& ri, const Ray& ro, float3 normal, const Intersection& property) const
{
	return float4(1);
}
