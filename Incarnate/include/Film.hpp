#ifndef FILM_HPP
#define FILM_HPP

#include <Vec.hpp>

namespace Incarnate
{

class FilmElement
{
public:
	float3 color;
	int sampleNum;
};

class Film
{
public:
	Film();
	Film(int x, int y);
	~Film();

	void resize(int w, int h);
	void clear();
	void distory();
	bool good() const;

	inline void setSample(int index, float3 value, int num = 1)
	{
		buffer[index].sampleNum = num;
		buffer[index].color = value;
	}

	inline void addSample(int index, float3 value)
	{
		float3 currentColor = buffer[index].color;
		int currentSampleNum = buffer[index].sampleNum;
		float3 mixedColor = mix(currentColor,value,1.0f/(currentSampleNum+1));
		setSample(index, mixedColor, currentSampleNum+1);
	}

	inline FilmElement& getSample(int index)
	{
		return buffer[index];
	}

	inline FilmElement& getSample(int index) const
	{
		return buffer[index];
	}

	FilmElement* buffer = nullptr;
	int width, height;
};

}

#endif
