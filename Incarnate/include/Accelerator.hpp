#ifndef ACCLERATOR_H
#define ACCLERATOR_H

#include <Ray.hpp>
#include <AABB.hpp>

#include <vector>

#ifdef INC_EMBREE_ACCELERATOR
#include <embree2/rtcore.h>
#include <embree2/rtcore_ray.h>
#endif
namespace Incarnate
{

class Scene;

class Accelerator
{
public:
	Accelerator(const Accelerator&) = delete;
	Accelerator(){}
	virtual ~Accelerator(){}
	virtual void build(){}
	void setScene(Scene* ptr);
	Scene* getScene();
	const Scene* getScene() const;
	virtual RayHit raycast(const Ray& ray);
	virtual void raycastStream(const Ray* rays, RayHit* hits, int num);
protected:
	Scene* scene = nullptr;
};

class BruteforceAccelerator : public Accelerator
{
public:
	virtual ~BruteforceAccelerator(){}
	RayHit raycast(const Ray& ray);
};

class BVHNode
{
public:
	AABB aabb;
	//The most significant bit is used to encode if this node is a leaf
	unsigned int nextNode[2];
};

class Triangle;

class TriangleCentroidComparator
{
public:
	TriangleCentroidComparator(int compareDimension,Triangle* triangleList);
	bool operator()(int index1, int index2) const;

private:
	int dimension;
	Triangle* triangles;
};

enum class SplitMethod
{
	EqualCount = 0,
	SAH = 1
};

class BVHAccelerator : public Accelerator
{
public:
	virtual ~BVHAccelerator();
	void build();
	RayHit raycast(const Ray& ray);
	void setSplitMethod(SplitMethod method);
	SplitMethod getSplitMethod() const;
protected:
	std::vector<BVHNode> bvh;
	AABB createAABB(int start, int end) const;
	void recusiveBuild(int start, int end);

	float calcSAHScore(int start, int end) const;

	Triangle* triangles = nullptr;
	std::vector<int> triangleIndexList;
	SplitMethod splitMethod = SplitMethod::SAH;
};

#ifdef INC_EMBREE_ACCELERATOR
class EmbreeAccelerator : public Accelerator
{
public:
	EmbreeAccelerator();
	~EmbreeAccelerator();
	RayHit raycast(const Ray& ray);
	void raycastStream(const Ray* rays, RayHit* hits, int num);
	void build();
protected:
	RTCDevice device;
	RTCScene embreeScene;
};
#endif

}

#endif
