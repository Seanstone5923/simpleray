#ifndef AABB_H
#define AABB_H

#include <limits.h>

#include <Vec.hpp>

namespace Incarnate
{
//FIXME: There is a bug where when a ray.direction's component is 0, AABB won't be hitted when it should be.
class AABB
{
public:
	inline AABB():AABB(float3(FLT_MAX),float3(-FLT_MAX)){}
	inline AABB(float3 min_, float3 max_) { bounds[0] = min_; bounds[1] = max_; }
	inline bool unbounded() const { return bounds[0].x == -FLT_MAX || bounds[0].y == -FLT_MAX ||
		bounds[0].z == -FLT_MAX || bounds[1].x == FLT_MAX || bounds[1].y == FLT_MAX || bounds[1].z == FLT_MAX; }
	inline size_t largestDimension() const
	{
		float dx = abs(bounds[1].x - bounds[0].x);
		float dy = abs(bounds[1].y - bounds[0].y);
		float dz = abs(bounds[1].z - bounds[0].z);
		if (dx > dy && dx > dz)
			return 0;
		if (dy > dz)
			return 1;

		return 2;
	}
	inline float3 center() const {return (bounds[0]+bounds[1])/2.0f;}
	inline float surfaceArea() const
	{
		float3 size = bounds[1] - bounds[0];
		float xArea = size.y*size.z;
		float yArea = size.x*size.z;
		float zArea = size.x*size.y;
		return (xArea+yArea+zArea)*2.0f;
	}

	float3 bounds[2];
};

class Ray;

float findIntersection(Ray ray, AABB aabb);
float findIntersection(const Ray& ray,int* sign,const float3& inverseDirection, const AABB& aabb);
inline AABB enclose(const AABB& firstBoundingBox, const AABB& secondBoundingBox)
{

	AABB ret;
	ret.bounds[0] = glm::min(firstBoundingBox.bounds[0], secondBoundingBox.bounds[0]);
	ret.bounds[1] = glm::max(firstBoundingBox.bounds[1], secondBoundingBox.bounds[1]);
	return ret;
}

inline AABB enclose(const AABB& boundingBox, const float3& point)
{
	AABB ret;
	ret.bounds[0] = glm::min(boundingBox.bounds[0], point);
	ret.bounds[1] = glm::max(boundingBox.bounds[1], point);
	return ret;
}
}

#endif
