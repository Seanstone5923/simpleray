#ifndef LIGHT_SOURCE_HPP
#define LIGHT_SOURCE_HPP

#include <Vec.hpp>
#include <Triangle.hpp>
#include <Ray.hpp>

namespace Incarnate
{

class Intersection;

class LightSource
{
public:
	virtual Ray sampleLi(const Intersection& intersection, float* pdf) const = 0;
	virtual float pdfLi(const Intersection& pointOnLight, const float3& pointInSpace) const = 0;
	virtual float3 le(const Intersection& pointOnLight) const = 0;
	virtual bool connectionIsValid(const RayHit& hit, float3 point) const = 0;
	virtual bool isDelta() const = 0;
};

class AreaLightSource : public LightSource
{
public:
	AreaLightSource();
	AreaLightSource(int geomGlobalIndex, const Triangle& triangle, float3 radience);
	Ray sampleLi(const Intersection& intersection, float* pdf) const;
	float pdfLi(const Intersection& pointOnLight, const float3& pointInSpace) const;
	float3 le(const Intersection& pointOnLight) const;
	bool connectionIsValid(const RayHit& hit, float3 point) const;
	bool isDelta() const;

	void setRadience(float3 emitRadience);
	void setTriangle(const Triangle& geom, int geomGlobalIndex);

	virtual AreaLightSource* clone() const;
	virtual void distoryClone();
protected:
	int geomId;
	float surfaceArea;
	Triangle triangle;
	float3 radience;
};

class PointLightSource : public LightSource
{
public:
	Ray sampleLi(const Intersection& intersection, float* pdf) const;
	float pdfLi(const Intersection& pointOnLight, const float3& pointInSpace) const;
	float3 le(const Intersection& pointOnLight) const;
	bool connectionIsValid(const RayHit& hit, float3 point) const;
	bool isDelta() const;

	void setIntensity(float3 val);
	void setPosition(float3 pos);

protected:
	float3 position;
	float3 intensity = float3(1);
};

class DirectionalLight : public LightSource
{
public:
	Ray sampleLi(const Intersection& intersection, float* pdf) const;
	float pdfLi(const Intersection& pointOnLight, const float3& pointInSpace) const;
	float3 le(const Intersection& pointOnLight) const;
	bool connectionIsValid(const RayHit& hit, float3 point) const;
	bool isDelta() const;

	void setRadience(float3 val);
	void setIncomeDirection(float3 val);

protected:
	float3 radience = float3(1);
	float3 incomeDirection = float3(0,0,1);

};

class Texture;
class EnvironmentMap;

class EnvironmentLight
{
public:
	float3 power(const Intersection& pointOnLight) const;
	void setScale(float4 val);

	void setEnvironmentMap(EnvironmentMap* envMap);
	EnvironmentMap* getEnvironmentMap();
protected:
	EnvironmentMap* environmentMap = nullptr;
	float4 scale = float4(1);
};

class GoniometricLight : public PointLightSource
{
public:
	float3 le(const Intersection& pointOnLight) const;

	void setEnvironmentMap(EnvironmentMap* envMap);
	EnvironmentMap* getEnvironmentMap();
protected:
	EnvironmentMap* environmentMap = nullptr;
};

}

#endif
