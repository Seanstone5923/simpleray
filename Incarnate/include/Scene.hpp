#ifndef SCENE_HPP
#define SCENE_HPP

#include <Vec.hpp>
#include <Camera.hpp>
#include <Triangle.hpp>
#include <Transform.hpp>
#include <TriangleProperty.hpp>

#include <unordered_map>
#include <vector>

namespace Incarnate
{

class Material;
class Mesh;
class DataBufferArray;
class LightSource;
class EnvironmentLight;

class MeshProperty
{
public:
	Material* material;
	DataBufferArray* dataBufferArray;
	int startIndex;
	float4x4 transformMatrix;
};


//TODO: This class is complete mess. Clean it up
class Scene
{
public:
	void addMesh(float4* verts, int verticesNum, int* inds, int indicesCount, Material* Material,DataBufferArray* dataBufferArray,float4x4 transformMatrix = float4x4());
	void addMesh(Mesh* mesh, float4x4 transformMatrix = float4x4());
	bool removeMesh(const Mesh* mesh);
	void setCamera(Camera* cam);
	void build();

	Camera* camera = nullptr;

	std::vector<Mesh*> meshes;
	std::vector<float4x4> transformMatrices;
	std::vector<MeshProperty> meshDatas;
	std::vector<int> triToMeshMap;
	std::unordered_map<const Mesh*,int> meshIndex;

	//Per vertex data
	std::vector<float4> vertices;

	Triangle getTriangle(int index) const;

	//Inline for speed
	inline void getIndices(int* data, int index) const
	{
		data[0] = indices[index*3+0];
		data[1] = indices[index*3+1];
		data[2] = indices[index*3+2];
	}

	inline int getTriangleCount() const
	{
		return indices.size()/3;
	}

	Intersection getIntersection(const Ray& ray, const RayHit& rayHit) const;
	inline const TriangleProperty getTriangleProprty(int index) const
	{
		MeshProperty mprop = meshDatas[triToMeshMap[index]];
		TriangleProperty prop;
		prop.globalIndex = index;
		prop.internalIndex = index - mprop.startIndex;
		prop.material = mprop.material;
		prop.dataBufferArray = mprop.dataBufferArray;
		return prop;
	}

	std::vector<LightSource*> lights;

	void setEnvirotmentLight(EnvironmentLight* envLight);
	EnvironmentLight* getnEvirotmentLight();


protected:
	Triangle getTriangleData(const std::vector<float4>& data, int index) const;
	EnvironmentLight* environmentLight = nullptr;

	std::vector<int> indices;
};

class SceneItem
{
public:
	void setTransform(const Transform& transformation)
	{
		transform = transformation;
	}

	inline Transform getTransform()
	{
		return transform;
	}

	virtual void addToScene(Scene* scene, float4x4 currentTransformMatrix = float4x4()){}
protected:
	Transform transform;
};

class SceneNode : public SceneItem
{
public:
	void addToScene(Scene* scene, float4x4 currentTransformMatrix = float4x4())
	{
		currentTransformMatrix = transform.getTransformMatrix()*currentTransformMatrix;
		for(SceneItem* node : childNode)
			node->addToScene(scene,currentTransformMatrix);
	}

	void addChild(SceneItem* node)
	{
		childNode.push_back(node);
	}
protected:
	std::vector<SceneItem*> childNode;
};

class SceneMeshNode : public SceneItem
{
public:
	void setMesh(Mesh* m)
	{
		mesh = m;
	}

	Mesh* getMesh()
	{
		return mesh;
	}

	void addToScene(Scene* scene, float4x4 currentTransformMatrix = float4x4())
	{
		currentTransformMatrix = transform.getTransformMatrix()*currentTransformMatrix;
		scene->addMesh(mesh,currentTransformMatrix);
	}
protected:
	Mesh* mesh = nullptr;
};

}

#endif
