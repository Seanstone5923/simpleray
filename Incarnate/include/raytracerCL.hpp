#ifndef RAYTRACERCL_H
#define RAYTRACERCL_H

#include <EasyCl.h>
#include <scene.h>

#ifdef OCL_KERNEL
struct Camera
{
    float4 position, direction
    float4 right, up;
	int width, height;
};
#endif

namespace Incarnate
{

class RaytracerCL
{
public:
    int init();
    void terminate();
    float* getBuffer();
    void createBuffer(int width, int height);
    void render();
    int getWidth();
	int getHeight();

protected:
    EasyCl::DeviceManager* deviceManager;
    EasyCl::ComputeDevice* device;
    EasyCl::SourceCode* raytracerSource;
    EasyCl::Software* raytracerProgram;
    EasyCl::Kernel* raytracerKernel;
    EasyCl::Kernel* raycastKernel;

    float* frameBuffer = NULL;
    int bufferWidth;
    int bufferHeight;

    Scene scene;
}

int RaytracerCL::init()
{
	deviceManager = NULL;
	device = NULL;
	raytracerSource = NULL;
	raytracerProgram = NULL;
	raytracerKernel = NULL;

	deviceManager = new EasyCl::DeviceManager;
	device = deviceManager->defaultDevice(CL_DEVICE_TYPE_CPU);
	raytracerSource = new EasyCl::SourceCode;
	raytracerSource->load("kernels/raytracer.cl");

	if(raytracerSource->good() == 0)
		return 0;
	raytracerProgram = new EasyCl::Software(device,*raytracerSource);
	if(raytracerProgram->build() != CL_SUCCESS)
		return 0;
	raytracerKernel = new EasyCl::Kernel(*raytracerProgram,"raytrace");
	raycastKernel = new EasyCl::Kernel(*raytracerProgram,"raycast");
}

void RaytracerCL::terminate()
{
	if(deviceManager != NULL)
		delete deviceManager;
	if(raytracerSource != NULL)
		delete raytracerSource;
	if(raytracerProgram != NULL)
		delete raytracerProgram;
	if(raytracerKernel != NULL)
		delete raytracerKernel;
	if(raycastKernel != NULL)
		delete raycastKernel;
}

void RaytracerCL::createBuffer(int width, int height)
{
	raytracerKernel->setArgBuffer(0,CL_MEM_WRITE_ONLY | CL_MEM_USE_HOST_PTR,
		width*height*4*sizeof(float),frameBuffer);
	//raytracerKernel->setArgBuffer(1,CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
	//	sizeof(int)*2,&size);
}

void RaytracerCL::setCamera(Camera cam)
{
	raytracerKernel->setArgBuffer(1,CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
		sizeof(Camera),&cam);
	//camera = cam;
}

void RaytracerCL::render()
{
	int width = getWidth();
	int height = getHeight();
	int indicesCount = scene.indices.size();

	Ray* rays = new Ray[camera.width*camera.height];
	RayHit* rayHit = new RayHit[camera.width*camera.height];
	int rayNum = camera.width*camera.height;
	for(int i=0;i<camera.width*camera.height;i++)
	{
		int x = i % camera.width;
		int y = i / camera.width;

		float4 rayVec = ((float)x/(float)camera.width-0.5f)*camera.right
			+ (0.5f-(float)y/(float)camera.height)*camera.up
			+ camera.direction;
		rays[i].origin = camera.position;
		rays[i].direction = rayVec;
	}

	raycastKernel->setArgBuffer(0,CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR
		,camera.width*camera.height*sizeof(Ray), rays);
	raycastKernel->setArgBuffer(1,CL_MEM_WRITE_ONLY | CL_MEM_USE_HOST_PTR,
		camera.width*camera.height*sizeof(RayHit),rayHit);
	raycastKernel->setArgBuffer(2,CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
		sizeof(int), &rayNum);
	//
	raycastKernel->setArgBuffer(3,CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
		scene.vertices.size()*sizeof(float4),&scene.vertices.front());
	raycastKernel->setArgBuffer(4,CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
		scene.indices.size()*sizeof(int),&scene.indices.front());
	raycastKernel->setArgBuffer(5,CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
		sizeof(int),&indicesCount);

	raycastKernel->enqueueSPMD();
	raycastKernel->readBuffer(1,camera.width*camera.height*sizeof(RayHit),rayHit);

	for(int i=0;i<camera.width*camera.height;i++)
	{
		if(rayHit[i].index != -1)
		{
			//Simple lighting
			float4 position = float4(0.0f,3.0f,0.0f,0.0f);
			float strength = 20;
			float4 intersection = rays[i].origin + normalize(rays[i].direction)*rayHit[i].distanceBwt;
			float len = length(position-intersection);
			float albedo = 1/(len*len);
			albedo *= strength;
			albedo *= dot(float4(0.0f,1.0f,0.0f,0.0f),normalize(position-intersection));
			//cout << dot(float3(0.0f,1.0f,0.0f),normalize(vec)) << endl;
			((float4*)frameBuffer)[i] = (float4)(1.0f,1.0f,1.0f,1.0f)*albedo;
			//((float4*)frameBuffer)[i] = (float4)(1.0f,1.0f,1.0f,1.0f);
		}
		else
			((float4*)frameBuffer)[i] = float4(0.0f,0.0f,0.0f,1.0f);
		((float4*)frameBuffer)[i].w = 1.0f;
	}
	//NOTE: We still cat get out UV coord now.

	/*raytracerKernel->setArgBuffer(2,CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR,
		vertices.size()*sizeof(float4),&vertices.front());

	raytracerKernel->setArgBuffer(3,CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR,
		indices.size()*sizeof(int),&indices.front());

	raytracerKernel->setArgBuffer(4,CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
		sizeof(int),&indicesCount);

	raytracerKernel->enqueueSPMD();
	raytracerKernel->readBuffer(0,width*height*4*sizeof(float),frameBuffer);*/

	delete [] rays;
	delete [] rayHit;
}

}

#endif
