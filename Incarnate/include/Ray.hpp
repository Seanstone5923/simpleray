#ifndef RAY_HPP
#define RAY_HPP

#include <Vec.hpp>

#include <limits.h>
namespace Incarnate
{

class RayHit
{
public:
	float t;
	float u,v;
	int index;
};

class Ray
{
public:
	Ray(){}
	inline Ray(float3 ori, float3 dir)
	{
		origin = ori;
		direction = dir;
	}

	float3 origin;
	float3 direction;

	inline void calcSign(int* sign) const
	{
		sign[0] = direction[0] < 0;
		sign[1] = direction[1] < 0;
		sign[2] = direction[2] < 0;
	}

	inline float3 inverseDirection() const
	{
		//Workarround a bug/unlikelly issue in Ray-AABB intersection caused by inf in inverseDirection.
		float3 inverseDirection = 1.0f/direction;
		inverseDirection = inverseDirection + float3(glm::equal(direction,float3(0)))*float3(FLT_EPSILON);
		return inverseDirection;
	}
};

}

#endif
