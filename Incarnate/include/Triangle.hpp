#ifndef TRIANGLE_HPP
#define TRIANGLE_HPP

#include <Vec.hpp>

namespace Incarnate
{

class Triangle
{
public:
	Triangle(){}
	Triangle(float4 p1,float4 p2,float4 p3)
	{
		vertex[0] = p1;
		vertex[1] = p2;
		vertex[2] = p3;
	}

	inline float3 normal() const
	{
		return float3(normalize(cross(vertex[1]-vertex[0],vertex[2]-vertex[0])));
	}

	inline float4 interpolate(float u, float v) const
	{
		return vertex[0] + u*(vertex[1] - vertex[0]) + v*(vertex[2]-vertex[0]);
	}

	float4 center()
	{
		return (vertex[0] + vertex[1]+ vertex[2])/3.0f;
	}

	inline float4  interpolate(float2 uv) const
	{
		return interpolate(uv.x,uv.y);
	}

	inline float4 center() const
	{
		return (vertex[0] + vertex[1]+ vertex[2])/3.0f;
	}

	inline float surfaceArea() const
	{
		return length(cross(vertex[1]-vertex[0],vertex[2]-vertex[0]))*0.5f;
	}
	float4 vertex[3];
};

class Ray;

float2 uniformSampleTriangle();

float4 calcReflect(float4 normal, float4 rayDir);
float findIntersection(const Ray& ray,const Triangle& triangle, float4* uv);
Ray createRandomReflect(Ray ray, Triangle triangle,float4 intersection);
}

#endif
