#ifndef BRDF_HPP
#define BRDF_HPP

#include <Vec.hpp>
#include <Incarnate.hpp>
#include <Ray.hpp>
#include <TriangleProperty.hpp>

namespace Incarnate
{

class BXDF
{
public:
	virtual float4 pdf(const Ray& ri, const Ray& ro, float3 normal, const Intersection& property) const = 0;
	//virtual float4 evaluate(const Ray& ri, const Ray& ro, float4 normal, const Intersection& property) const;
	virtual void sample(const Ray& ray, float3 normal, const Intersection& intersection
		, Ray& sampleRay, float4& coefficient) = 0;
};

class LambertianBRDF : public BXDF
{
public:
	static float3 cosineSampleHemisphere(float u1, float u2)
	{
		const float r = sqrtf(u1);
		const float theta = 2.0 * M_PI * u2;

		const float x = r * cosf(theta);
		const float y = r * sinf(theta);

		return float3(x, y, sqrtf(std::max(0.0f, 1.0f - u1)));
	}

	static Ray createCosineReflect(const float3& normal, const float4& intersection)
	{
		float3 rotX, rotY;
		ons(normal, rotX, rotY);
		float3 sampledDir = cosineSampleHemisphere(rand1(),rand1());
		float3 rotatedDir;
		rotatedDir.x = dot(float3(rotX.x, rotY.x, normal.x),sampledDir);
		rotatedDir.y = dot(float3(rotX.y, rotY.y, normal.y),sampledDir);
		rotatedDir.z = dot(float3(rotX.z, rotY.z, normal.z),sampledDir);

		float3 direction = rotatedDir;	// already normalized
		float3 origin = float3(intersection)
			+direction*1e-3f;
		Ray reflectRay;
		reflectRay.origin = float3(origin);
		reflectRay.direction = float3(direction);

		return reflectRay;
	}
	static inline float calcCosineReflectCoeff(const Ray& ray, const float3 normal)
	{
		return 1.f;
	}

	virtual float4 pdf(const Ray& ri, const Ray& ro, float3 normal, const Intersection& property) const
	{
		if(dot(float3(normal),ri.direction) > 0)
			normal = -normal;
		float3 norm = float3(normal);
		float factor = std::abs(dot(ro.direction, norm))*((float)(M_1_PI));
		return float4(factor,factor,factor,0);
	}
	virtual void sample(const Ray& ray, float3 normal, const Intersection& intersection
		, Ray& sampleRay, float4& coefficient)
	{
		if(dot(float3(normal),ray.direction) > 0)
			normal = -normal;
		sampleRay = createCosineReflect(normal,intersection.intersection);
		float coeff = calcCosineReflectCoeff(sampleRay,normal);
		coefficient = float4(coeff,coeff,coeff,1);
	}
};


class TransparentBTDF : public BXDF
{
public:
	virtual float4 pdf(const Ray& ri, const Ray& ro, float3 normal, const Intersection& property) const
	{
		return float4(0);
	}
	virtual void sample(const Ray& ray, float3 normal, const Intersection& intersection
		, Ray& sampleRay, float4& coefficient)
	{
		sampleRay.origin = float3(intersection.intersection) + ray.direction*0.00001f;
		sampleRay.direction = ray.direction;
		coefficient = float4(1);
	}
};

class PhongBRDF : public BXDF
{
public:
	virtual float4 pdf(const Ray& ri, const Ray& ro, float3 normal, const Intersection& property) const
	{
		if(dot(float3(normal),ri.direction) > 0)
			normal = -normal;
		float3 reflectDirection = reflect(ri.direction,float3(normal));
		float n = power+1.f;
		float phongCoefficient = powf(dot(ro.direction,reflectDirection),n);
		if(dot(ro.direction,reflectDirection) < 0)
			return float4(0);
		float normalizationFactor = ((n+2.f)*(n+4.f))/(8.f*(powf(2,-n*0.5f)+n));
		float factor = phongCoefficient*normalizationFactor;
		return float4(factor,factor,factor,0);
	}
	virtual void sample(const Ray& ray, float3 normal, const Intersection& intersection
		, Ray& sampleRay, float4& coefficient)
	{
		if(dot(float3(normal),ray.direction) > 0)
			normal = -normal;

		float3 reflectDirection = reflect(ray.direction,float3(normal));
		float2 rnd(rand1(),rand1());
		float phi = 2.0f*M_PI*rnd.x;
		float theta = acos(powf(rnd.y,1.0f/(power+2)));
		float4 rotX,rotY;
		ons(float4(reflectDirection,0),rotX,rotY);
		float3 result = cosf(phi)*sinf(theta)*float3(rotX)
			+ sinf(phi)*sinf(theta)*float3(rotY)
			+ cosf(theta)*reflectDirection;
		sampleRay.origin = float3(intersection.intersection) + result*1e-5f;

		//So the sample ray dones't hit the surface again
		if(dot(result,float3(normal)) < 0)
			result = -result;
		sampleRay.direction = result;

		coefficient = float4((float)((power+1)/(power+2)));
	}

	inline void setPower(float val)
	{
		power = val;
	}

	const inline float getPower() const
	{
		return power;
	}
protected:
	float power = 1.0f;
};

}

#endif
