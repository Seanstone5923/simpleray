#ifndef TRIANGLE_PROPERTY_HPP
#define TRIANGLE_PROPERTY_HPP

#include <Vec.hpp>
#include <Triangle.hpp>

namespace Incarnate
{

class Material;
class DataBufferArray;

class TriangleProperty
{
public:
	int globalIndex;
	int internalIndex;
	Material* material;
	DataBufferArray* dataBufferArray;
};

class Intersection : public TriangleProperty
{
public:
	float4 intersection;
	float3 normal;
	float4 direction;
	float2 uv;
	float t;
};

}

#endif
