#ifndef TRANSFORM_HPP
#define TRANSFORM_HPP

#include <Vec.hpp>

#include <glm/gtx/transform.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <glm/matrix.hpp>
#include <glm/gtc/type_ptr.hpp>

namespace Incarnate
{

class Transform
{
public:

	float3 getTranslation();

	float4x4 getScalingMatrix();
	float4x4 getTranlationMatrix();
	float4x4 getRotationMatrix();

	float4x4 getTransformMatrix();

	void translate(float3 vec);
	void rotate(float3 vec, float rad);
	void scale(float3 vec);
	void setTransformMatrix(float4x4 matrix);

	void applyComponent();

protected:
	bool manuelTransformMatrixApplied = false;

	float3 translation = float3(0,0,0);
	float3 rotateVector = float3(0,0,1);
	float radiance = 0;
	float3 scaling = float3(1,1,1);

	float4x4 transformMatrix;
};

}

#endif
