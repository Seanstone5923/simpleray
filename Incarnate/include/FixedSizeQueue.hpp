#ifndef FIX_SIZE_QUEUE_HPP
#define FIX_SIZE_QUEUE_HPP

namespace Incarnate
{

template <typename T>
class FixedSizeQueue
{
public:
	FixedSizeQueue(int queueSize)
	{
		buffer = new T[queueSize];
		size = queueSize;
	}

	virtual ~FixedSizeQueue()
	{
		delete [] buffer;
		buffer = nullptr;
		size = 0;
	}

	//FIXME: this cannot be compiled
	FixedSizeQueue(const FixedSizeQueue<T>& other)
	{
		buffer = new T[other.getSize()];
		size = other.getSize();

		for(int i=0;i<currentSize;i++)
			buffer[i] = other[i];
	}

	inline T& operator [](int index)
	{
		return buffer[index];
	}

	const inline T& operator [](int index) const
	{
		return buffer[index];
	}

	inline void enqueue(const T& data)
	{
		buffer[currentSize++] = data;
	}

	inline void clear()
	{
		currentSize = 0;
	}

	inline void dequeue()
	{
		currentSize--;
	}

	inline int getSize() const
	{
		return size;
	}

	inline void setSize(size_t val)
	{
		size = val;
	}

protected:
	T* buffer = nullptr;
	int size = 0;
	int currentSize = 0;
};

}

#endif
