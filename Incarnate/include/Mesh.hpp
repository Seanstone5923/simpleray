#ifndef MESH_HPP
#define MESH_HPP

#include <Vec.hpp>
#include <DataBuffer.hpp>

namespace Incarnate
{

class Material;

class Mesh
{
public:
	Mesh();
	virtual ~Mesh();

	void setVertices(const float4* verts, int num);
	void setIndices(const int* inds, int num);
	void setNormals(const float4* norms, int num);
	void setUVs(const float2* uvPtr, int num);
	void setMaterial(Material* materialPtr);

	inline int getVerticesCount()
	{
		return vertices.getSize()/sizeof(float4);
	}

	inline float4* getVertices()
	{
		return (float4*)vertices.getBuffer();
	}

	inline int getIndicesCount()
	{
		return indices.getSize()/sizeof(int);
	}

	inline int* getIndices()
	{
		return (int*)indices.getBuffer();
	}

	inline Material* getMaterial()
	{
		return material;
	}

	DataBufferArray* getDataBufferArray();

protected:
	DataBufferArray dataBufferArray;
	DataBuffer vertices;
	DataBuffer indices;
	DataBuffer normals;
	DataBuffer uvs;

	Material* material = nullptr;
};

}


#endif
