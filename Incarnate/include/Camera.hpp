#ifndef CAMERA_HPP
#define CAMERA_HPP

#include <Vec.hpp>
#include <Ray.hpp>

namespace Incarnate
{

class Camera
{
public:
	Camera();
	Camera(float4 camPosition, float4 camDirection,
		float4 camRight, float4 camUp);
	void lookAt(float4 camPos, float4 lookAt, float4 upVec);
	void setAspectRatio(float ratio);
	float getAspectRatio() const;
	virtual Ray createCameraRay(float2 uv) const;
	virtual void update();

	float4 position, direction;
	float4 right, up;

protected:
	float pixelAspectRatio = 1.0f;
};

class PerspectiveCamera : public Camera
{
public:
	void setFOV(float2 filedOfView);
	void setFOV(float fovX, float fovY);
	inline float2 getFOV() const
	{
		return fov;
	}
	virtual Ray createCameraRay(float2 uv) const;
	void update();
protected:
	float4 rightVec;
	float4 upVec;
	float2 fov = float2(M_PI/2.0);//default as 45 deg of fov
};

}

#endif
