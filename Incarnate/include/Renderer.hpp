#ifndef RENDERER_HPP
#define RENDERER_HPP

#include <Ray.hpp>
#include <Vec.hpp>
#include <Film.hpp>
#include <random>

namespace Incarnate
{

class Accelerator;
class LightSource;
class AreaLightSource;
class Intersection;
class EnvironmentLight;

class Renderer
{
public:
	virtual void clearBuffer();
	Renderer();
	virtual ~Renderer();
	virtual int init();
	void terminate();

	virtual void initRender();
	virtual void createBuffer(int width, int height);
	const Film* getBuffer() const;
	virtual void render();
	virtual float4 trace(Ray ray){return float4(0);};
	int getWidth() const;
	int getHeight() const;
	int getSampleNum() const;
        virtual void setSampleNum(int num);
        int getBounceDepth() const;
        virtual void setBounceDepth(int num);
	int getCurrentSampleCount() const;
	virtual void setAccelerator(Accelerator* ptr);
	Accelerator* getAccelerator();
	const Accelerator* getAccelerator() const;

protected:
	int bufferWidth;
	int bufferHeight;
	int sampleNum = 3;
	int bounceDepth = 7;
	int currentSampleNum = 0;

	Accelerator* accelerator = nullptr;

	int2 calculatePixelCoord(int index);
	Scene* scene = nullptr;

	Film film;
};

class ExplcitRenderer : public Renderer
{
	void render();
	virtual float4 integratePoint(Ray ray, RayHit hit) = 0;
};

class VarianceRenderer : public Renderer
{
public:
	void clearBuffer();
	void terminate();
	void createBuffer(int width, int height);

	void initRender();
	void setBaseRenderer(Renderer* renderer);
	void render();

	void setSampleNum(int num);
	void setBounceDepth(int num);
	void setAccelerator(Accelerator* ptr);

protected:
	Renderer* baseRenderer = nullptr;

	float4* cumulationBuffer = nullptr;
	float4* squareCumulationBuffer = nullptr;
	float4* baseBuffer = nullptr;
};

class DirectLightingRenderer : public Renderer
{
public:
	~DirectLightingRenderer();
	void clearBuffer();
	void initRender();
	float4 trace(Ray ray);
	void terminate();

protected:
	float4 estimateDirect(const Ray& ri, float3 normal, const Intersection& intersection);
	float4 estimateDirectMIS(const Ray& ri, float3 normal
		, const Intersection& intersection, Ray* sampleRay,RayHit* sampleHit, float4* coefficient);

	std::vector<LightSource*> lights;

	EnvironmentLight* environmentLight = nullptr;

	//XXX: race condition and cache invalidation in rng under multi-threading
	std::uniform_int_distribution<int> randomLightSelector;
	std::mt19937* rng = nullptr;//mt19937 is faster than pcg here
};

class PathRenderer : public DirectLightingRenderer
{
public:
	float4 trace(Ray ray);

protected:
};

class NormalRenderer : public ExplcitRenderer
{
public:
	virtual float4 integratePoint(Ray ray, RayHit hit);
protected:

};

}

#endif
