#ifndef TEXTURE_HPP
#define TEXTURE_HPP

#include <Vec.hpp>
#include <Film.hpp>

#include <string>

namespace Incarnate
{
class Image
{
public:
	int getWidth() const;
	int getHeight() const;
	bool save(const std::string&, float gamma=2.2f) const;
	//A dummy method that does nothing. Should be implement in the derived class
	virtual bool load(const std::string& path, float gamma=2.2f);

	virtual bool good() const = 0;
	virtual float4 getPixel(int x, int y) const = 0;
	virtual void create(int w, int h) = 0;

protected:
	bool saveHDR(const std::string&, float gamma=2.2f) const;//A hack to save hdr images
	int width, height;
};

class HDRImage : public Image
{
public:
	HDRImage();
	HDRImage(const std::string& path);
	virtual ~HDRImage();
	bool load(const std::string& path, float gamma=2.2f);
	bool load(const float4* data, int w, int h);
	bool load(const Film* data);
	float4* getRaw();
	void unload();

	bool good() const;
	float4 getPixel(int x, int y) const;
	void create(int w, int h);
protected:
	float4* buffer = nullptr;
};

class LDRImage: public Image
{
public:
	LDRImage();
	LDRImage(const std::string& path);
	virtual ~LDRImage();
	bool load(const std::string& path, float gamma=2.2f);
	bool load(const unsigned char* data, int w, int h);
	unsigned char* getRaw();
	void unload();

	bool good() const;
	float4 getPixel(int x, int y) const;
	void create(int w, int h);
protected:
	unsigned char* buffer = nullptr;
};

enum class TextureFilteringMethod
{
	Nearest = 0,
	Bilinear
};

enum class UVWrapMode
{
	Clamp = 0,
	Repeat,
	MirroredRepeat
};

class Texture
{
public:
	float4 getPixel(float2 uv);

	void setImage(Image* img);
	inline Image* getImage()
	{
		return image;
	}

	float2 wrapUV(const float2& uv) const;

	void setFilteringMethod(TextureFilteringMethod method);
	TextureFilteringMethod getFilteringMethod() const;
protected:
	Image* image = nullptr;
	TextureFilteringMethod filteringMethod = TextureFilteringMethod::Bilinear;
	UVWrapMode uvWrapMode = UVWrapMode::Repeat;
};

class EnvironmentMap
{
public:
	virtual float4 getPixel(float4 vec) const = 0;
protected:
};

class SphereMap : public EnvironmentMap
{
public:
	float4 getPixel(float4 direction) const;
	void setTexture(Texture* tex);
	Texture* getTexture();
protected:
	Texture* texture = nullptr;
};

}

#endif
