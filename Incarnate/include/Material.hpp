#ifndef MATERIAL_HPP
#define MATERIAL_HPP

#include <Vec.hpp>
#include <Ray.hpp>
#include <BRDF.hpp>

namespace Incarnate
{
class DataBuffer;
class DataBufferArray;
class Texture;
class Intersection;
class Triangle;
class AreaLightSource;

class Material
{
public:
	static float4 hemisphere(float u1, float u2);

	// I don't think getNormal() is something that should be in Material.
	static float3 getNormal(const Intersection& intersection);
	static float3 getNormal(const float3& normal, float2 uv, const DataBufferArray* dataBufferArray, int index);
	static float3 getNormal(const float3& normal, const Intersection& intersection);

	static Ray createRandomReflect(const float3& normal, const float4& intersection);
	static inline float calcRandomReflectCoeff(const Ray& ray, const float3 normal)
	{
		return dot(normalize(ray.direction),normal)*2.f;
	}

	static float4 cosineSampleHemisphere(float u1, float u2)
	{
		const float r = sqrtf(u1);
		const float theta = 2.0 * M_PI * u2;

		const float x = r * cosf(theta);
		const float y = r * sinf(theta);

		return float4(x, y, sqrtf(std::max(0.0f, 1.0f - u1)),0);
	}

	static Ray createCosineReflect(const float4& normal, const float4& intersection);
	static inline float calcCosineReflectCoeff(const Ray& ray, const float4 normal)
	{
		return 1.f;
	}

	//The acuatl APIs
	//Using Lambertian reflection as a default material.

	Material(float4 reflect = float4(0,0,0,1))
	{
		reflectColor = reflect;
	}

	//NOTE: pdf() is evaluate() without color
	virtual float4 pdf(const Ray& ri, const Ray& ro, float3 normal, const Intersection& property) const;
	virtual float4 evaluate(const Ray& ri, const Ray& ro, float3 normal, const Intersection& property) const;

	virtual void sample(const Ray& ray, float3 normal, const Intersection& intersection
		, Ray& sampleRay, float4& coefficient);

	virtual float4 getEmit(const Intersection& intersection) const;
	virtual float4 getReflect(const Intersection& intersection) const;
	virtual void setReflect(float4 reflect);
	inline bool isEmit() const
	{
		return emitProp != nullptr;
	}

	virtual const Material* getBSDF(const Intersection& intersection) const;
	virtual Material* getBSDF(const Intersection& intersection);
	Material* resolve(const Intersection& intersection);

	virtual ~Material();
	virtual bool specular(const Intersection& intersetion) const;

	void setTexture(Texture* tex, int index);
	Texture* getTexture(int index);

	void setEmitProperty(AreaLightSource* prop);
	const AreaLightSource* getEmitProperty() const;

protected:
	bool isSpecular = false;
	AreaLightSource* emitProp = nullptr;

	float4 reflectColor;

	inline void setSpecular(bool specular)
	{
		isSpecular = specular;
	}

	Texture* textures[16] = {nullptr};
};

class MirrorMaterial : public Material
{
public:
	MirrorMaterial(float4 color = float4(1,1,1,0));

	virtual void sample(const Ray& ray, float3 normal, const Intersection& intersection
		, Ray& sampleRay, float4& coefficient);
	virtual float4 pdf(const Ray& ri, const Ray& ro, float3 normal, const Intersection& property) const override;

protected:

};

class PhongMaterial : public Material
{
public:
	PhongMaterial(float4 color = float4(1,1,1,0), float powerVal = 0.0f);
	void setPower(float val);
	float getPower() const;

	virtual void sample(const Ray& ray, float3 normal, const Intersection& intersection
		, Ray& sampleRay, float4& coefficient);
	virtual float4 pdf(const Ray& ri, const Ray& ro, float3 normal, const Intersection& property) const override;
protected:
	//PhongBRDF brdf;
	float power = 1.0;
};

class MixedMaterial : public Material
{
public:
	void setBSDF(Material* material, int index);
	void setFirstBSDF(Material* material);
	void setSecondBSDF(Material* material);

	virtual const Material* getBSDF(const Intersection& intersection) const;

	void setRatio(float mixRatio);
	float getRatio();

protected:
	Material* materials[2] = {nullptr};
	float ratio = .5f;
};

class TransparentMaterial : public Material
{
public:
	TransparentMaterial();

	float4 pdf(const Ray& ri, const Ray& ro, float3 normal, const Intersection& property) const;
	virtual void sample(const Ray& ray, float3 normal, const Intersection& intersection
		, Ray& sampleRay, float4& coefficient);

protected:
	TransparentBTDF btdf;
};

class MatteMaterial : public Material
{
public:
	MatteMaterial(float4 color = float4(1));

	virtual void sample(const Ray& ray, float3 normal, const Intersection& intersection
		, Ray& sampleRay, float4& coefficient);
	virtual float4 pdf(const Ray& ri, const Ray& ro, float3 normal, const Intersection& property) const override;
	virtual bool specular(const Intersection& intersetion) const;

	virtual const Material* getBSDF(const Intersection& intersection) const;

protected:
	LambertianBRDF brdf;
	TransparentMaterial transparentMaterial;
};

}
#endif
