project(Incarnate)

file(GLOB INCARNATE_SRC "src/*.cpp")
file(GLOB INCARNATE_STB_SRC "src/3rdparty/STB/*.c")
include_directories("include/3rdparty/STB")

#for STB
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -O3")

add_library(Incarnate SHARED ${INCARNATE_SRC} ${INCARNATE_STB_SRC})
install(TARGETS Incarnate Incarnate DESTINATION /usr/lib)
install(DIRECTORY include/ DESTINATION /usr/include/Incarnate)

#HACK: don't find FineEmbree.cmake anywhere. So just link to it
if(INC_EMBREE_ACCELERATOR)
	target_link_libraries(Incarnate embree)
endif()
