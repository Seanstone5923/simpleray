#ifndef PBRT_BASE_PARSERS_HPP
#define PBRT_BASE_PARSERS_HPP
#include <Vec.hpp>

#include <PBRTArgument.hpp>

#include <map>
#include <string>
#include <type_traits>

namespace Incarnate
{
class PBRTSceneInfo;

class PBRTArgumentParser
{
public:
	virtual int parse(const ArgumentVector& argv, int index) = 0;
protected:
};

struct PBRTParserProp
{
	PBRTArgumentParser* parser = NULL;
	std::string defaultValue;
	bool required = false;
};

typedef std::map<ArgumentVector,PBRTParserProp> PBRTNameParserPairs;

template<typename T>
class PBRTValueParser : public PBRTArgumentParser
{
public:
	//Return the number of arguments the prasing is used
	int parse(const ArgumentVector& argv, int index);
	void setDestination(T* ptr)
	{
		destination = ptr;
	}
	T* getDestination()
	{
		return destination;
	}
	const T* getDestination() const
	{
		return destination;
	}
protected:

	T* destination = NULL;
};

template<typename T>
class PBRTArrayParser : public PBRTValueParser<std::vector<T>>
{
public:
	int parse(const ArgumentVector& argv, int index)
	{
		if(index+1 >= argv.size())
		{
			std::cout << "Error: Parameter not avliable at the end of command." << '\n';
			return 0;
		}

		const Argument& arg = argv[index+1];

		if(!arg.isList())
		{
			std::cout << "Error: not a list." << '\n';
			return 0;
		}
		std::string content = arg.getListContent();
		ArgumentVector decomposed(content);
		std::vector<T> result;
		result.reserve(decomposed.size());
		int desireSize = getParseSize();

		if(decomposed.size() != desireSize && desireSize > 0)
		{
			std::cout << "Error: " << desireSize << " value is desired. But "
				<< decomposed.size() << " is provided.\n";
		}

		PBRTValueParser<T> parser;
		ArgumentVector fakeArg;
		fakeArg.resize(2);
		fakeArg[0] = std::string("foo");
		for(const Argument& a : decomposed)
		{
			T val;
			fakeArg[1] = a;
			parser.setDestination(&val);
			if(parser.parse(fakeArg,0) <= 0)
			{
				return 0;
			}
			result.push_back(val);
		}
		*(this->destination) = result;

		return 2;
	}
	void setParseSize(int size)
	{
		targetParseSize = size;
	}
	int getParseSize()
	{
		return targetParseSize;
	}
protected:
	int targetParseSize = -1;
};

typedef PBRTValueParser<std::string> PBRTStringParser;
typedef PBRTValueParser<int> PBRTIntegerParser;
typedef PBRTValueParser<float> PBRTFloatParser;
typedef PBRTValueParser<bool> PBRTBoolenParser;

typedef PBRTArrayParser<std::string> PBRTStringArrayParser;
typedef PBRTArrayParser<int> PBRTIntegerArrayParser;
typedef PBRTArrayParser<float> PBRTFloatArrayParser;
typedef PBRTArrayParser<bool> PBRTBoolenArrayParser;


class PBRTFlagParser : public PBRTArgumentParser
{
public:
	//Return the number of arguments the prasing is used
	virtual int parse(const ArgumentVector& argv, int index);
	void setDestination(bool* var);
	void setStoreValue(bool val);
	bool getStoreValue() const;
protected:

	bool* storePtr = NULL;
	bool storeValue = true;
};

template<typename Test, template<typename...> class Ref>
struct is_specialization : std::false_type {};
template<template<typename...> class Ref, typename... Args>
struct is_specialization<Ref<Args...>, Ref>: std::true_type {};


class PBRTCommandParser
{
public:
	virtual ~PBRTCommandParser();
	bool parse(const ArgumentVector& argv);
	bool success() const;

	template<typename T>
	bool addArgument(const ArgumentVector& name, T* ptr ,bool required=false
		, const std::string& defaultVal="", int targetSize = -1)
	{
		//Use constexpt here after C++17 is released
		PBRTValueParser<T>* parser = new PBRTValueParser<T>;
		parser->setDestination(ptr);
		return addParser(name, parser,required, defaultVal);
	}

	bool addFlag(const ArgumentVector& name, bool* ptr);
protected:

	PBRTArgumentParser* getParser(const ArgumentVector& argv);
	PBRTParserProp* getParserProp(const ArgumentVector& argv);
	bool addParser(const ArgumentVector& name, PBRTArgumentParser* parser, bool required=false
		, const std::string& defaultVal="");
	PBRTNameParserPairs nameParserPairs;
	bool good = false;
};

template<>
bool PBRTCommandParser::addArgument(const ArgumentVector& name, std::vector<int>* ptr ,bool required
	, const std::string& defaultVal, int targetSize);
template<>
bool PBRTCommandParser::addArgument(const ArgumentVector& name, std::vector<float>* ptr ,bool required
	, const std::string& defaultVal, int targetSize);
template<>
bool PBRTCommandParser::addArgument(const ArgumentVector& name, std::vector<std::string>* ptr ,bool required
	, const std::string& defaultVal, int targetSize);
template<>
bool PBRTCommandParser::addArgument(const ArgumentVector& name, std::vector<bool>* ptr ,bool required
	, const std::string& defaultVal, int targetSize);


template <typename T>
bool parseValue(const ArgumentVector& value, T* ptr, int index = 0, int targetSize=-1)
{
	if(!is_specialization<T,std::vector>::value)
	{
		PBRTValueParser<T> parser;
		parser.setDestination(ptr);
		return parser.parse(value,index-1);
	}
	else
	{
		PBRTArrayParser<typename T::value_type> parser;
		parser.setDestination(ptr);
		parser.setParseSize(targetSize);
		return parser.parse(value,index-1);
	}
}

std::string makeList(const ArgumentVector& value, int index, int size);


}
#endif
