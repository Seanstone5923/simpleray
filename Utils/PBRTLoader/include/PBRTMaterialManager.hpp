#ifndef PBRT_MATERIAL_MANAGER_HPP
#define PBRT_MATERIAL_MANAGER_HPP

#include <PBRTResourceManager.hpp>

namespace Incarnate
{

class Material;

class MaterialManager : protected ResourceManager<Material>
{
public:
	void setCurrentMaterial(Material* material);
	Material* getCurrentMaterial();

	template<typename T>
	T* newMaterial(const std::string& name, bool setAsCurrentMaterial = false)
	{
		T* material = newResource<T>(name);
		if(setAsCurrentMaterial)
			setCurrentMaterial(material);
		return material;
	}

	Material* getMaterial(const std::string& name);

	void destroyAll();

protected:
	Material* currentMaterial = NULL;
};

}

#endif
