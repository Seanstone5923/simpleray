#ifndef PBRT_DOCUMENT_HPP
#define PBRT_DOCUMENT_HPP

#include <string>

namespace Incarnate
{

class ArgumentVector;
class PBRTDocument
{
public:
	bool load(std::string filename);
	int nextCommand(ArgumentVector& argv);
	std::string directory();
	int getLineByIndex(int ind);

	std::string data;
	int index = 0;
	int oldIndex = 0;
	std::string path;
};
}

#endif
