#ifndef PBRT_ARGUMENT_HPP
#define PBRT_ARGUMENT_HPP

#include <string>
#include <vector>

namespace Incarnate
{

class Argument
{
public:
	Argument();
	Argument(std::string str, int ind = 0);
	std::string arg;
	int loocaton = -1;//Where the string is in the file. This is not used now
	bool operator ==(const Argument& argument) const;
	bool operator !=(const Argument& argument) const;
	bool operator <(const Argument& argument) const;

	bool isList() const;
	std::string getListContent() const;

	bool isString() const;
	std::string getStringContent() const;

	const std::string& getRaw() const;

	int parseFirstArgument(std::string& result, int start = 0);
};

class ArgumentVector : public std::vector<Argument>
{
public:
	bool operator==(ArgumentVector const& argv) const;
	bool operator!=(ArgumentVector const& argv) const;

	ArgumentVector();
	//break a string to multiple Statements, same as parse().
	ArgumentVector(const std::string& data);
	ArgumentVector(const char* data);

	//break a string to multiple Statements
	int parse(const std::string& data, int start = 0);
	//Only break the first segment
	int parseFirstCommand(const std::string& data, int start = 0);
};

std::ostream& operator<< (std::ostream& out, const Argument& arg);
std::ostream& operator<< (std::ostream& out, const ArgumentVector& argv);

}

#endif
