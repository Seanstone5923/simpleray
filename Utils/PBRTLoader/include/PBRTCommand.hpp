#ifndef PBRT_COMMAND_HPP
#define PBRT_COMMAND_HPP

#include <PBRTBaseParsers.hpp>
#include <PBRTResourceManager.hpp>

#include <vector>

namespace Incarnate
{

class Camera;
class Scene;
class Renderer;
class Accelerator;
class Material;
class MatteMaterial;
class MirrorMaterial;
class PhongMaterial;
class Mesh;
class SceneNode;
class SceneItem;
class PerspectiveCamera;
class PathRenderer;
class DirectLightingRenderer;

class ArgumentVector;
class PBRTLoaderInfo;

class PBRTCommand
{
public:
	PBRTCommand();
	virtual ~PBRTCommand();
	virtual int execute(PBRTLoaderInfo* loaderInfo, const ArgumentVector& argv);
protected:
};

class PBRTSubTypedCommand : public PBRTCommand
{
public:
	template <typename T>
	bool addSubCommand(const std::string& name)
	{
		if(subCommands.find(name) != subCommands.end())
		{
			std::cout << "Error :Sub command *" << name << "* already exists." << std::endl;
			return false;
		}

		subCommands[name] = new T;
		return true;
	}

	bool aliasCommand(const std::string& name, const std::string& target);

	void clear();

	virtual ~PBRTSubTypedCommand();
	virtual int execute(PBRTLoaderInfo* loaderInfo, const ArgumentVector& argv);
protected:
	std::map<std::string, PBRTCommand*> subCommands;
	std::map<std::string, PBRTCommand*> commandAliases;
};

class PBRTFilmCommand : public PBRTCommand
{
public:
	PBRTFilmCommand();
	virtual int execute(PBRTLoaderInfo* loaderInfo, const ArgumentVector& argv);
protected:
	PBRTCommandParser parser;

	bool flag;
	std::vector<int> xresolution;
	std::vector<int> yresolution;
	std::vector<std::string> filename;
};

class PBRTLookAtCommand : public PBRTCommand
{
public:
	virtual int execute(PBRTLoaderInfo* loaderInfo, const ArgumentVector& argv);
protected:
};

class PBRTCameraCommand : public PBRTSubTypedCommand
{
public:
	PBRTCameraCommand();
};

class PBRTPerspectiveCameraCommand : public PBRTCommand
{
public:
	PBRTPerspectiveCameraCommand();
	int execute(PBRTLoaderInfo* loaderInfo, const ArgumentVector& argv);
protected:
	PBRTCommandParser parser;
	std::vector<float> fov;
	std::vector<float> hfov;
};

class PBRTSamplerCommand : public PBRTCommand
{
public:
	PBRTSamplerCommand();
	int execute(PBRTLoaderInfo* loaderInfo, const ArgumentVector& argv);
protected:
	PBRTCommandParser parser;
	bool flag;
	std::vector<int> sampleNum;
};

class PBRTRendererCommand : public PBRTCommand
{
public:
	PBRTRendererCommand();
	int execute(PBRTLoaderInfo* loaderInfo, const ArgumentVector& argv);
protected:
	PBRTCommandParser parser;
	bool flag;
};

class PBRTSurfaceIntegratorCommand : public PBRTSubTypedCommand
{
public:
	PBRTSurfaceIntegratorCommand();
protected:
	PBRTCommandParser parser;
};

class PBRTPathRendererCommand : public PBRTCommand
{
public:
	PBRTPathRendererCommand();
	int execute(PBRTLoaderInfo* loaderInfo, const ArgumentVector& argv);
protected:
	PBRTCommandParser parser;
	std::vector<int> bounceDepth;
};

class PBRTDirectLightingRendererCommand : public PBRTCommand
{
public:
	PBRTDirectLightingRendererCommand();
	int execute(PBRTLoaderInfo* loaderInfo, const ArgumentVector& argv);
protected:
	PBRTCommandParser parser;
};

class PBRTNormalRendererCommand : public PBRTCommand
{
public:
	PBRTNormalRendererCommand();
	int execute(PBRTLoaderInfo* loaderInfo, const ArgumentVector& argv);
protected:
	PBRTCommandParser parser;
};


class PBRTAcceleratorCommand : public PBRTSubTypedCommand
{
public:
	PBRTAcceleratorCommand();
protected:
	PBRTCommandParser parser;
};

class PBRTBVHAcceleratorCommand : public PBRTCommand
{
public:
	PBRTBVHAcceleratorCommand();
	int execute(PBRTLoaderInfo* loaderInfo, const ArgumentVector& argv);
protected:
	PBRTCommandParser parser;
	std::string acceleratorType;
};

#ifdef INC_EMBREE_ACCELERATOR

class PBRTEmbreeAcceleratorCommand : public PBRTCommand
{
public:
	PBRTEmbreeAcceleratorCommand();
	int execute(PBRTLoaderInfo* loaderInfo, const ArgumentVector& argv);
protected:
	PBRTCommandParser parser;
};

#endif

class PBRTBruteforceAcceleratorCommand : public PBRTCommand
{
public:
	PBRTBruteforceAcceleratorCommand();
	int execute(PBRTLoaderInfo* loaderInfo, const ArgumentVector& argv);
protected:
	PBRTCommandParser parser;
};

class PBRTWorldBeginCommand : public PBRTCommand
{
public:
	int execute(PBRTLoaderInfo* loaderInfo, const ArgumentVector& argv);
protected:
};

class PBRTWorldEndCommand : public PBRTCommand
{
public:
	int execute(PBRTLoaderInfo* loaderInfo, const ArgumentVector& argv);
protected:
};

class PBRTAttributeBeginCommand : public PBRTCommand
{
public:
	int execute(PBRTLoaderInfo* loaderInfo, const ArgumentVector& argv);
protected:
};

class PBRTAttributeEndCommand : public PBRTCommand
{
public:
	int execute(PBRTLoaderInfo* loaderInfo, const ArgumentVector& argv);
protected:
};

class PBRTMaterialCommand : public PBRTSubTypedCommand
{
public:
	PBRTMaterialCommand();
};

class PBRTMatteMaterialCommand : public PBRTCommand
{
public:
	PBRTMatteMaterialCommand();
	int execute(PBRTLoaderInfo* loaderInfo, const ArgumentVector& argv);
protected:
	PBRTCommandParser parser;
	std::vector<float> diffuseColor;
	std::string name;
	std::string textureName;
};

class PBRTMirrorMaterialCommand : public PBRTCommand
{
public:
	PBRTMirrorMaterialCommand();
	int execute(PBRTLoaderInfo* loaderInfo, const ArgumentVector& argv);
protected:
	PBRTCommandParser parser;
	std::string name;
	std::vector<float> reflectColor;
};

class PBRTMixedMaterialCommand : public PBRTCommand
{
public:
	PBRTMixedMaterialCommand();
	int execute(PBRTLoaderInfo* loaderInfo, const ArgumentVector& argv);
protected:
	PBRTCommandParser parser;
	std::vector<float> amount;
	std::string material1Name;
	std::string material2Name;
	std::string name;
};

class PBRTPhongMaterialCommand : public PBRTCommand
{
public:
	PBRTPhongMaterialCommand();
	int execute(PBRTLoaderInfo* loaderInfo, const ArgumentVector& argv);
protected:
	PBRTCommandParser parser;
	std::string name;
	std::vector<float> color;
	std::vector<float> power;
};

class PBRTShapeCommand : public PBRTSubTypedCommand
{
public:
	PBRTShapeCommand();
};

class PBRTTriangleMeshCommand : public PBRTCommand
{
public:
	PBRTTriangleMeshCommand();
	int execute(PBRTLoaderInfo* loaderInfo, const ArgumentVector& argv);
protected:
	PBRTCommandParser parser;
	std::vector<float> vertices;
	std::vector<float> normals;
	std::vector<float> uvs;
	std::vector<int> indices;
};

class PBRTAreaLightSourceCommand : public PBRTCommand
{
public:
	PBRTAreaLightSourceCommand();
	int execute(PBRTLoaderInfo* loaderInfo, const ArgumentVector& argv);
protected:
	PBRTCommandParser parser;
	bool flag;
	std::vector<float> radience;
};

class PBRTIncludeCommand : public PBRTCommand
{
public:
	int execute(PBRTLoaderInfo* loaderInfo, const ArgumentVector& argv);
};

class PBRTScaleCommand : public PBRTCommand
{
public:
	int execute(PBRTLoaderInfo* loaderInfo, const ArgumentVector& argv);
};

class PBRTTranslateCommand : public PBRTCommand
{
public:
	int execute(PBRTLoaderInfo* loaderInfo, const ArgumentVector& argv);
};

class PBRTRotateCommand : public PBRTCommand
{
public:
	int execute(PBRTLoaderInfo* loaderInfo, const ArgumentVector& argv);
};

class PBRTIdentityCommand : public PBRTCommand
{
public:
	int execute(PBRTLoaderInfo* loaderInfo, const ArgumentVector& argv);
};

class PBRTTransformCommand : public PBRTCommand
{
public:
	int execute(PBRTLoaderInfo* loaderInfo, const ArgumentVector& argv);
};

class PBRTMakeNamedMaterialCommand : public PBRTCommand
{
public:
	int execute(PBRTLoaderInfo* loaderInfo, const ArgumentVector& argv);
protected:
	PBRTMaterialCommand materialCommand;
};

class PBRTNamedMaterialCommand : public PBRTCommand
{
public:
	int execute(PBRTLoaderInfo* loaderInfo, const ArgumentVector& argv);
};

class PBRTTextureCommand : public PBRTSubTypedCommand
{
public:
	PBRTTextureCommand();
	int execute(PBRTLoaderInfo* loaderInfo, const ArgumentVector& argv);
};

class PBRTImageMapCommand : public PBRTCommand
{
public:
	PBRTImageMapCommand();
	int execute(PBRTLoaderInfo* loaderInfo, const ArgumentVector& argv);
protected:
	PBRTCommandParser parser;
	std::string name;
	std::string filename;
};

class PBRTLightSourceCommand : public PBRTSubTypedCommand
{
public:
	PBRTLightSourceCommand();
};

class PBRTPointghtSourceComman : public PBRTCommand
{
public:
	PBRTPointghtSourceComman();
	int execute(PBRTLoaderInfo* loaderInfo, const ArgumentVector& argv);
protected:
	PBRTCommandParser parser;
	std::vector<float> intensity;
	std::vector<float> from;
};

class PBRTDistantLightSourceCommand: public PBRTCommand
{
public:
	PBRTDistantLightSourceCommand();
	int execute(PBRTLoaderInfo* loaderInfo, const ArgumentVector& argv);
protected:
	PBRTCommandParser parser;
	std::vector<float> intensity;
	std::vector<float> from;
	std::vector<float> to;
};

class PBRTInfiniteLightSourceCommand: public PBRTCommand
{
public:
	PBRTInfiniteLightSourceCommand();
	int execute(PBRTLoaderInfo* loaderInfo, const ArgumentVector& argv);
protected:
	PBRTCommandParser parser;
	std::string mapName;
	std::vector<float> scale;
};

class PBRTGoniometricLightSourceCommand: public PBRTCommand
{
public:
	PBRTGoniometricLightSourceCommand();
	int execute(PBRTLoaderInfo* loaderInfo, const ArgumentVector& argv);
protected:
	PBRTCommandParser parser;
	std::string mapName;
};

}

#endif
