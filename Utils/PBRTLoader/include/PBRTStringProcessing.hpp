#ifndef PBRT_STRING_PROCESSING_HPP
#define PBRT_STRING_PROCESSING_HPP

#include <string>
#include "PBRTArgument.hpp"

namespace Incarnate
{
std::string removeComments(const std::string& data);
void convertToLF(std::streambuf* sb, std::string& out);
std::string combinePath(const std::string& directory, const std::string& appendPath);
std::string extendsionFromPath(const std::string& path);
}

#endif
