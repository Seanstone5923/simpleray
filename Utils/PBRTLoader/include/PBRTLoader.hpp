#ifndef PBRT_LOADER_H
#define PBRT_LOADER_H

#include <string>
#include <vector>

#include "PBRTSceneInfo.hpp"

namespace Incarnate
{

class ArgumentVector;
class PBRTCommand;

class PBRTCommandList
{
public:
	template <typename T>
	bool addCommand(std::string name)
	{
		return (commands.newResource<T>(name) != NULL);
	}

	int execute(PBRTLoaderInfo* loaderInfo, const ArgumentVector& argv);
	void addSubCommandList(PBRTCommandList* commandList);
	PBRTCommand* getCommand(const std::string& name);
protected:
	ResourceManager<PBRTCommand> commands;
	std::vector<PBRTCommandList*> subCommandList;
};

class PBRTLoader
{
public:
	PBRTLoader();
	PBRTLoader(const std::string& path);
	PBRTLoader(const PBRTLoader&) = delete;

	int loadScene(std::string filename);
	void clear();
	bool good();
	virtual ~PBRTLoader();

	Scene* getScene();
	Renderer* getRenderer();
	Camera* getCamera();
	Accelerator* getAccelerator();

	PBRTLoaderInfo loaderInfo;

	int execute(const ArgumentVector& argv);
protected:
	PBRTCommandList commands;
	PBRTCommandList sceneCommands;
	PBRTCommandList attribCommands;

	PBRTCommandList* commandList[3];

	bool isGood = false;
};

}
#endif
