#ifndef PBRT_RESOURCE_MANAGER_HPP
#define PBRT_RESOURCE_MANAGER_HPP

#include <unordered_map>
#include <iostream>
#include <string>
#include <algorithm>    // std::remove

// FIXME: for workaround for Android
#ifdef __ANDROID__
#include <sstream>
#endif

namespace Incarnate
{

class Material;



template <typename BaseType>
class ResourceManager
{
public:
	virtual ~ResourceManager()
	{
		destroyAll();
	}

	template <typename T = BaseType>
	T* newResource(const std::string& name = "")
	{
		T* resource = new T;
		std::string resourceName = name;

		if(resourceName == "")
		{
			int i = 0;
			while(true)
			{
				// FIXME: workaround for Android
				#ifdef __ANDROID__
				std::stringstream generatedNameStream;
				generatedNameStream << "unnamed." << i;
				std::string generatedName = generatedNameStream.str();
				#else
				std::string generatedName = "unnamed." + std::to_string(i);
				#endif
				if(resourceExists(generatedName) == false)
				{
					resourceName = generatedName;
					break;
				}
				i++;
			}
		}

		if(getResource(resourceName) == NULL)
			resources[resourceName] = resource;
		else
		{
			std::cout << "Error: *" << name << "* is already in resource manager" << std::endl;
			delete resource;
			resource = NULL;
		}
		return resource;
	}


	BaseType* getResource(const std::string& name) const
	{
		auto it = resources.find(name);
		if(it == resources.end())
			return NULL;
		return (*it).second;
	}

	bool resourceExists(const std::string& name) const
	{
		return getResource(name) != NULL;
	}

	void destroyAll()
	{
		for(auto it : resources)
			delete it.second;
		resources.clear();
	}

	bool destroy(const std::string& name)
	{
		BaseType* ptr = getResource(name);
		if(ptr == NULL)
		{
			std::cout << "Warrning: Resource *" << name  << "* not found. Cannot be removed." << std::endl;
			return false;
		}
		return destroy(ptr);
	}

	//XXX: Do some check to confirm that ptr is in the resource list?
	bool destroy(BaseType* ptr)
	{
		delete ptr;
		resources.erase(std::remove(resources.begin(), resources.end(), ptr), resources.end());
		return true;
	}
protected:
	std::unordered_map<std::string,BaseType*> resources;
};

}

#endif
