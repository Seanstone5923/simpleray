#ifndef PBRT_SCENE_INFO_HPP
#define PBRT_SCENE_INFO_HPP

#include <Vec.hpp>
#include <Texture.hpp>
#include <LightSource.hpp>

#include <vector>
#include <string>
#include <stack>


#include <PBRTResourceManager.hpp>
#include <PBRTMaterialManager.hpp>
namespace Incarnate
{

class Camera;
class Scene;
class Renderer;
class Accelerator;
class Material;
class Mesh;
class SceneNode;
class SceneItem;
class Texture;
class Image;
class LightSource;
class EnvironmentMap;

class PBRTDocument;

enum class PBRTParserState
{
	RootState = 0,
	WorldState,
	AttributeState
};

class PBRTFilmInfo
{
public:
	int width = 640;
	int height = 480;
	std::string filename;
};

class PBRTLookAtInfo
{
public:
	float3 pos;
	float3 target = float3(0,0,-1);
	float3 up = float3(0,1,0);
};

class PBRTSamplerInfo
{
public:
	int sampleNum = 2048;
};

class PBRTSceneInfo
{
public:
	Renderer* renderer = NULL;
	Camera* camera = NULL;
	Accelerator* accelerator = NULL;
	Scene* scene = NULL;
	SceneNode* rootNode = NULL;
	std::vector<Mesh*> meshes;
	std::vector<SceneNode*> sceneNodes;
	std::vector<SceneItem*> sceneItem;
	MaterialManager materialManager;
	ResourceManager<Image> imageManager;
	ResourceManager<Texture> textureManager;
	ResourceManager<LightSource> lightSourceManager;
	ResourceManager<EnvironmentMap> environmentMapManager;
};

class PBRTLoaderInfo
{
public:
	PBRTFilmInfo filmInfo;
	PBRTLookAtInfo lookAtInfo;
	PBRTSamplerInfo samplerInfo;
	PBRTParserState parserState = PBRTParserState::RootState;
	std::stack<PBRTDocument*> documentStack;
	PBRTDocument* currentDocument = NULL;
	PBRTDocument* rootDocument = NULL;
	Material* worldMaterial = NULL;
	SceneNode* worldNode = NULL;

	PBRTSceneInfo sceneInfo;
};

}

#endif
