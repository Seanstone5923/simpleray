#include "PBRTLoader.hpp"
#include "PBRTArgument.hpp"
#include "PBRTStringProcessing.hpp"
#include "PBRTCommand.hpp"
#include "PBRTBaseParsers.hpp"
#include "PBRTResourceManager.hpp"
#include <PBRTDocument.hpp>

#include <Camera.hpp>
#include <Scene.hpp>
#include <Accelerator.hpp>
#include <Material.hpp>
#include <Renderer.hpp>
#include <Mesh.hpp>
#include <Vec.hpp>
#include <FixedSizeQueue.hpp>

#include <fstream>
#include <iostream>
#include <sstream>
#include <chrono>
#include <algorithm>
#include <stack>
#include <utility>
using namespace std::chrono;

namespace Incarnate
{

PBRTLoader::PBRTLoader()
{
	commands.addCommand<PBRTFilmCommand>("Film");
	commands.addCommand<PBRTLookAtCommand>("LookAt");
	commands.addCommand<PBRTCameraCommand>("Camera");
	commands.addCommand<PBRTSamplerCommand>("Sampler");
	commands.addCommand<PBRTRendererCommand>("Renderer");
	commands.addCommand<PBRTSurfaceIntegratorCommand>("SurfaceIntegrator");
	commands.addCommand<PBRTAcceleratorCommand>("Accelerator");
	commands.addCommand<PBRTWorldBeginCommand>("WorldBegin");
	commands.addCommand<PBRTIncludeCommand>("Include");

	sceneCommands.addCommand<PBRTWorldEndCommand>("WorldEnd");
	sceneCommands.addCommand<PBRTAttributeBeginCommand>("AttributeBegin");
	sceneCommands.addSubCommandList(&attribCommands);


	attribCommands.addCommand<PBRTShapeCommand>("Shape");
	attribCommands.addCommand<PBRTMakeNamedMaterialCommand>("MakeNamedMaterial");
	attribCommands.addCommand<PBRTNamedMaterialCommand>("NamedMaterial");
	attribCommands.addCommand<PBRTIncludeCommand>("Include");
	attribCommands.addCommand<PBRTTextureCommand>("Texture");
	attribCommands.addCommand<PBRTAttributeEndCommand>("AttributeEnd");
	attribCommands.addCommand<PBRTMaterialCommand>("Material");
	attribCommands.addCommand<PBRTAreaLightSourceCommand>("AreaLightSource");
	attribCommands.addCommand<PBRTScaleCommand>("Scale");
	attribCommands.addCommand<PBRTTranslateCommand>("Translate");
	attribCommands.addCommand<PBRTRotateCommand>("Rotate");
	attribCommands.addCommand<PBRTIdentityCommand>("Identity");
	attribCommands.addCommand<PBRTTransformCommand>("Transform");
	attribCommands.addCommand<PBRTLightSourceCommand>("LightSource");

	commandList[0] = &commands;
	commandList[1] = &sceneCommands;
	commandList[2] = &attribCommands;
}

PBRTLoader::PBRTLoader(const std::string& path) : PBRTLoader()
{
	loadScene(path);
}

int PBRTLoader::loadScene(std::string filename)
{
	PBRTDocument* doc = new PBRTDocument;

	if(doc->load(filename))
	{
		PBRTDocument*& currentDocument = loaderInfo.currentDocument;
		PBRTSceneInfo& sceneInfo = loaderInfo.sceneInfo;
		std::stack<PBRTDocument*>& documentStack = loaderInfo.documentStack;

		documentStack.push(doc);
		currentDocument = doc;
		loaderInfo.rootDocument = doc;

		std::cout << "\nReading file: " << filename << "\n";

		ArgumentVector argv;
		while(true)
		{
			if(currentDocument->nextCommand(argv) < 0)
			{
				delete currentDocument;
				documentStack.pop();
				if(documentStack.size() == 0)
					break;
				currentDocument = documentStack.top();
				continue;
			}

			int returnVal = 1;
			returnVal = execute(argv);

			//0 means ok. As how the C return value convention goes
			if(returnVal != 0)
			{
				std::cout << "An Error had occured in file : " << currentDocument->path
					<< ", Command starts at line "
					<< currentDocument->getLineByIndex(currentDocument->oldIndex)
					<< "." << std::endl;
				std::cout << "Abord." << std::endl;
				while(!documentStack.empty())
				{
					delete documentStack.top();
					documentStack.pop();
				}
				clear();
				return 0;
			}
		}

		if(sceneInfo.camera == NULL || sceneInfo.renderer == NULL || sceneInfo.scene == NULL)
		{
			std::cout << "Error: Camera, Renderer or WorldBegin not setted." << '\n';
			clear();
			return 0;
		}

		if(loaderInfo.parserState != PBRTParserState::RootState)
			std::cout << "Warrning : Prasing does not end at RootState." << std::endl;

		if(sceneInfo.camera == nullptr || sceneInfo.renderer == nullptr
			|| sceneInfo.scene == nullptr || sceneInfo.accelerator == nullptr)
		{
			std::cout << "Error: Something isn't setted up correctly in the scene" << '\n';
			return 0;
		}

		//Setup the rest of stuff
		float3 diffBwt = loaderInfo.lookAtInfo.target-loaderInfo.lookAtInfo.pos;
		float3 cameraUp = normalize(cross(cross(diffBwt,loaderInfo.lookAtInfo.up),diffBwt));
		sceneInfo.camera->lookAt(float4(loaderInfo.lookAtInfo.pos,1),
			float4(loaderInfo.lookAtInfo.target,1),
			float4(cameraUp,0));
		sceneInfo.camera->setAspectRatio((float)loaderInfo.filmInfo.width/loaderInfo.filmInfo.height);
		sceneInfo.camera->update();

		sceneInfo.renderer->setAccelerator(sceneInfo.accelerator);
		sceneInfo.renderer->setSampleNum(loaderInfo.samplerInfo.sampleNum);
		sceneInfo.scene->setCamera(sceneInfo.camera);
		sceneInfo.renderer->createBuffer(loaderInfo.filmInfo.width,loaderInfo.filmInfo.height);

		sceneInfo.rootNode->addToScene(sceneInfo.scene);
		sceneInfo.scene->build();
		//sceneInfo.renderer->setScene(sceneInfo.scene);
		sceneInfo.accelerator->setScene(sceneInfo.scene);
	}
	else
	{
		delete doc;
		std::cout << "Failed to open file: " << filename << "\n";
		return 0;
	}

	isGood = true;
	return 1;
}

void PBRTLoader::clear()
{
	PBRTSceneInfo& sceneInfo = loaderInfo.sceneInfo;

	delete sceneInfo.camera;
	delete sceneInfo.renderer;
	delete sceneInfo.scene;
	delete sceneInfo.accelerator;

	sceneInfo.camera = NULL;
	sceneInfo.renderer = NULL;
	sceneInfo.scene = NULL;
	sceneInfo.accelerator = NULL;
	sceneInfo.rootNode = NULL;//rootNode will be deleted by the loop next to this

	for(SceneNode* node : sceneInfo.sceneNodes)
		delete node;
	for(SceneItem* item : sceneInfo.sceneItem)
		delete item;

	sceneInfo.sceneNodes.clear();
	sceneInfo.sceneItem.clear();

	sceneInfo.materialManager.destroyAll();
	sceneInfo.textureManager.destroyAll();
	sceneInfo.imageManager.destroyAll();
}

PBRTLoader::~PBRTLoader()
{
	clear();
}

bool PBRTLoader::good()
{
	return isGood;
}

int PBRTLoader::execute(const ArgumentVector& argv)
{
	return commandList[(int)loaderInfo.parserState]->execute(&loaderInfo, argv);
}

Scene* PBRTLoader::getScene()
{
	return loaderInfo.sceneInfo.scene;
}

Renderer* PBRTLoader::getRenderer()
{
	return loaderInfo.sceneInfo.renderer;
}

Camera* PBRTLoader::getCamera()
{
	return loaderInfo.sceneInfo.camera;
}

Accelerator* PBRTLoader::getAccelerator()
{
	return loaderInfo.sceneInfo.accelerator;
}

int PBRTCommandList::execute(PBRTLoaderInfo* loaderInfo, const ArgumentVector& argv)
{
	PBRTCommand* command = getCommand(argv[0].arg);
	if(command != NULL)
		return command->execute(loaderInfo, argv);

	for(int i=0;i<subCommandList.size(); i++)
	{
		command = subCommandList[i]->getCommand(argv[0].arg);
		if(command != NULL)
			return command->execute(loaderInfo, argv);
	}

	std::cout << "Error : Command *" << argv[0] << "* not found or not suppored in this block." << std::endl;
	return 1;
}

void PBRTCommandList::addSubCommandList(PBRTCommandList* commandList)
{
	subCommandList.push_back(commandList);
}

PBRTCommand* PBRTCommandList::getCommand(const std::string& name)
{
	return commands.getResource(name);
}

}
