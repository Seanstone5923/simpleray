#include <PBRTMaterialManager.hpp>
#include <Material.hpp>

using namespace Incarnate;

void MaterialManager::setCurrentMaterial(Material* material)
{
	currentMaterial = material;
}

Material* MaterialManager::getCurrentMaterial()
{
	return currentMaterial;
}

Material* MaterialManager::getMaterial(const std::string& name)
{
	return getResource(name);
}

void MaterialManager::destroyAll()
{
	ResourceManager<Material>::destroyAll();
}
