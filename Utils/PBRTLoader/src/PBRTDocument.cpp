#include <PBRTDocument.hpp>
#include <PBRTArgument.hpp>
#include <PBRTStringProcessing.hpp>

#include <algorithm>
#include <fstream>

using namespace Incarnate;

bool PBRTDocument::load(std::string filename)
{
	std::ifstream pbrtfile;
	pbrtfile.open(filename);
	if(pbrtfile.is_open())
	{
		std::istream::sentry se(pbrtfile, true);
		std::streambuf* sb = pbrtfile.rdbuf();
		convertToLF(sb, data);

		data = removeComments(data);
		path = filename;
		return true;
	}

	return false;
}

int PBRTDocument::nextCommand(ArgumentVector& argv)
{
	oldIndex = index;
	index = argv.parseFirstCommand(data,index);
	return index;
}

std::string PBRTDocument::directory()
{
	//Just incase we got a Windows-style path
	std::string unixStylePath = path;
	std::replace(unixStylePath.begin(), unixStylePath.end(), '\\', '/');
	std::string directory;
	const size_t index = unixStylePath.rfind('/');
	if (std::string::npos != index)
	 	directory = unixStylePath.substr(0, index);

	if(directory.empty())
		directory += ".";
	directory += "/";
	return directory;
}

int PBRTDocument::getLineByIndex(int ind)
{
	int lineNum = 1;
	for(int i=0;i<ind;i++)
	{
		if(data[i] == '\n')
			lineNum++;
	}

	return lineNum;
}
