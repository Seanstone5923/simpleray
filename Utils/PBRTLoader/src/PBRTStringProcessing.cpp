#include "PBRTStringProcessing.hpp"

#include <iostream>

namespace Incarnate
{

static inline bool isAlphabet(char ch)
{
	return (('A' >= ch && ch <= 'Z') || ('a' >= ch && ch <= 'z'));
}

std::string removeComments(const std::string& data)
{
	bool isInString = false;
	bool inComment = false;
	std::string result;
	int length = data.length();
	for(int i=0;i<length;i++)
	{
		char ch = data[i];

		if(ch == '\\')//Jump trough the escape charector
		{
			result += ch;
			if(i+1 < length)
				result += data[i+1];
			continue;
		}

		if(ch == '\"')
			isInString = !isInString;
		if(isInString == false)
		{
			if(ch == '#')
				inComment = true;
			else if(ch == '\n')
				inComment = false;
		}

		if(inComment == false)
			result += ch;
	}
	return result;
}

void convertToLF(std::streambuf* sb, std::string& out)
{
	for(bool quit=false;quit==false;)
	{
		int ch = sb->sbumpc();
		switch(ch)
		{
		case '\r':
			if(sb->sgetc() == '\n')
				sb->sbumpc();
			out += "\n";
			break;
		case EOF:
			quit = true;
			break;

		default:
			out += (char)ch;
			break;
		}
	}
}

std::string combinePath(const std::string& directory, const std::string& appendPath)
{
	if(appendPath[0] == '/' || appendPath[0] == '~' || (isAlphabet(appendPath[0]) && appendPath[1] == ':'))
		return appendPath;
	return directory + appendPath;
}

std::string extendsionFromPath(const std::string& path)
{
	std::size_t found;
	found = path.find(".");
	if(found == std::string::npos)//No .
		return "";

	std::string fileExt = path.substr(path.find_last_of(".") + 1);
	found = fileExt.find("/");
	if(found != std::string::npos)//No . in file name. instead we find it in directory name
		return "";
	return fileExt;

}

}
