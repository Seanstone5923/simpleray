#include "PBRTArgument.hpp"

#include <iostream>

namespace Incarnate
{

Argument::Argument()
{
}

Argument::Argument(std::string str, int ind)
{
	arg = str;
	loocaton = ind;
}

bool Argument::operator==(const Argument& argument) const
{
	//Yes, we do ignore the index here.
	return arg==argument.arg;
}

bool Argument::operator!=(const Argument& argument) const
{
	return !(arg==argument.arg);
}

bool Argument::operator<(const Argument& argument) const
{
	return (arg.compare(argument.arg) < 0);
}

bool Argument::isList() const
{
	int length = arg.length();
	if(arg[0] == '[' && arg[length-1] == ']')
		return true;
	return false;
}

std::string Argument::getListContent() const
{
	std::string result;
	int length = arg.length();
	for (int i=1; i<length-1; i++)
		result += arg[i];

	return result;
}


bool Argument::isString() const
{
	int length = arg.length();
	if(arg[0] == '\"' && arg[length-1] == '\"')
		return true;
	return false;
}

std::string Argument::getStringContent() const
{
	std::string result;
	int length = arg.length();
	for (int i=1; i<length-1; i++)
		result += arg[i];

	return result;
}

const std::string& Argument::getRaw() const
{
	return arg;
}

int Argument::parseFirstArgument(std::string& result, int start)
{
	//Find the first charector which is not *empty*
	const std::string& data = arg;
	int index = start;
	while(data[index] == '\n' || data[index] == '\t' || data[index] == ' ')
		index++;

	int length = data.length();
	int bracketsNum = 0;
	bool isInString = false;

	result.clear();

	for(;index<length;index++)
	{
		char ch = data[index];

		if(ch == '\\')//Jump trough the escape charector
		{
			result += ch;
			if(index+1 < length)
				result += data[index+1];
			continue;
		}

		if(ch == '\"')
			isInString = !isInString;

		if(isInString == false)
		{
			if(ch == '[')
			{
				bracketsNum++;
			}
			else if(ch == ']')
			{
				if(bracketsNum == 0)
				{
					std::cout << "Error : A back brackets is unpaired with a front one." << std::endl;
					return -index;
				}

				bracketsNum--;
			}
		}

		if((isInString == false && bracketsNum == 0)&&
			(ch == ' ' || ch == '\t' || ch == '\n'))
			break;

		result += ch;
	}


	if(isInString == true || bracketsNum != 0)
	{
		std::cout << "Error : EOF reached while prasing fragments. Maybe a \" or ] is missing somewhere" << std::endl;
		return -index;
	}

	return index;
}

ArgumentVector::ArgumentVector()
{
}

ArgumentVector::ArgumentVector(const std::string& data)
{
	parse(data);
}

ArgumentVector::ArgumentVector(const char* data) : ArgumentVector(std::string(data))
{
}

int ArgumentVector::parse(const std::string& data, int start)
{
	clear();
	Argument arg;
	std::string str;
	int index = start;
	int length = data.length();
	int lastStartIndex = -1;
	Argument dataArg(data);
	while(index < length)
	{
		index = dataArg.parseFirstArgument(str,index);
		if(index < 0)//Failed
			return -index;

		arg.arg = str;
		arg.loocaton = index - str.length();
		lastStartIndex = index - str.length();

		if(str != "")
			push_back(arg);
	}
	return -index;
}

//return a positive value if prasing success.
//return -1 if reaches end of string
//else means prasing failed
int ArgumentVector::parseFirstCommand(const std::string& data, int start)
{
	clear();
	int index = start;
	int length = data.length();
	bool firstArgument = true;
	int lastStartIndex = -1;
	Argument arg;
	std::string str;
	Argument dataArg(data);
	while(index < length)
	{
		index = dataArg.parseFirstArgument(str,index);
		if(index < 0)//Failed
			return index;
		arg.arg = str;
		arg.loocaton = index - str.length();
		lastStartIndex = index - str.length();

		if((str[0] != '\"' && str[0] != '[' && isdigit(str[0]) == 0 && str[0] != '-' && str[0] != '+' && str[0] != '.')
			&& firstArgument == false)
			break;

		firstArgument = false;
		push_back(arg);
	}

	return lastStartIndex;
}

bool ArgumentVector::operator==(ArgumentVector const& statement) const
{
	if(size() != statement.size())
		return false;
	int dataSize = size();
	for(int i=0;i<dataSize;i++)
	{
		if(at(i)!=statement[i])
			return false;
	}
	return true;
}

bool ArgumentVector::operator!=(ArgumentVector const& statement) const
{
	return !(*this == statement);
}

std::ostream& operator<< (std::ostream& out, const Argument& arg)
{
	return out << arg.arg;
}

std::ostream& operator<< (std::ostream& out, const ArgumentVector& statement)
{
	int size = statement.size();
	for(int i=0;i<size;i++)
		out << statement[i] << ((i+1 < size) ? " " : "");
	return out;

}
}
