#include <PBRTCommand.hpp>
#include <PBRTSceneInfo.hpp>
#include <PBRTDocument.hpp>
#include <PBRTStringProcessing.hpp>

#include <Camera.hpp>
#include <Renderer.hpp>
#include <Accelerator.hpp>
#include <Scene.hpp>
#include <Material.hpp>
#include <Mesh.hpp>
#include <Texture.hpp>
#include <LightSource.hpp>

using namespace Incarnate;

PBRTCommand::PBRTCommand()
{
}

PBRTCommand::~PBRTCommand()
{
}

int PBRTCommand::execute(PBRTLoaderInfo* loaderInfo, const ArgumentVector& argv)
{
}

PBRTFilmCommand::PBRTFilmCommand()
{
	parser.addFlag("image", &flag);
	parser.addArgument("integer xresolution", &xresolution, false, "[640]",1);
	parser.addArgument("integer yresolution", &yresolution, false, "[480]",1);
	parser.addArgument("string filename", &filename, false, R"(["simpleray.exr"])",1);
}

int PBRTFilmCommand::execute(PBRTLoaderInfo* loaderInfo, const ArgumentVector& argv)
{
	if(parser.parse(argv) == false)
		return 1;
	loaderInfo->filmInfo.width = xresolution[0];
	loaderInfo->filmInfo.height = yresolution[0];
	loaderInfo->filmInfo.filename = filename[0];
	return 0;
}

PBRTCameraCommand::PBRTCameraCommand()
{
	addSubCommand<PBRTPerspectiveCameraCommand>("perspective");
}

PBRTPerspectiveCameraCommand::PBRTPerspectiveCameraCommand()
{
	parser.addArgument("float fov", &fov, false, "[90]",1);
	parser.addArgument("float hfov", &hfov, false, "",1);
}

int PBRTPerspectiveCameraCommand::execute(PBRTLoaderInfo* loaderInfo, const ArgumentVector& argv)
{
	if(parser.parse(argv) == false)
		return 1;
	float fovVal = (hfov.size()>0 ? hfov[0] : fov[0]);

	PerspectiveCamera* camera = new PerspectiveCamera;
	loaderInfo->sceneInfo.camera = camera;
	camera->setFOV(float2(glm::radians(fovVal)));
	return 0;
}

PBRTSamplerCommand::PBRTSamplerCommand()
{
	parser.addFlag("random", &flag);
	parser.addArgument("integer pixelsamples",&sampleNum,false,"[2048]",1);
}

int PBRTSamplerCommand::execute(PBRTLoaderInfo* loaderInfo, const ArgumentVector& argv)
{
	if(parser.parse(argv) == false)
		return 1;
	loaderInfo->samplerInfo.sampleNum = sampleNum[0];
	return 0;
}

int PBRTLookAtCommand::execute(PBRTLoaderInfo* loaderInfo, const ArgumentVector& argv)
{
	if(argv.size() != 10)
	{
		std::cout << "Error : Must have 9 parameter for the LookUp command" << std::endl;
		return 1;
	}
	std::vector<float> params;
	ArgumentVector  fakeArg(makeList(argv,1,9));
	if(parseValue(fakeArg, &params, 0, 9) < 0)
		return 1;
	float3 pos(params[0],params[1],params[2]);
	float3 target(params[3],params[4],params[5]);
	float3 up(params[6],params[7],params[8]);
	loaderInfo->lookAtInfo.pos = pos;
	loaderInfo->lookAtInfo.target = target;
	loaderInfo->lookAtInfo.up = up;
	return 0;
}

PBRTRendererCommand::PBRTRendererCommand()
{
	parser.addFlag("sampler", &flag);
}

int PBRTRendererCommand::execute(PBRTLoaderInfo* loaderInfo, const ArgumentVector& argv)
{
	if(parser.parse(argv) == false)
		return 1;
	return 0;
}

PBRTSurfaceIntegratorCommand::PBRTSurfaceIntegratorCommand()
{
	addSubCommand<PBRTPathRendererCommand>("path");
	addSubCommand<PBRTDirectLightingRendererCommand>("directlighting");
	addSubCommand<PBRTNormalRendererCommand>("normal");
}

PBRTPathRendererCommand::PBRTPathRendererCommand()
{
	parser.addArgument("integer maxdepth",&bounceDepth,false,"[5]",1);
}

int PBRTPathRendererCommand::execute(PBRTLoaderInfo* loaderInfo, const ArgumentVector& argv)
{
	if(parser.parse(argv) == false)
		return 1;
	PathRenderer* renderer = new PathRenderer;
	renderer->setBounceDepth(bounceDepth[0]);
	loaderInfo->sceneInfo.renderer = renderer;
	return 0;
}

PBRTDirectLightingRendererCommand::PBRTDirectLightingRendererCommand()
{
}

int PBRTDirectLightingRendererCommand::execute(PBRTLoaderInfo* loaderInfo, const ArgumentVector& argv)
{
	if(argv.size() != 1)
	{
		std::cout << "Error : DirectLighting should have no parameter" << std::endl;
		return 1;
	}

	DirectLightingRenderer* renderer = new DirectLightingRenderer;
	loaderInfo->sceneInfo.renderer = renderer;
	return 0;
}

PBRTNormalRendererCommand::PBRTNormalRendererCommand()
{
}

int PBRTNormalRendererCommand::execute(PBRTLoaderInfo* loaderInfo, const ArgumentVector& argv)
{
	if(argv.size() != 1)
	{
		std::cout << "Error : NormalRenderer should have no parameter" << std::endl;
		return 1;
	}

	NormalRenderer* renderer = new NormalRenderer;
	loaderInfo->sceneInfo.renderer = renderer;
	return 0;
}


PBRTAcceleratorCommand::PBRTAcceleratorCommand()
{
	addSubCommand<PBRTBVHAcceleratorCommand>("bvh");
	addSubCommand<PBRTBruteforceAcceleratorCommand>("none");
	#ifdef INC_EMBREE_ACCELERATOR
	addSubCommand<PBRTEmbreeAcceleratorCommand>("embree");
	#else
	addSubCommand<PBRTBVHAcceleratorCommand>("embree");
	#endif
}

PBRTBVHAcceleratorCommand::PBRTBVHAcceleratorCommand()
{
	parser.addArgument("string splitmethod",&acceleratorType,false,R"("sah")");
}

int PBRTBVHAcceleratorCommand::execute(PBRTLoaderInfo* loaderInfo, const ArgumentVector& argv)
{
	if(parser.parse(argv) == false)
		return 1;

	BVHAccelerator* accelerator = new BVHAccelerator;
	if(acceleratorType == "sah")
		accelerator->setSplitMethod(SplitMethod::SAH);
	else if(acceleratorType == "equal")
		accelerator->setSplitMethod(SplitMethod::EqualCount);
	else
	{
		std::cout << "Warrning : BVH spliting method *" << acceleratorType << "* not supported. default to SAH" << std::endl;
		accelerator->setSplitMethod(SplitMethod::SAH);
	}

	loaderInfo->sceneInfo.accelerator = accelerator;
	return 0;
}

#ifdef INC_EMBREE_ACCELERATOR

PBRTEmbreeAcceleratorCommand::PBRTEmbreeAcceleratorCommand()
{
}

int PBRTEmbreeAcceleratorCommand::execute(PBRTLoaderInfo* loaderInfo, const ArgumentVector& argv)
{
	if(parser.parse(argv) == false)
		return 1;

	EmbreeAccelerator* accelerator = new EmbreeAccelerator;
	loaderInfo->sceneInfo.accelerator = accelerator;
	return 0;
}
#endif

PBRTBruteforceAcceleratorCommand::PBRTBruteforceAcceleratorCommand()
{
}

int PBRTBruteforceAcceleratorCommand::execute(PBRTLoaderInfo* loaderInfo, const ArgumentVector& argv)
{
	if(parser.parse(argv) == false)
		return 1;

	BruteforceAccelerator* accelerator = new BruteforceAccelerator;
	loaderInfo->sceneInfo.accelerator = accelerator;
	return 0;
}



int PBRTWorldBeginCommand::execute(PBRTLoaderInfo* loaderInfo, const ArgumentVector& argv)
{
	if(argv.size() != 1)
	{
		std::cout << "Error : WorldBegin command should have no parameter" << std::endl;
		return 1;
	}
	if(loaderInfo->sceneInfo.scene != NULL)
	{
		std::cout << "Error : WorldBegin command had been executed previously." << std::endl;
		return 1;
	}

	Scene* scene = new Scene;
	SceneNode* rootNode = new SceneNode;
	loaderInfo->sceneInfo.scene = scene;
	loaderInfo->sceneInfo.rootNode = rootNode;
	loaderInfo->sceneInfo.sceneNodes.push_back(rootNode);
	loaderInfo->parserState = PBRTParserState::WorldState;

	loaderInfo->worldMaterial = loaderInfo->sceneInfo.materialManager.newMaterial<MatteMaterial>("PBRTLoaderDefaultMaterial");
	loaderInfo->worldNode = rootNode;

	return 0;
}

int PBRTWorldEndCommand::execute(PBRTLoaderInfo* loaderInfo, const ArgumentVector& argv)
{
	if(argv.size() != 1)
	{
		std::cout << "Error : WorldBegin command should have no parameter" << std::endl;
		return 1;
	}
	loaderInfo->parserState = PBRTParserState::RootState;
	return 0;
}

int PBRTAttributeBeginCommand::execute(PBRTLoaderInfo* loaderInfo, const ArgumentVector& argv)
{
	if(argv.size() != 1)
	{
		std::cout << "Error : AttributeBegin command should have no parameter" << std::endl;
		return 1;
	}
	loaderInfo->parserState = PBRTParserState::AttributeState;
	return 0;
}

int PBRTAttributeEndCommand::execute(PBRTLoaderInfo* loaderInfo, const ArgumentVector& argv)
{
	if(argv.size() != 1)
	{
		std::cout << "Error : AttributeEnd command should have no parameter" << std::endl;
		return 1;
	}
	loaderInfo->parserState = PBRTParserState::WorldState;

	loaderInfo->sceneInfo.materialManager.setCurrentMaterial(loaderInfo->worldMaterial);
	SceneNode* node = new SceneNode;
	loaderInfo->worldNode->addChild(node);
	loaderInfo->sceneInfo.sceneNodes.push_back(node);

	return 0;
}

PBRTMaterialCommand::PBRTMaterialCommand()
{
	addSubCommand<PBRTMatteMaterialCommand>("matte");
	addSubCommand<PBRTMirrorMaterialCommand>("mirror");
	addSubCommand<PBRTPhongMaterialCommand>("phong");
	addSubCommand<PBRTMixedMaterialCommand>("mix");
}

PBRTMatteMaterialCommand::PBRTMatteMaterialCommand()
{
	parser.addArgument("rgb Kd", &diffuseColor, false, "[1 1 1]",3);
	parser.addArgument("string name",&name, false, "\"\"");
	parser.addArgument("texture Kd",&textureName, false,"");
}

int PBRTMatteMaterialCommand::execute(PBRTLoaderInfo* loaderInfo, const ArgumentVector& argv)
{
	if(parser.parse(argv) == false)
		return 1;
	Texture* texture = NULL;
	if(textureName != "")
	{
		texture = loaderInfo->sceneInfo.textureManager.getResource(textureName);
		if(texture == NULL)
		{
			std::cout << "Error : Texture *" << textureName << "* not found"  << std::endl;
			return 1;
		}
	}

	MatteMaterial* material = loaderInfo->sceneInfo.materialManager.newMaterial<MatteMaterial>(name,true);
	material->setReflect(float4(diffuseColor[0],diffuseColor[1],diffuseColor[2],1));
	material->setTexture(texture,0);

	if(loaderInfo->parserState == PBRTParserState::WorldState)
		loaderInfo->worldMaterial = material;
	return 0;
}

PBRTMirrorMaterialCommand::PBRTMirrorMaterialCommand()
{
	parser.addArgument("rgb Kr", &reflectColor, false, "[1 1 1]",3);
	parser.addArgument("string name",&name, false, "\"\"");
}

int PBRTMirrorMaterialCommand::execute(PBRTLoaderInfo* loaderInfo, const ArgumentVector& argv)
{
	if(parser.parse(argv) == false)
		return 1;
	MirrorMaterial* material = loaderInfo->sceneInfo.materialManager.newMaterial<MirrorMaterial>(name,true);
	material->setReflect(float4(reflectColor[0],reflectColor[1],reflectColor[2],1));

	if(loaderInfo->parserState == PBRTParserState::WorldState)
		loaderInfo->worldMaterial = material;
	return 0;
}

PBRTMixedMaterialCommand::PBRTMixedMaterialCommand()
{
	parser.addArgument("float amount",&amount, false, "[0.5]",1);
	parser.addArgument("string namedmaterial1",&material1Name, true);
	parser.addArgument("string namedmaterial2",&material2Name, true);
	parser.addArgument("string name",&name, false, "\"\"");
}

int PBRTMixedMaterialCommand::execute(PBRTLoaderInfo* loaderInfo, const ArgumentVector& argv)
{
	if(parser.parse(argv) == false)
		return 1;

	Material* material1 = loaderInfo->sceneInfo.materialManager.getMaterial(material1Name);
	Material* material2 = loaderInfo->sceneInfo.materialManager.getMaterial(material2Name);

	if(material1 == NULL)
	{
		std::cout << "Error: material *" << material1Name << "* does not exist." << std::endl;
		return 1;
	}

	if(material2 == NULL)
	{
		std::cout << "Error: material *" << material2Name << "* does not exist." << std::endl;
		return 1;
	}

	MixedMaterial* material = loaderInfo->sceneInfo.materialManager.newMaterial<MixedMaterial>(name,true);;
	material->setFirstBSDF(material1);
	material->setSecondBSDF(material2);
	material->setRatio(amount[0]);

	if(loaderInfo->parserState == PBRTParserState::WorldState)
		loaderInfo->worldMaterial = material;
	return 0;
}


PBRTPhongMaterialCommand::PBRTPhongMaterialCommand()
{
	parser.addArgument("rgb Kd",&color,false, "[1 1 1]",3);
	parser.addArgument("float power",&power, false, "[1.0]",1);
	parser.addArgument("string name",&name,false, "\"\"");
}

int PBRTPhongMaterialCommand::execute(PBRTLoaderInfo* loaderInfo, const ArgumentVector& argv)
{
	if(parser.parse(argv) == false)
		return 1;

	PhongMaterial* material = loaderInfo->sceneInfo.materialManager.newMaterial<PhongMaterial>(name,true);
	material->setReflect(float4(color[0],color[1],color[2],1));
	material->setPower(power[0]);

	if(loaderInfo->parserState == PBRTParserState::WorldState)
		loaderInfo->worldMaterial = material;

	return 0;
}

PBRTShapeCommand::PBRTShapeCommand()
{
	addSubCommand<PBRTTriangleMeshCommand>("trianglemesh");
}

PBRTTriangleMeshCommand::PBRTTriangleMeshCommand()
{
	parser.addArgument("point P",&vertices,true);
	parser.addArgument("integer indices",&indices,true);
	parser.addArgument("normal N",&normals,false,"[]");
	parser.addArgument("float uv",&uvs,false,"[]");
}

int PBRTTriangleMeshCommand::execute(PBRTLoaderInfo* loaderInfo, const ArgumentVector& argv)
{
	if(parser.parse(argv) == false)
		return 1;
	bool normalAvliable = false;
	bool uvAvliable = false;

	if(normals.size() > 0)
		normalAvliable = true;

	if(uvs.size() > 0)
		uvAvliable = true;

	if(indices.size() % 3 != 0)
	{
		std::cout << "Error : Size of indices must be a multiple of 3." << std::endl;
		return 1;
	}

	if(normals.size() != vertices.size() && normalAvliable)
	{
		std::cout << "Error : Size of normal does not match the size of vertices" << std::endl;
		return 1;
	}

	if(vertices.size()/3 != uvs.size()/2 && uvAvliable)
	{
		std::cout << "Error : Size of UV does not match the size of vertices" << std::endl;
		return 1;
	}

	Mesh* mesh = new Mesh;

	float4* verts = new float4[vertices.size()/3];
	float4* norms = NULL;
	if(normalAvliable)
		norms = new float4[vertices.size()/3];


	for(int i=0;i<vertices.size()/3;i++)
	{
		verts[i] = float4(vertices[i*3+0],vertices[i*3+1],vertices[i*3+2],1);
		if(normalAvliable)
		{
			norms[i] = float4(normals[i*3+0],normals[i*3+1],normals[i*3+2],0);
			if(dot(norms[i],float4(1)) == 0)//Somehow we have invalid normals
				norms[i] = float4(0,1,0,0);//Set a default value for it
		}
	}

	mesh->setVertices(verts,vertices.size()/3);
	mesh->setIndices(&indices[0],indices.size());
	if(normalAvliable)
		mesh->setNormals(norms,vertices.size()/3);
	if(uvAvliable)
		mesh->setUVs((float2*)&uvs[0],uvs.size()/2);

	mesh->setMaterial(loaderInfo->sceneInfo.materialManager.getCurrentMaterial());

	SceneMeshNode* node = new SceneMeshNode;
	node->setMesh(mesh);
	loaderInfo->sceneInfo.sceneNodes.back()->addChild(node);
	loaderInfo->sceneInfo.sceneItem.push_back(node);

	delete [] verts;
	delete [] norms;
	return 0;
}

PBRTAreaLightSourceCommand::PBRTAreaLightSourceCommand()
{
	parser.addFlag("diffuse",&flag);
	parser.addArgument("rgb L",&radience, false, "[1 1 1]",3);
}

int PBRTAreaLightSourceCommand::execute(PBRTLoaderInfo* loaderInfo, const ArgumentVector& argv)
{
	if(parser.parse(argv) == false)
		return 1;

	MatteMaterial* material = loaderInfo->sceneInfo.materialManager.newMaterial<MatteMaterial>("",true);
	AreaLightSource* emitProp = loaderInfo->sceneInfo.lightSourceManager.newResource<AreaLightSource>();
	emitProp->setRadience(float4(radience[0],radience[1],radience[2],1));
	material->setEmitProperty(emitProp);
	material->setReflect(float4(0,0,0,1));

	if(loaderInfo->parserState == PBRTParserState::WorldState)
		loaderInfo->worldMaterial = material;

	return 0;
}

int PBRTIncludeCommand::execute(PBRTLoaderInfo* loaderInfo, const ArgumentVector& argv)
{
	if(argv.size() != 2)
	{
		std::cout << "Error : Include command should only have 1 parameter" << std::endl;
		return 1;
	}

	if(Argument(argv[1]).isString() == false)
	{
		std::cout << "Error : the parameter for Include command should be a string." << std::endl;
		return 1;
	}

	PBRTDocument* doc = new PBRTDocument;
	std::string filePath = argv[1].getStringContent();
	std::string rootFileDirectory = loaderInfo->rootDocument->directory();
	std::string combinedPath = combinePath(rootFileDirectory,filePath);
	if(doc->load(combinedPath) == false)
	{
		std::cout << "Error : cannot open file *" << combinedPath << "*" << std::endl;
		return 1;
	}

	loaderInfo->currentDocument = doc;
	loaderInfo->documentStack.push(doc);
	return 0;
}

int PBRTScaleCommand::execute(PBRTLoaderInfo* loaderInfo, const ArgumentVector& argv)
{
	if(argv.size() != 4)
	{
		std::cout << "Error : Scale command should have and only have 3 prameters." << std::endl;
		return 0;
	}
	std::vector<float> vec;
	ArgumentVector  fakeArg(makeList(argv,1,3));
	if(parseValue(fakeArg, &vec, 0, 3) < 0)
		return 1;

	SceneNode* node = new SceneNode;
	Transform transform;
	transform.scale(float3(vec[0],vec[1],vec[2]));
	node->setTransform(transform);
	loaderInfo->sceneInfo.sceneNodes.back()->addChild(node);
	loaderInfo->sceneInfo.sceneNodes.push_back(node);

	if(loaderInfo->parserState == PBRTParserState::WorldState)
		loaderInfo->worldNode = node;

	return 0;
}

int PBRTTranslateCommand::execute(PBRTLoaderInfo* loaderInfo, const ArgumentVector& argv)
{
	if(argv.size() != 4)
	{
		std::cout << "Error : Translate command should have and only have 3 prameters." << std::endl;
		return 0;
	}
	std::vector<float> vec;
	ArgumentVector  fakeArg(makeList(argv,1,3));
	if(parseValue(fakeArg, &vec, 0, 3) < 0)
		return 1;

	SceneNode* node = new SceneNode;
	Transform transform;
	transform.translate(float3(vec[0],vec[1],vec[2]));
	node->setTransform(transform);
	loaderInfo->sceneInfo.sceneNodes.back()->addChild(node);
	loaderInfo->sceneInfo.sceneNodes.push_back(node);

	if(loaderInfo->parserState == PBRTParserState::WorldState)
		loaderInfo->worldNode = node;

	return 0;
}

int PBRTRotateCommand::execute(PBRTLoaderInfo* loaderInfo, const ArgumentVector& argv)
{
	if(argv.size() != 5)
	{
		std::cout << "Error : Rotate command should have and only have 3 prameters." << std::endl;
		return 0;
	}
	std::vector<float> vec;
	ArgumentVector  fakeArg(makeList(argv,1,4));
	if(parseValue(fakeArg, &vec, 0, 4) < 0)
		return 1;

	SceneNode* node = new SceneNode;
	Transform transform;
	transform.rotate(float3(vec[1],vec[2],vec[3]),radians(vec[0]));
	node->setTransform(transform);
	loaderInfo->sceneInfo.sceneNodes.back()->addChild(node);
	loaderInfo->sceneInfo.sceneNodes.push_back(node);

	if(loaderInfo->parserState == PBRTParserState::WorldState)
		loaderInfo->worldNode = node;

	return 0;
}

int PBRTIdentityCommand::execute(PBRTLoaderInfo* loaderInfo, const ArgumentVector& argv)
{
	if(argv.size() != 1)
	{
		std::cout << "Error : Rotate command should have and no prameter." << std::endl;
		return 0;
	}

	SceneNode* node = new SceneNode;
	loaderInfo->sceneInfo.rootNode->addChild(node);
	loaderInfo->sceneInfo.sceneNodes.push_back(node);

	if(loaderInfo->parserState == PBRTParserState::WorldState)
		loaderInfo->worldNode = node;

	return 0;
}

int PBRTTransformCommand::execute(PBRTLoaderInfo* loaderInfo, const ArgumentVector& argv)
{
	std::string arg = argv[1].arg;

	if(argv.size() != 2)
	{
		std::cout << "Error : Transform should have 1 parameter, which is the transform matrix data list" << std::endl;
		return 1;
	}

	std::vector<float> vec;
	ArgumentVector  fakeArg(makeList(argv,1,16));
	if(parseValue(fakeArg, &vec, 0, 16) < 0)
		return 1;

	SceneNode* node = new SceneNode;
	Transform transform;
	float4x4 matrix = glm::make_mat4(&vec[0]);
	transform.setTransformMatrix(glm::transpose(matrix));
	node->setTransform(transform);
	loaderInfo->sceneInfo.sceneNodes.back()->addChild(node);
	loaderInfo->sceneInfo.sceneNodes.push_back(node);

	if(loaderInfo->parserState == PBRTParserState::WorldState)
		loaderInfo->worldNode = node;

	return 0;
}

int PBRTMakeNamedMaterialCommand::execute(PBRTLoaderInfo* loaderInfo, const ArgumentVector& argv)
{
	if(argv.size() < 4)
	{
		std::cout << "Error : Too few parameter for MakeNamedMaterial" << std::endl;
		return 1;
	}

	if(argv[1].isString() == false)
	{
		std::cout << "Error : Parameter 1 is the material name. It must be a string" << std::endl;
		return 1;
	}

	if(argv[2].isString() == false || ArgumentVector(argv[2].getStringContent()) != ArgumentVector("string type"))
	{
		std::cout << "Error : Parameter 2 must be *\"string type\"*" << std::endl;
		return 1;
	}

	if(argv[3].isString() == false)
	{
		std::cout << "Errpr : Argument 3 is the material type. It must be a string" << std::endl;
		return 1;
	}

	ArgumentVector constructedArgv;
	constructedArgv.push_back(Argument("Material"));
	constructedArgv.push_back(argv[3]);
	for(int i=4;i<argv.size();i++)
		constructedArgv.push_back(argv[i]);
	constructedArgv.push_back(Argument("\"string name\""));
	constructedArgv.push_back(argv[1]);

	return materialCommand.execute(loaderInfo, constructedArgv);
}

int PBRTNamedMaterialCommand::execute(PBRTLoaderInfo* loaderInfo, const ArgumentVector& argv)
{
	if(argv.size() != 2)
	{
		std::cout << "Error : NamedMaterial needs and only needs one parameter" << std::endl;
		return 1;
	}

	if(argv[1].isString() == false)
	{
		std::cout << "Error : Parameter 1 muste be string" << std::endl;
		return 1;
	}
	std::string name = argv[1].getStringContent();

	Material* material = loaderInfo->sceneInfo.materialManager.getMaterial(name);
	if(material == NULL)
	{
		std::cout << "Error : Material *" << name << "* not found" << std::endl;
		return 1;
	}

	loaderInfo->sceneInfo.materialManager.setCurrentMaterial(material);
	if(loaderInfo->parserState == PBRTParserState::WorldState)
		loaderInfo->worldMaterial = material;
	return 0;
}

PBRTTextureCommand::PBRTTextureCommand()
{
	addSubCommand<PBRTImageMapCommand>("imagemap");
}

int PBRTTextureCommand::execute(PBRTLoaderInfo* loaderInfo, const ArgumentVector& argv)
{
	if(argv.size() < 4)
	{
		std::cout << "Error : Texture command need at least 4 parameter. (not including parameres needs for "
			"the acuatil texture part)" << std::endl;
		return 1;
	}

	if(argv[1].isString() == false)
	{
		std::cout << "Error : parameter 1 must be a string" << std::endl;
		return 1;
	}

	if(argv[2] != Argument("\"float\"") && argv[2] != Argument("\"spectrum\"")
	 	&& argv[2] != Argument("\"color\""))
	{
		std::cout << "Error : Unsupported texture type. Must be \"float\", \"spectrum\", or \"color\"" << std::endl;
		return 1;
	}

	if(argv[3].isString() == false)
	{
		std::cout << "Error : Parameter 3 must be a string" << std::endl;
		return 1;
	}


	ArgumentVector constructedArgv;
	constructedArgv.push_back(Argument("SubTypedTexture"));//Just a dummy value
	constructedArgv.push_back(argv[3]);
	for(int i=4;i<argv.size();i++)
		constructedArgv.push_back(argv[i]);
	constructedArgv.push_back(Argument("\"string name\""));
	constructedArgv.push_back(argv[1]);

	return PBRTSubTypedCommand::execute(loaderInfo,constructedArgv);
}

PBRTImageMapCommand::PBRTImageMapCommand()
{
	parser.addArgument("string name",&name,true);
	parser.addArgument("string filename",&filename,true);
}

int PBRTImageMapCommand::execute(PBRTLoaderInfo* loaderInfo, const ArgumentVector& argv)
{
	if(parser.parse(argv) == false)
		return 1;

	std::string fileExt = extendsionFromPath(filename);
	//convert to lowercase
	std::transform(fileExt.begin(), fileExt.end(), fileExt.begin(), ::tolower);


	Texture* texture = loaderInfo->sceneInfo.textureManager.newResource(name);
	Image* image;
	if(fileExt != "hdr")
		image = loaderInfo->sceneInfo.imageManager.newResource<HDRImage>(name);
	else
		image = loaderInfo->sceneInfo.imageManager.newResource<LDRImage>(name);

	if(texture == NULL || image == NULL)
	{
		std::cout << "Error : Name *" << name << "* had been used for texture or image." << std::endl;
		return 1;
	}

	std::string rootFileDirectory = loaderInfo->rootDocument->directory();
	std::string combinedPath = combinePath(rootFileDirectory,filename);
	image->load(combinedPath);
	if(image->good() == false)
		return 1;
	texture->setImage(image);
	return 0;
}

void PBRTSubTypedCommand::clear()
{
	for(auto& kv : subCommands)
		delete kv.second;
	subCommands.clear();
}

PBRTSubTypedCommand::~PBRTSubTypedCommand()
{
	clear();
}

int PBRTSubTypedCommand::execute(PBRTLoaderInfo* loaderInfo, const ArgumentVector& argv)
{
	int size = argv.size();
	if(size < 2)
	{
		std::cout << "Error : Should have at least 1 parameter indication which sub-type to use." << std::endl;
		return 1;
	}
	if(Argument(argv[1]).isString() == false)
	{
		std::cout << "Error: *" << argv[1] << "* is not a string." << '\n';
		return 1;
	}

	ArgumentVector newArgv;

	std::string subTypeStr = argv[1].getStringContent();
	auto it = subCommands.find(subTypeStr);
	if(it == subCommands.end())
	{
		std::cout << "Error : Type *" << subTypeStr << "* not avliable." << std::endl;
		return 1;
	}

	newArgv.resize(size-1);
	newArgv[0] = subTypeStr;
	for(int i=2;i<size;i++)
		newArgv[i-1] = argv[i];

	return it->second->execute(loaderInfo,newArgv);
}

bool PBRTSubTypedCommand::aliasCommand(const std::string& name, const std::string& target)
{
	auto it = subCommands.find(target);
	if(it == subCommands.end())
	{
		std::cout << "Error : Sub command *" << target << "* not found, "
			"cannot be aliased."<< std::endl;
		return false;
	}

	commandAliases[name] = it->second;
	return true;
}

PBRTLightSourceCommand::PBRTLightSourceCommand()
{
	addSubCommand<PBRTPointghtSourceComman>("point");
	addSubCommand<PBRTDistantLightSourceCommand>("distant");
	addSubCommand<PBRTInfiniteLightSourceCommand>("infinite");
	addSubCommand<PBRTGoniometricLightSourceCommand>("goniometric");
}

PBRTPointghtSourceComman::PBRTPointghtSourceComman()
{
	parser.addArgument("rgb I",&intensity, false, "[1 1 1]",3);
	parser.addArgument("point from",&from, false, "[0 0 0]",3);
}

int PBRTPointghtSourceComman::execute(PBRTLoaderInfo* loaderInfo, const ArgumentVector& argv)
{
	if(parser.parse(argv) == false)
		return 1;
	PointLightSource* light = loaderInfo->sceneInfo.lightSourceManager.newResource<PointLightSource>();
	light->setIntensity(float3(intensity[0],intensity[1],intensity[2]));
	light->setPosition(float3(from[0],from[1],from[2]));
	loaderInfo->sceneInfo.scene->lights.push_back(light);

	return 0;
}

PBRTDistantLightSourceCommand::PBRTDistantLightSourceCommand()
{
	parser.addArgument("rgb L",&intensity, false, "[1 1 1]",3);
	parser.addArgument("point from",&from, false, "[0 0 0]",3);
	parser.addArgument("point to",&to, false, "[0 0 1]",3);
}

int PBRTDistantLightSourceCommand::execute(PBRTLoaderInfo* loaderInfo, const ArgumentVector& argv)
{
	if(parser.parse(argv) == false)
		return 1;
	float3 intensityVec = float3(intensity[0],intensity[1],intensity[2]);
	float3 fromVec  = float3(from[0],from[1],from[2]);
	float3 toVec = float3(to[0],to[1],to[2]);

	DirectionalLight* light = loaderInfo->sceneInfo.lightSourceManager.newResource<DirectionalLight>();
	light->setRadience(intensityVec);
	light->setIncomeDirection(toVec - fromVec);
	loaderInfo->sceneInfo.scene->lights.push_back(light);

	return 0;
}

PBRTInfiniteLightSourceCommand::PBRTInfiniteLightSourceCommand()
{
	parser.addArgument("string mapname",&mapName, true);
	parser.addArgument("rgb L",&scale, false, "[1 1 1]",3);
}

int PBRTInfiniteLightSourceCommand::execute(PBRTLoaderInfo* loaderInfo, const ArgumentVector& argv)
{
	if(parser.parse(argv) == false)
		return 1;
	Texture* texture = loaderInfo->sceneInfo.textureManager.getResource(mapName);
	if(texture == NULL)
	{
		std::cout << "Error: Texture *" << mapName << "* does not exist." << std::endl;
		return 1;
	}
	//Todo: Make EnvironmentLight a LightSource and add it to lightSourceManager.
	//TODO: Handle memory leak here
	EnvironmentLight* light = new EnvironmentLight;
	SphereMap* sphereMap = loaderInfo->sceneInfo.environmentMapManager.newResource<SphereMap>(mapName);
	sphereMap->setTexture(texture);
	light->setEnvironmentMap(sphereMap);
	light->setScale(float4(scale[0],scale[1],scale[2],1));
	loaderInfo->sceneInfo.scene->setEnvirotmentLight(light);
	return 0;
}

PBRTGoniometricLightSourceCommand::PBRTGoniometricLightSourceCommand()
{
	parser.addArgument("string mapname",&mapName, true);
}

int PBRTGoniometricLightSourceCommand::execute(PBRTLoaderInfo* loaderInfo, const ArgumentVector& argv)
{
	if(parser.parse(argv) == false)
		return 1;
	Texture* texture = loaderInfo->sceneInfo.textureManager.getResource(mapName);
	if(texture == NULL)
	{
		std::cout << "Error: Texture *" << mapName << "* does not exist." << std::endl;
		return 1;
	}
	GoniometricLight* light = loaderInfo->sceneInfo.lightSourceManager.newResource<GoniometricLight>(mapName);
	SphereMap* sphereMap = loaderInfo->sceneInfo.environmentMapManager.newResource<SphereMap>(mapName);
	sphereMap->setTexture(texture);
	light->setEnvironmentMap(sphereMap);
	loaderInfo->sceneInfo.scene->lights.push_back(light);
	return 0;
}
