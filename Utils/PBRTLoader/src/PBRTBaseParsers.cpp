#include "PBRTBaseParsers.hpp"
#include "PBRTStringProcessing.hpp"
#include <PBRTArgument.hpp>

#include <iostream>
#include <stdio.h>
#include <stdlib.h>

namespace Incarnate
{

bool PBRTCommandParser::parse(const ArgumentVector& argv)
{
	std::map<ArgumentVector, bool> parsedList;//second = setted
	for(const auto& prop : nameParserPairs)
		parsedList[prop.first] = false;//If required -> set as false

	int argc = argv.size();
	for(int i=1;i<argc;)
	{
		ArgumentVector argName = ArgumentVector(argv[i].getStringContent());
		PBRTParserProp* prop = getParserProp(argName);
		if(prop == NULL)
		{
			std::cout << "Error : Option *" << argName << "* not supported." << '\n';
			return false;
		}
		parsedList[argName] = true;
		PBRTArgumentParser* parser = prop->parser;
		int ret = parser->parse(argv,i);
		if(ret <= 0)
			return false;

		i += ret;
	}

	//Check if all the required arguments are setted
	bool missing = false;
	for(const auto& element : parsedList)
	{
		const ArgumentVector argName = element.first;
		PBRTParserProp* prop = getParserProp(argName);
		if(element.second == true)//Parsed. Good!
			continue;

		if(prop->required == true)
		{
			std::cout << "Error: Argument *" << element.first << "* is required. But not setted."
				<< '\n';
			missing = true;
		}

		//Else we use the defaultValue if provided
		if(prop->defaultValue != "")
		{
			ArgumentVector fakeArg("Foo");
			fakeArg.push_back(prop->defaultValue);
			int ret = prop->parser->parse(fakeArg,0);
			if(ret <= 0)
			{
				std::cout << "Error: parsing default value *" <<  prop->defaultValue
					<< "*. Who wrote the code!?"<< '\n';
				return false;
			}
		}

	}
	if(missing)
		return false;

	return true;
}

bool PBRTCommandParser::success() const
{
	return good;
}

bool PBRTCommandParser::addParser(const ArgumentVector& name, PBRTArgumentParser* parser, bool required,
	const std::string& defaultVal)
{
	if(getParser(name) != NULL)
	{
		std::cout << "Error: Parameter *" << name << "* is already occupied." << '\n';
		return false;
	}

	nameParserPairs[name] = {parser,defaultVal,required};
	return true;
}


bool PBRTCommandParser::addFlag(const ArgumentVector& name, bool* ptr)
{
	PBRTFlagParser* parser = new PBRTFlagParser;
	parser->setDestination(ptr);
	addParser(name, parser,false,R"("false")");
}

PBRTCommandParser::~PBRTCommandParser()
{
	for(auto& it : nameParserPairs)
		delete it.second.parser;
}

PBRTArgumentParser* PBRTCommandParser::getParser(const ArgumentVector& name)
{
	auto ptr = getParserProp(name);
	return (ptr==NULL?NULL:ptr->parser);
}

PBRTParserProp* PBRTCommandParser::getParserProp(const ArgumentVector& name)
{
	auto it = nameParserPairs.find(name);
	return (it==nameParserPairs.end()?NULL:&it->second);
}

template<>
bool PBRTCommandParser::addArgument(const ArgumentVector& name, std::vector<int>* ptr ,bool required
	, const std::string& defaultVal, int targetSize)
{
	PBRTArrayParser<int>* parser = new PBRTArrayParser<int>;
	parser->setDestination(ptr);
	parser->setParseSize(targetSize);
	return addParser(name, parser,required, defaultVal);
}
template<>
bool PBRTCommandParser::addArgument(const ArgumentVector& name, std::vector<float>* ptr ,bool required
	, const std::string& defaultVal, int targetSize)
{
	PBRTArrayParser<float>* parser = new PBRTArrayParser<float>;
	parser->setDestination(ptr);
	parser->setParseSize(targetSize);
	return addParser(name, parser,required, defaultVal);
}
template<>
bool PBRTCommandParser::addArgument(const ArgumentVector& name, std::vector<std::string>* ptr ,bool required
	, const std::string& defaultVal, int targetSize)
{
	PBRTArrayParser<std::string>* parser = new PBRTArrayParser<std::string>;
	parser->setDestination(ptr);
	parser->setParseSize(targetSize);
	return addParser(name, parser,required, defaultVal);
}
template<>
bool PBRTCommandParser::addArgument(const ArgumentVector& name, std::vector<bool>* ptr ,bool required
	, const std::string& defaultVal, int targetSize)
{
	PBRTArrayParser<bool>* parser = new PBRTArrayParser<bool>;
	parser->setDestination(ptr);
	parser->setParseSize(targetSize);
	return addParser(name, parser,required, defaultVal);
}

int PBRTFlagParser::parse(const ArgumentVector& argv, int index)
{
	*storePtr = storeValue;
	return 1;
}

void PBRTFlagParser::setDestination(bool* var)
{
	storePtr = var;
}

bool PBRTFlagParser::getStoreValue() const
{
	return storeValue;
}

void PBRTFlagParser::setStoreValue(bool val)
{
	storeValue = val;
}

template<>
int PBRTStringParser::parse(const ArgumentVector& argv, int index)
{
	const Argument& arg = argv[index+1];
	if(!arg.isString())
	{
		std::cout << "Error: *" << arg << "* is not a valid string" << std::endl;
		return 0;
	}
	*destination = arg.getStringContent();
	return 2;
}

template<>
int PBRTIntegerParser::parse(const ArgumentVector& argv, int index)
{
	const Argument& arg = argv[index+1];
	char* ptr = NULL;
	int val = strtol(arg.getRaw().c_str(),&ptr,0);
	if(*ptr != '\0')
	{
		std::cout << "Error: *" << arg << "* is not a valid integer" << std::endl;
		return 0;
	}
	*destination = val;
	return 2;
}

template<>
int PBRTFloatParser::parse(const ArgumentVector& argv, int index)
{
	const Argument& arg = argv[index+1];
	char* ptr = NULL;
	float val = strtof(arg.getRaw().c_str(),&ptr);
	if(*ptr != '\0')
	{
		std::cout << "Error: *" << arg << "* is not a valid floaring point number" << std::endl;
		return 0;
	}
	*destination = val;
	return 2;
}

template<>
int PBRTBoolenParser::parse(const ArgumentVector& argv, int index)
{
	const Argument& arg = argv[index+1];
	if(!arg.isString())
	{
		std::cout << "Error: *" << arg << "* is not a valid boolen" << std::endl;
		return 0;
	}

	std::string content = arg.getStringContent();
	if(content == "true")
	{
		*destination = true;
		return 2;
	}
	else if(content == "false")
	{
		*destination = false;
		return 2;
	}
	std::cout << "Error: *" << arg << "* is not a valid boolen" << std::endl;
	return 0;
}

std::string makeList(const ArgumentVector& value, int index, int size)
{
	std::string result = "[";
	for(int i=0;i<size;i++)
		result += value[index+i].getRaw() + " ";
	result += "]";
	return result;
}

}
